/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Configurator provides initial config definition
 *              parsing and latter access to config options
 *
 ***************************************************************/
import {ConfigurationException} from "../exceptions/exceptions";

export class Configurator {

    private configDataObject: Configuration = null;

    private instanceIsConfigInstance:boolean = false;

    private defaultMongo: DBConfiguration = null;

    private defaultMYSQL: DBConfiguration = null;

    private defaultRedis: DBConfiguration = null;

    private dbList: Map <string, DBConfiguration> = new Map();

    private dbTypes: Map<string, string> = new Map();

    private serverConfiguration: ServerConfig = null;

    /**
     *
     * -----------------------------------------------------
     * @param {Configuration} configObject
     */
    constructor(configObject: Configuration) {

        this.dbTypes.set("mongodb", "mongo");
        this.dbTypes.set("mongo", "mongo");
        this.dbTypes.set("mysql", "mysql");
        this.dbTypes.set("mariadb", "mysql");
        this.dbTypes.set("redis", "redis");
        this.dbTypes.set("cassandra", "cassandra");
        this.dbTypes.set("dynamo", "dynamodb");
        this.dbTypes.set("dynamodb", "dynamodb");

        this.serverConfiguration = new ServerConfig();

        this.configDataObject = configObject;

        /*
         *   Set as config object if methods are correct
         */
        if(typeof this.configDataObject.has === "function")
            this.instanceIsConfigInstance = true;

        /*
         *
         *            Databases configuration
         *
         */
        if(configObject.hasOwnProperty("db")) {

            for (let key in configObject["db"]) {

                /*
                 *  If there was none type defined check key for unique type
                 */
                if(!(configObject["db"][key] as any).hasOwnProperty("type")) {

                    const type = key.toLowerCase();

                    if(this.dbTypes.has(type)) {
                        configObject["db"][key]["type"] = this.dbTypes.get(type);
                    } else
                        configObject["db"][key]["type"] = null;

                }
                /*
                 *  If there was a type check for a normalized version
                 */
                else {

                    const type = configObject["db"][key]["type"];

                    if(this.dbTypes.has(type)) {
                        configObject["db"][key]["type"] = this.dbTypes.get(type);
                    } else
                        configObject["db"][key]["type"] = null;

                }

                if(!(configObject["db"][key] as any).hasOwnProperty("name")) {
                    configObject["db"][key]["name"] = key;
                }

                this.dbList.set(key, configObject["db"][key]);

                // Cache default mongo instance configuration if found
                if(key === "mongo") {
                    configObject["db"][key]["type"] = "mongo";
                    this.defaultMongo = configObject["db"][key];
                }
                // Cache default mysql instance configuration if found
                else if(key === "mysql") {
                    configObject["db"][key]["type"] = "mysql";
                    this.defaultMYSQL = configObject["db"][key];
                }
                // Cache default redis instance configuration if found
                else if(key === "redis") {
                    configObject["db"][key]["type"] = "redis";
                    this.defaultRedis = configObject["db"][key];
                }

            }

        }

    }

    /**
     * Server configuration definition
     * -----------------------------------------------------
     * @returns {ServerConfig}
     */
    public getServerConfig() {

        return this.serverConfiguration;

    }

    /**
     * Get Configuration for Mongo Database
     * -----------------------------------------------------
     * @returns {DBConfiguration}
     * @throws {ConfigurationException}
     */
    public getForMongo() : DBConfiguration {

        if(this.defaultMongo === null)
            throw new ConfigurationException("Default Mongo configuration was not found. Bad configuration data."+
                " Please define: {db: {mongo: {host... object in config file", 400);

        return this.defaultMongo;

    }

    /**
     * Get Configuration for MYSQL Database
     * -----------------------------------------------------
     * @returns {DBConfiguration}
     * @throws {ConfigurationException}
     */
    public getForMYSQL() : DBConfiguration {

        if(this.defaultMYSQL === null)
            throw new ConfigurationException("Default MYSQL configuration was not found. Bad configuration data."+
                " Please define: {db: {mysql: {host... object in config file", 400);

        return this.defaultMYSQL;

    }

    /**
     * Get Configuration for Redis Database
     * -----------------------------------------------------
     * @returns {DBConfiguration}
     * @throws {ConfigurationException}
     */
    public getForRedis() : DBConfiguration {

        if(this.defaultRedis === null)
            throw new ConfigurationException("Default Redis configuration was not found. Bad configuration data."+
                " Please define: {db: {redis: {host... object in config file", 400);

        return this.defaultRedis;

    }

    /**
     * Get Configuration for Database name
     * -----------------------------------------------------
     * @param {string} dbName
     * @returns {DBConfiguration|null}
     * @throws {ConfigurationException}
     */
    public getForDatabase(dbName: string): DBConfiguration|null {

        if(this.dbList.size === 0)
            throw new ConfigurationException("No databases found in configuration. Bad configuration data."+
                " Please define: {db: {[dbtype]: {host... object in config file", 400);

        if(this.dbList.has(dbName))

            return this.dbList.get(dbName);

        return null;

    }

    /**
     * Get list of All Database entries
     * -----------------------------------------------------
     * @returns DBConfiguration[]
     */
    public getDatabaseConfigurationList(): DBConfiguration[] {

        return Array.from(this.dbList.values());

    }

    /**
     * Get config property
     * -----------------------------------------------------
     * @param {string} property
     * @returns {null | string | number}
     */
    public get(property: string) : null & {[key:string]:any} & string & number {

        if(this.instanceIsConfigInstance && this.configDataObject.has(property))

            return this.configDataObject.get(property);

        else

            return null;

    }

}

/**
 * TODO FINISH THIS CONFIGURATION FOR SSL CONNECTIONS
 * -----------------------------------------------------
 */
export class ServerConfig {

    private certificate: string = null;
    private key: string = null;
    private port: number = null;

    public getCertificateData() {

    }

    public getKeyData() {

    }

    public getPort() {

    }

}

export interface Configuration {
    db: {[index:string] : DBConfiguration}
    get?(property:string) : any;
    has?(property:string) : any;
}

export interface DBConfiguration {
    name?:string;
    host?: string | string[];
    port?: number;
    dbname?: string;
    user?: string;
    password?: string;
    socket?: string;
    type?:string;
}

export interface MongoDBConfiguration extends DBConfiguration {
    replicaName?: string;
    readPreference?: string;
}