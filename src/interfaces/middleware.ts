/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Middleware interface
 *
 ***************************************************************/
import {RouteHandler} from "./routing";
import {Request, Response} from "../app";

export interface MiddlewareHandler {
    handle():any;
}

export interface Middleware {
    new():MiddlewareHandler;
    prototype:MiddlewareHandler;
    handle(req: Request, res: Response, next:RouteHandler):void;
}