/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Command execution interface
 *
 ***************************************************************/
import {CommandPipeline} from "../command/command.pipeline";
import {CommandElement} from "../command/command.element";

export interface CommandFlag {

    name: string,
    desc: string,

}

export interface CustomCommand {

    name: string,
    desc: string,
    flags: Array<CommandFlag>
    executor: (data:any, cmd: CommandElement, pipeline: CommandPipeline) => any

}