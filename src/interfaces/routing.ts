/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Representation of routing interfaces
 *
 ***************************************************************/
import {CommandPipeline} from "../command/command.pipeline";
import {Request, Response} from "../app";
import {CommandElement} from "../command/command.element";

export interface Route {
    type: Method;
    path: string;
    to: RouteHandler;
    middleware?: RouteMiddleware[];
}

export interface ErrorRoute {
    to: ErrorRouteHandler | RouteHandler;
    middleware?: ErrorRouteMiddleware[];
}

export type ProcessHandler = (data: string, command: CommandElement, pipeline: CommandPipeline) => any;

export type RouteHandler = (req: Request, res: Response, next?: RouteHandler) => any;

export type RouteMiddleware = (req: Request, res: Response, next: Function) => void;

export type ErrorRouteHandler = (err: any, req: Request, res: Response, next?: RouteHandler) => Response;

export type ErrorRouteMiddleware = (err: any, req: Request, res: Response, next: Function) => void;

export enum Method {
    GET,
    POST,
    PUT,
    DELETE,
    PATCH
}