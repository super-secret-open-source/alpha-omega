/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Custom framework exceptions
 *
 ***************************************************************/
import {Exception} from "ts-exceptions";

export class ConfigurationException extends Exception {
    constructor(message: string, code: number) {
        super(message, code);
    }
}