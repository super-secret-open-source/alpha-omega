
/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Command pipeline particle
 *
 ***************************************************************/
import {CommandType} from "./command.types";
import {Exception} from "ts-exceptions";
import {CommandPipeline} from "./command.pipeline";
import {CommandFlag} from "../interfaces/commands";

/**
 *  Command Element is an entity with attached set of parameters and functions
 *  which can be executed by CommandPipeline within Fy infrastructure
 */
export class CommandElement {

    private name: string = null;

    private attachedPipeline: CommandPipeline = null;

    private awaitingForDefinition: boolean = true;

    private prepared: boolean = false;

    private executed: boolean = false;

    private done: boolean = false;

    private actionOnStart: (definition:Array<string>) => any | null = null;

    private actionOnFinish: (definition:Array<string>) => any | null = null;

    private commandType: CommandType = null;

    private commandExecutor: (arg:Array<string>, cmd: CommandElement, pipeline: CommandPipeline) => any | null = null;

    private commandDefinition: Array<string> = [];

    private commandDescription: string = null;

    private commandFlagsMap: Map<string, CommandFlag> = new Map();

    /**
     * Build Command Element using string name and Pipeline which defines command execution
     * ----------------------------------------------------------------------------------------------
     *
     * @param {string} name
     * @param {CommandPipeline} pipeline
     */
    constructor(name: string, pipeline: CommandPipeline) {

        this.name = name;
        this.attachedPipeline = pipeline;

    }

    public setType(type: CommandType) {

        this.commandType = type;

    }

    public setDefinition(name: string) {

        this.commandDefinition.push(name);

    }

    public setAvailableFlag(flag: CommandFlag) {

        this.commandFlagsMap.set(flag.name, flag);

    }

    public setExecutor(exec: (any:any, cmd: CommandElement, pipeline: CommandPipeline) => any) {

        if(typeof exec !== "function")

            throw new Exception("Executor passed to command pipeline for definition: "
                + this.commandDefinition + " is not a callable type", 500);

        this.commandExecutor = exec;

        this.prepared = true;

    }

    public setDescription(desc: string) {

        this.commandDescription = desc;

    }

    public getName() : string {

        return this.name;

    }

    public getType() : CommandType {

        return this.commandType;

    }

    public getDefinition() : Array<string> {

        return this.commandDefinition;

    }

    public getExecutor() : (arg:any, cmd: CommandElement, pipeline: CommandPipeline) => any | null {

        return this.commandExecutor;

    }

    public getDescription() : string {

        return this.commandDescription;

    }

    public isFlagDefined(flag: string) : boolean {

        return this.commandFlagsMap.has(flag);

    }

    /**
     * Main command execution process which will be used by pipeline to execute command in order
     * ----------------------------------------------------------------------------------------------
     *
     * Should return promise for pipeline will be able chain execution
     * async model structure of framework.
     *
     * @returns Promise<any>
     */
    public async execute() : Promise<any> {

        return new Promise((res, rej) => {

            // Add action before
            if(this.actionOnStart !== null)
                this.actionOnStart(this.commandDefinition);

            // Add function which should be executed
            if(this.commandExecutor !== null) {

                this.executed = true;

                this.commandExecutor(this.commandDefinition, this, this.attachedPipeline).then(() => {

                    this.done = true;

                    if(this.actionOnFinish !== null)
                        this.actionOnFinish(this.commandDefinition);

                    res();

                }).catch((e:any) => rej(e));

            }
            // Resolve if no executors were defined
            else

                res();

        });

    }

    public isExecuted() : boolean {

        return this.executed;

    }

    public isDone() : boolean {

        return this.done;

    }

    public isAwaitingForDefinition() : boolean {

        return this.awaitingForDefinition;

    }

    public __setToNotAwaitAnyNewInput() {

        this.awaitingForDefinition = false;

    }

    public isPrepared() : boolean {

        return this.prepared;

    }

    public onStart(exec: (definition:Array<string>) => any) : void {

        if(typeof exec !== "function")

            throw new Exception("Executor passed to command pipeline for definition: "
                + this.commandDefinition + " is not a callable type", 500);

        this.actionOnStart = exec;

    }

    public onFinish(exec: (defintion:Array<string>) => any) : void {

        if(typeof exec !== "function")

            throw new Exception("Executor passed to command pipeline for definition: "
                + this.commandDefinition + " is not a callable type", 500);

        this.actionOnFinish = exec;

    }

}