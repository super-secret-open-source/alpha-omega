/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Command manager Command definitions
 *
 ***************************************************************/
export enum CommandType {

    CustomCommand,
    StaticProcess

}