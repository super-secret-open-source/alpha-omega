/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Command manager provides set of methods to add
 *              custom commands and processes
 *
 ***************************************************************/
import {CommandElement} from "./command.element";
import {CommandType} from "./command.types";
import {CommandPipeline} from "./command.pipeline";
import {CustomCommand} from "../interfaces/commands";

export class CommandManager {

    private binaryPath: string = null;

    private programMainPath: string = null;

    private customCommands: Map<string, CommandElement> = new Map();

    private commandPipeline: CommandPipeline = null;

    public constructor(processCommandArguments: Array<string>) {

        this.commandPipeline = new CommandPipeline();

        this._parseCommandArguments(processCommandArguments);

    }

    /**
     * Define custom command to be executed when called by command prompt
     * ------------------------------------------------------------------------------------
     *
     * @param {CustomCommand} commandDefinition
     */
    public defineCustomCommand(commandDefinition: CustomCommand) {

        const command = new CommandElement(commandDefinition.name, this.commandPipeline);

        command.setType(CommandType.CustomCommand);
        command.setExecutor(commandDefinition.executor);
        command.setDescription(commandDefinition.desc);

        if(commandDefinition.flags) {

            commandDefinition.flags.map(flag => command.setAvailableFlag(flag));

        }

    }

    /**
     * Get command by it's definition or name if any defined on pipeline
     * ------------------------------------------------------------------------------------
     *
     * @param {string} name
     * @returns {CommandElement}
     */
    public getCommand(name: string) : CommandElement | null {

        return this.commandPipeline.getCommandByDefinition(name);

    }

    /**
     * Outputs pipeline object
     * ------------------------------------------------------------------------------------
     *
     * @returns {CommandPipeline}
     */
    public getPipeline() : CommandPipeline {

        return this.commandPipeline;

    }

    /**
     * Parse Array of arguments came from external command to Command Elements
     * ------------------------------------------------------------------------------------
     * @param {Array<string>} argv
     * @private
     */
    private _parseCommandArguments(argv: Array<string>) {

        let toBeFilledWithCommand: CommandElement = null;

        argv.map((_arg, index: number) => {

            // First ARGV parameter should be Node executable path
            if(index === 0) {
                this.binaryPath = _arg;
                return;
            }
            // Second ARGV parameter should be path of main program file
            else if(index === 1) {
                this.programMainPath = _arg;
                return;
            }

            // Trim argument
            const arg = _arg.trim();

            // Parse flags presented in command prompt
            if(arg.charAt(0) === ':') {

                arg.split(':').map(sarg => {

                    if(sarg.length > 0) {

                        if(toBeFilledWithCommand !== null)

                            toBeFilledWithCommand.setAvailableFlag({name:sarg, desc:""});

                        else

                            this.commandPipeline.addFlag(sarg);

                    }

                });

            }
            // Otherwise parse exact names of commands
            else if(arg !== '+') {

                if(toBeFilledWithCommand !== null)

                    toBeFilledWithCommand.setDefinition(arg);

                else {

                    let command = this._whichCommandFlag(arg);

                    if (command.isAwaitingForDefinition())
                        toBeFilledWithCommand = command;

                    this.commandPipeline.add(command);

                }

            }
            // If next char is separator character then skip and await for next command
            else if(arg === '+') {

                if(toBeFilledWithCommand !== null) {

                    toBeFilledWithCommand.__setToNotAwaitAnyNewInput();

                    toBeFilledWithCommand = null;

                }

            }

        });

    }

    /**
     * Distinguish is command waiting for specific predefined custom command
     * or it should be executor from route.command
     * ------------------------------------------------------------------------------------
     * @param {string} command
     * @returns {CommandElement}
     * @private
     */
    private _whichCommandFlag(command: string) : CommandElement {

        const element = new CommandElement(command, this.commandPipeline);

        if(this.customCommands.has(command)) {

            element.setType(CommandType.CustomCommand);
            element.setExecutor(this.customCommands.get(command).getExecutor());

        } else

            element.setType(CommandType.StaticProcess);

        return element;

    }

}