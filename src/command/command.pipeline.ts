/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Command pipeline execution
 *
 ***************************************************************/
import {CommandElement} from "./command.element";

export class CommandPipeline {

    private flagMapStorage: Map<string, boolean> = new Map();

    private executionLine: Array<CommandElement> = [];

    private commandMapping: Map<string, number> = new Map();

    public isFlagDefined(flag: string) : boolean {

        return this.flagMapStorage.has(flag);

    }

    public add(element: CommandElement) : void {

        const n = this.executionLine.push(element);

        this.commandMapping.set(element.getName(), n - 1);

    }

    public addFlag(name: string) : void {

        this.flagMapStorage.set(name, true);

    }

    public getCommandByDefinition(name: string) {

        if(this.commandMapping.has(name))

            return this.executionLine[this.commandMapping.get(name)];

        return null;

    }

    /**
     * Get length of only active commands which are ready to be executed
     * -----------------------------------------------------------------------
     * @returns {number}
     */
    public getLength() : number {

        let l: number = 0;

        for(let i=0; i < this.executionLine.length; i++) {

            if(this.executionLine[i].isPrepared())

                l++;

        }

        return l;

    }

    /**
     * Start pipeline execution
     * -----------------------------------------------------------------------
     */
    public execute() {

        const pipeline = this.executionLine.map(cmd => {

            if(cmd.isPrepared())

                return cmd.execute();

            return false;

        });

        // Execute all commands synchronously
        Promise.all(pipeline).then(() => {

            // Exit on finish
            process.exit();

        })
            // Exit on error
            .catch((e) => {

                console.error("Error happened while process was executing. Error message:");

                if(e.hasOwnProperty("code"))

                    console.error('[' + e.code + "] " + e.message);

                else

                    console.error(e);

                process.exit();
            }
        );

    }

}