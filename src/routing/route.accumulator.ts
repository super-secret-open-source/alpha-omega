/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Network routes preparation
 *              and controller management
 *
 ***************************************************************/
import {
    ErrorRoute,
    ErrorRouteHandler, ErrorRouteMiddleware, Method,
    ProcessHandler, Route, RouteHandler,
    RouteMiddleware
} from "../interfaces/routing";
import {ConfigurationException} from "../exceptions/exceptions";
import {App,Request, Response, Process} from "../app";
import {CommandPipeline} from "../command/command.pipeline";
import {Exception} from "ts-exceptions";

export class RouteAccumulator {

    private processList: Process[] = [];

    private routes: Route[] = [];

    private executeBefore: RouteMiddleware[] = [];

    private executeAfter: RouteMiddleware[] = [];

    private executeOnNotFound: ErrorRoute[] = [];

    private executeOnError: ErrorRoute[] = [];

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @returns {Process[]}
     * @private
     */
    public __getProcessList() : Process[] {
        return this.processList;
    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @returns {Route[]}
     * @private
     */
    public __getRoutes() : Route[] {
        return this.routes;
    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @returns {RouteMiddleware[]}
     * @private
     */
    public __getBeforeRoutes() : RouteMiddleware[] {
        return this.executeBefore;
    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @returns {RouteMiddleware[]}
     * @private
     */
    public __getAfterRoutes() : RouteMiddleware[] {
        return this.executeAfter;
    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @returns {ErrorRoute[]}
     * @private
     */
    public __getNotFoundRoutes() : ErrorRoute[] {
        return this.executeOnNotFound;
    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @returns {ErrorRoute[]}
     * @private
     */
    public __getErrorHandlers() : ErrorRoute[] {
        return this.executeOnError;
    }

    public command(name: string, to: ProcessHandler) {

        let _to:any = to;

        if(_to.toString().indexOf("return __awaiter(this") === -1)

            _to = asyncProcessWrapper(to);

        this.processList.push({name: name, to: _to});

    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @param {string} path
     * @param {RouteHandler} to
     * @param {RouteMiddleware[]} middleware
     */
    public get(path:string,
               to: RouteHandler,
               middleware?: RouteMiddleware[]) {

        this.validateHandler(to);
        this.validateMiddleware(middleware);

        let _to:any = to;

        if(_to.toString().indexOf("return __awaiter(this") > 0)

            _to = asyncRouteWrapper(to);

        this.routes.push({type: Method.GET, to: _to, path: path, middleware: middleware});

    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @param {string} path
     * @param {RouteHandler} to
     * @param {RouteMiddleware[]} middleware
     */
    public post(path:string,
                to: RouteHandler,
                middleware?: RouteMiddleware[]) {

        this.validateHandler(to);
        this.validateMiddleware(middleware);

        let _to:any = to;

        if(_to.toString().indexOf("return __awaiter(this") > 0)

            _to = asyncRouteWrapper(to);

        this.routes.push({type: Method.POST, to: _to, path: path, middleware: middleware});

    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @param {string} path
     * @param {RouteHandler} to
     * @param {RouteMiddleware[]} middleware
     */
    public put(path:string,
               to: RouteHandler,
               middleware?: RouteMiddleware[]) {

        this.validateHandler(to);
        this.validateMiddleware(middleware);

        let _to:any = to;

        if(_to.toString().indexOf("return __awaiter(this") > 0)

            _to = asyncRouteWrapper(to);

        this.routes.push({type: Method.PUT, to: _to, path: path, middleware: middleware});

    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @param {string} path
     * @param {RouteHandler} to
     * @param {RouteMiddleware[]} middleware
     */
    public delete(path:string,
                  to: RouteHandler,
                  middleware?: RouteMiddleware[]) {

        this.validateHandler(to);
        this.validateMiddleware(middleware);

        let _to:any = to;

        if(_to.toString().indexOf("return __awaiter(this") > 0)

            _to = asyncRouteWrapper(to);

        this.routes.push({type: Method.DELETE, to: _to, path: path, middleware: middleware});

    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @param {string} path
     * @param {RouteHandler} to
     * @param {RouteMiddleware[]} middleware
     */
    public patch(path:string,
                 to: RouteHandler,
                 middleware?: RouteMiddleware[]) {

        this.validateHandler(to);
        this.validateMiddleware(middleware);

        let _to:any = to;

        if(_to.toString().indexOf("return __awaiter(this") > 0)

            _to = asyncRouteWrapper(to);

        this.routes.push({type: Method.PATCH, to: _to, path: path, middleware: middleware});

    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @param {RouteMiddleware} to
     */
    public handleBefore(to: RouteMiddleware) {

        this.validateMiddleware(to, true);

        let _to:any = to;

        if(_to.toString().indexOf("return __awaiter(this") > 0)

            _to = asyncRouteWrapper(to);

        this.executeBefore.push(_to);

    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @param {RouteMiddleware} to
     */
    public handleAfter(to: RouteMiddleware) {

        this.validateMiddleware(to, true);

        let _to:any = to;

        if(_to.toString().indexOf("return __awaiter(this") > 0)

            _to = asyncRouteWrapper(to);

        this.executeAfter.push(_to);

    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @param {RouteHandler} to
     * @param {ErrorRouteMiddleware[]} middleware
     */
    public handleNotFound(to: RouteHandler, middleware?: ErrorRouteMiddleware[]) {

        this.validateMiddleware(middleware);

        let _to:any = to;

        if(_to.toString().indexOf("return __awaiter(this") > 0)

            _to = asyncRouteWrapper(to);

        this.executeOnNotFound.push({
            to: _to,
            middleware: middleware
        });

    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @param {ErrorRouteHandler} to
     * @param {ErrorRouteMiddleware[]} middleware
     */
    public handleError(to: ErrorRouteHandler, middleware?: ErrorRouteMiddleware[]) {

        this.validateMiddleware(middleware);

        let _to:any = to;

        if(_to.toString().indexOf("return __awaiter(this") > 0)

            _to = asyncRouteWrapper(to);

        this.executeOnError.push({
            to: _to,
            middleware: middleware
        });

    }

    /**
     * Validate route Handler
     * -----------------------------------------------------------------------------------------------
     * @param {Function} handler
     */
    private validateHandler(handler: Function) {

        if(typeof handler !== "function") {

            throw new ConfigurationException(
                "Handler validation failed for method: "+this.getHandlerName(handler)+
                ". Passed method is not a function. " +
                " Check your routes configuration", 500);

        }
        else if(handler.length < 2) {

            throw new ConfigurationException(
                "Handler validation failed for method: "+this.getHandlerName(handler)+
                ". Wrong number of parameters: " + handler.length + " expected 3. " +
                " Check your routes configuration", 500);

        }

    }

    /**
     * Validate list of Middleware
     * -----------------------------------------------------------------------------------------------
     * @param {Function|Function[]} handlers
     * @param {boolean} strict
     */
    private validateMiddleware(handlers: Function|Function[], strict: boolean = false) {

        /*
         *  Make strict validation - check that handler is defined, it's single, and it's callable
         */
        if(strict) {

            if(typeof handlers === "function") {

                if(handlers.length !== 3) {

                    throw new ConfigurationException(
                        "Middleware validation failed for method: "+this.getHandlerName(handlers)+
                        ". Wrong number of parameters: " + handlers.length + " expected 3. " +
                        " Check your routes configuration", 500);

                }

                return;

            }

            throw new ConfigurationException("Middleware validation failed. Check your routes configuration", 500);

        }
        /*
         * Make soft validation - do not check that handler is defined
         */
        else {

            /*
             *  Check that handler is single one
             */
            if(typeof handlers === "function") {

                if(handlers.length < 3) {

                    throw new ConfigurationException(
                        "Middleware validation failed for method: "+this.getHandlerName(handlers)+
                        ". Wrong number of parameters: " + handlers.length + " expected 3. " +
                        " Check your routes configuration", 500);

                }

                return;

            }
            /*
             *  Check that handlers is a list
             */
            else if (typeof handlers !== "undefined" && typeof handlers[0] !== "undefined") {

                handlers.map((handler) => {

                    if(typeof handler !== "function" || handler.length < 3) {

                        throw new ConfigurationException(
                            "Middleware validation failed for method: "+this.getHandlerName(handler)+
                            ". Wrong number of parameters: " + handlers.length + " expected 3. " +
                            " Check your routes configuration", 500);

                    }

                });

                return;

            }

        }

    }

    /**
     * Get name of Handler Function
     * -----------------------------------------------------------------------------------------------
     * @param {Function} handler
     * @returns {string}
     */
    private getHandlerName(handler: Function) {

        let props = Object.getOwnPropertyDescriptor(handler, "name");
        return props.value;

    }

}

/**
 * If route controller is written with async/await syntax, then probably it may
 * be a situation when route rejection is not called properly or not called at all,
 * in that case we should add additional catch handler to request by decorating
 * controller call with additional wrapper
 * -----------------------------------------------------------------------------------------------
 * @param {Function} fn
 * @returns {Function}
 */
export function asyncRouteWrapper(fn:Function) : Function {

    return (req:Request, res:Response, next?: any) => {

        fn(req, res, next).catch((e: any) => {

            App.error(
                "Route rejection happened on path: [" + req.path + "] with message: " + e.message,
                "app:controller");

            if(!res.headersSent) {

                let code = typeof e.code !== "undefined" ? e.code : 500;
                let message = typeof e.message !== "undefined" ? e.message : e;

                process.nextTick(()=> {

                    let r: any = res;
                    r.shouldKeepAlive = false;

                    res.status(code).send(message).end();

                });

            } else {

                console.error("Headers was sent prior error was catched on route, "+
                    "please be sure to avoid using [Response.send(code)] when you want to output some error. "+
                    "Use [throw new Exception(message,code)] instead");

                req.destroy(new Error("Improper execution flow on route"));

            }

        });

    }

}

/**
 * If route controller is written without async/await syntax, then system not able to guarantee
 * order of execution for command pipeline. Executor will be wrapped into Promise to have proper
 * execution mechanics
 * -----------------------------------------------------------------------------------------------
 * @param {Function} fn
 * @returns {Function}
 */
export function asyncProcessWrapper(fn:Function) : Function {

    return (data: string, pipeline: CommandPipeline) => {

        return new Promise((res, rej) => {

            try {

                fn(data, pipeline);
                res();

            } catch (e) {

                if(e.hasOwnProperty("code"))

                    rej(e);

                else

                    rej(new Exception(e.message, 500));

            }

        });

    }

}