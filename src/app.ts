/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Application manager
 *
 ***************************************************************/
import * as http from "http";
import * as https from "https";
import * as path from "path";
import * as express from "express";
import * as debug from "debug";
import * as config from "config";

import {Application} from "express";
import {RouteParser} from "./routing/route.parser";
import {RouteAccumulator} from "./routing/route.accumulator";
import {Configurator} from "./config/configurator";
import {IDebug, IDebugger} from "debug";
import {ConfigurationException} from "./exceptions/exceptions";
import {CommandManager} from "./command/command.manager";
import {Request as ExpressRequest} from "express"
import {Response as ExpressResponse} from "express";
import {ProcessHandler} from "./interfaces/routing";
import {ORMLoader} from "./orm/system/orm.loader";

/** -----------------------------------------------------------------
 *                  Main Application Module
 ----------------------------------------------------------------- */
export class App {

    private static _commandManager: CommandManager = null;

    private static _debugInstances: Map<string,IDebugger> = new Map();

    private static _debugEnabled = false;

    private static _initialized = false;

    private static _server: any = null;

    private static _systemPath: any = null;

    private static _express: any = null;

    private static _debug: IDebug = null;

    private static _http: any = null;

    private static _https: any = null;

    private static _router: RouteAccumulator = null;

    private static _routeParser: RouteParser = null;

    private static _configuration: Configurator = null;

    /**
     * Initialize Application services
     * -----------------------------------------------------------------
     */
    public static async init(routesFilePath: string) : Promise<App> {

        console.time("app startup time was:");

        if(this._commandManager === null)

            this._commandManager = new CommandManager(process.argv);

        let routeStack: () => void = null;

        try {

            routeStack = require(process.cwd() + '/' + routesFilePath);

        } catch (e) {

            console.error("Route stack cannot be resolved for path: " + routesFilePath +
                " please use correct file name for application routes can be initialized."+
                " Original error: " + e.message + "\n");

        }

        // Apply configuration from config file
        this._configuration = new Configurator(config as any);

        // Create Debug console output environment if application in verbose mode
        if (typeof process.env.DEBUG_OUTPUT !== "undefined")

            this._debugEnabled = true;

        // Initialize Foundation libraries
        this.initFoundation();

        // Initialize Storage Connections
        try {

            const ormLoader = new ORMLoader();

            // Load and initialize ORM Instances
            await ormLoader.load(this._configuration.getDatabaseConfigurationList());

        } catch (e) {

            process.exit(e.code);

        }

        // Initialize List of Application Routes
        this.initRoutes(routeStack);

        // Set as initialized after all configuration got through
        this._initialized = true;

        console.timeEnd("app startup time was:");

        // Return App static
        return this;

    }

    /**
     * Get debug instance for some name
     * -----------------------------------------------------------------
     * @param {string} name
     * @returns {debug.IDebugger}
     */
    public static debug(name = "app:log") : IDebugger {

        const instance = this._debug(name);
        instance.enabled = this._debugEnabled;

        return instance;

    }

    /**
     * Apply Debug log output for some namespace
     * -----------------------------------------------------------------
     * @param {string} message
     * @param {string} namespace
     */
    public static log(message: string | any, namespace:string = "app:log") : void {

        let instance = null;

        if(!this._debugInstances.has(namespace)) {

            instance = this.debug(namespace);
            this._debugInstances.set(namespace, instance);

        } else

            instance = this._debugInstances.get(namespace);

        instance(message);

    }

    /**
     * Apply success log output for some namespace
     * -----------------------------------------------------------------
     * @param {string} message
     * @param {string} namespace
     */
    public static success(message:string | any, namespace:string = "app:success") : void {

        let instance = null;

        if(namespace !== "app:success") {
            namespace += ":success";
        }

        if(!this._debugInstances.has(namespace)) {

            instance = this.debug(namespace);
            this._debugInstances.set(namespace, instance);

        } else

            instance = this._debugInstances.get(namespace);

        message = "\x1b[32m" + "[SUCCESS]" + message;

        instance(message);

    }

    /**
     * Apply Debug error output for some namespace
     * -----------------------------------------------------------------
     * @param {string} message
     * @param {string} namespace
     */
    public static error(message:string | any, namespace:string = "app:error") : void {

        let instance = null;

        if(namespace !== "app:error") {
            namespace += ":error";
        }

        if(!this._debugInstances.has(namespace)) {

            instance = this.debug(namespace);
            this._debugInstances.set(namespace, instance);

        } else

            instance = this._debugInstances.get(namespace);

        message = "\x1b[31m" + "[ERROR]" + message;

        instance(message);

    }

    /**
     * Outputs Express Application instance
     * -----------------------------------------------------------------
     * @returns {any}
     */
    static get express() : Application | null {
        return this._express;
    }

    /**
     * Outputs Application Route Accumulator
     * -----------------------------------------------------------------
     * @returns {RouteAccumulator}
     */
    static get router() : RouteAccumulator {
        return this._router;
    }

    /**
     * Outputs Application Command manager
     * -----------------------------------------------------------------
     * @returns {CommandManager}
     */
    static get commandManager() : CommandManager {

        if(this._commandManager === null)

            this._commandManager = new CommandManager(process.argv);

        return this._commandManager;

    }

    /**
     * Outputs application configuration from attached config file
     * -----------------------------------------------------------------
     * @returns {Configurator}
     */
    static get config() : Configurator {

        return this._configuration;

    }

    /**
     * Start Application to serve web-requests
     * -----------------------------------------------------------------
     * @param {number} port
     * @param {string} host
     */
    public static start(port: number, host?: string) {

        // If Application wasn't initialized prior to be started as server Error should be thrown
        if(!this._initialized)

            throw new Error("\nApplication not been initialized, can't load dependencies at this time\n");

        // Prepare processes to check if we shouldn't start server but execute command first
        this._routeParser.prepareProcesses(this._router);

        // If there no commands found to be matching Command Pipeline then start routes with server
        if(this._commandManager.getPipeline().getLength() === 0) {

            // Cache routes before server starts
            this._routeParser.prepareRoutes(this._router);

            this.log("Program start, service loading at port: " + port);

            // Start server according to configuration
            this._server.listen(port);

        }
        // If pipeline has commands then start pipeline executing commands
        else

            this._commandManager.getPipeline().execute();

    }

    /**
     * Initialize Foundation libraries for Bundle platform
     * -----------------------------------------------------------------
     */
    private static initFoundation() {

        process.on('unhandledRejection', (reason) => {
            console.warn("Unhandled rejection registered");
            console.log(reason);
        });

        // Initialize express server
        this._express = express();

        // Apply debug module to App
        this._debug = debug;

        // Apply HTTP module to App
        this._http = http;

        // Apply Path module to App
        this._systemPath = path;

        // Create new Server instance with express handler
        this._server = this._http.Server(this._express);

        // TODO Implement HTTPS SERVER CREATION
        // this._server = https.createServer({}, this._express);

    }

    /**
     * Initialize Routing system
     * -----------------------------------------------------------------
     */
    private static initRoutes(routeStack : any) {

        // If passed variable is object then we should extract first key of object
        if(typeof routeStack === "object")

            routeStack = routeStack[Object.keys(routeStack)[0]];

        // Routestack should be represented by imported module function
        if(typeof routeStack !== "function")

            throw new ConfigurationException("Route stack passed to route initializer is not a function,"+
                " can't initialize routes", 500);

        // Create Route parser instance to parse defined set of routes
        this._routeParser = new RouteParser(this);

        // Create new Route accumulator for route collecting
        this._router = new RouteAccumulator();

        // Open route file to catch suitable routes and pass them to routing system
        routeStack();

    }

}

/*  -----------------------------------------------------------------
 *
 *
 *                      Interfaces and Enumerators
 *                     ----------------------------
 *
 *     Application using rich set of different interfaces and enum
 *     properties, for fast access and clarity of required interfaces
 *     those will be provided in main module
 *
 *  -----------------------------------------------------------------
 */

/**
 * Request interface
 * -----------------------------------------------------------------
 * describing set of parameters which Application method
 * attached to route will be getting when router will call
 * such method after network request
 *
 * (as of current router is Express - IF providing extension)
 */
export interface Request extends ExpressRequest {
    json?: {[key:string]:any};
    auth?: Auth<any>
}

/**
 * Authenticated Request interface
 * -----------------------------------------------------------------
 */
export interface AuthenticatedRequest<T> extends Request {
    auth: Auth<T>
}

/**
 * Response interface
 * -----------------------------------------------------------------
 * describing set of parameters which Application
 * should pass back to network channel as HTTP request
 * answer
 *
 * (as of current router is Express - IF providing extension)
 */
export interface Response extends ExpressResponse
{

}

/**
 * Process interface
 * -----------------------------------------------------------------
 * describing set of parameters which Application method
 * attached to process route will be getting on command
 * execution request
 *
 */
export interface Process {
    name: string;
    to: ProcessHandler;
}

/**
 * Types supported by Application
 * -----------------------------------------------------------------
 * While using ORM User may encounter that some Typing can
 * be used within Model interaction. Application provide
 * set of types which are can be utilized for that behavior.
 */
export enum Type {
    None,
    Int,
    Float,
    String,
    Bool,
    Map,
    Array,
    Date
}

/**
 * Auth interface for Request objects
 * -----------------------------------------------------------------
 */
export interface Auth<T> {
    type: AuthType;
    data: T;
    authorized: boolean;
}

/**
 * Simple Authentication credentials
 * -----------------------------------------------------------------
 */
export interface AuthCommonCredentials {
    login: string;
    password: string;
}

/**
 * Client Authentication credentials
 * -----------------------------------------------------------------
 */
export interface OauthClientCredentials {
    client: string;
    secret: string;
    application: string;
    scope: string;
}

/**
 * Token bearer credentials
 * -----------------------------------------------------------------
 */
export interface OauthBearerCredentials {
    token: string;
    application: string;
    user: string;
    scope: string;
    expires: number;
}

/**
 * Simple Key bearer credentials
 * -----------------------------------------------------------------
 */
export interface AuthKeyBearerCredentials {
    key: string;
    expiresAt: string;
}

/**
 * Possible types of Authentication for service
 * -----------------------------------------------------------------
 */
export enum AuthType {
    Basic,
    Bearer,
    Session
}