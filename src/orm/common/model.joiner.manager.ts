/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Joiner storage container for Joiners to be
 *              easy discoverable among the data structures
 *
 ***************************************************************/
import {ModelJoinerInterface} from "./model";

export class ModelJoinerManager {

    private static map: {[key:string]:ModelJoinerInterface} = {};

    public static set(name:string, joiner: ModelJoinerInterface) {

        this.map[name] = joiner;

    }

    public static get(name:string) : ModelJoinerInterface {

        if(this.map.hasOwnProperty(name))
            return this.map[name];

        return null;

    }

}