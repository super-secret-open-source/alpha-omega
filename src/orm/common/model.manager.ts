/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Model manager provides initial Model instantiation
 *              and connectivity to data sources
 *
 ***************************************************************/
import {DBConfiguration, MongoDBConfiguration} from "../../config/configurator";
import {ConfigurationException} from "../../exceptions/exceptions";
import * as Mongoose from "mongoose";
import {Connection} from "mongoose";
import {App} from "../../app";

export class ModelManager {

    private static ready: boolean = false;

    private static awaitingForReadyStateIdMap = {mongo:0, mysql:1, redis:2};

    private static awaitForReadyState: Array<boolean> = new Array(3);

    private static defaultMongoInstance: any = null;

    private static defaultMongoInstanceName: string = null;

    private static mongoConnectionInitialized: boolean = false;

    private static mysqlConnectionInitialized: boolean = false;

    private static redisConnectionInitialized: boolean = false;

    private static dbs: Map<string, any> = new Map();

    private static default: any = null;

    /**
     * Ready flag will be available when Manager will be fully prepared
     * -----------------------------------------------------------------------
     * @returns {boolean}
     */
    public static isReady() : boolean {

        if(!this.ready) {

            for(let i=0; i < this.awaitForReadyState.length; i++) {

                if(this.awaitForReadyState[i] === false)
                    return false;

            }

            this.ready = true;

        }

        return this.ready;

    }

    /**
     *
     * -------------------------------------------------------------------------
     * @param {string} name
     * @param {DBConfiguration} configuration
     * @param {boolean} asDefault
     */
    public static setConfiguration(name: string, configuration: DBConfiguration, asDefault = false) {

        let db = null;

        if(configuration.type === "mongo") {

            this.awaitForReadyState[this.awaitingForReadyStateIdMap["mongo"]] = false;

            // this.initializeMongoConnection(configuration);

            db = this.createMongoConnection(configuration);

            this.dbs.set(configuration.name, db);

            if(ModelManager.defaultMongoInstance === null) {

                ModelManager.defaultMongoInstance = db;
                ModelManager.defaultMongoInstanceName = configuration.name;

            }

        }
        else if(configuration.type === "mysql") {

            this.awaitForReadyState[this.awaitingForReadyStateIdMap["mysql"]] = false;

            // Stub
            this.awaitForReadyState[this.awaitingForReadyStateIdMap["mysql"]] = true;

        }
        else if(configuration.type === "redis") {

            this.awaitForReadyState[this.awaitingForReadyStateIdMap["redis"]] = false;

            // Stub
            this.awaitForReadyState[this.awaitingForReadyStateIdMap["redis"]] = true;

        }
        else {

            throw new ConfigurationException("Configuration for Database type name: "+configuration.type+" is "+
            "not supported yet, please use identified types for DB initialization", 500);

        }

        if(this.default === null)

            this.default = db;

    }

    /**
     * -------------------------------------------------------------------------
     * @returns {Promise<any>}
     */
    public static waitForReadyState() {

        return new Promise((res, rej) => {

            const tryouts = 200;

            let tryout = 0;

            const iv = setInterval(() => {

                if(ModelManager.isReady()) {
                    clearInterval(iv);
                    res();
                }

                if(tryout > tryouts)

                    rej(new ConfigurationException(
                        "Model manager wasn't able to connect all databases sources in reasonable amount of time",
                        500));

                tryout++;

            }, 50);

        });

    }

    /**
     * -------------------------------------------------------------------------
     * @param {string} name
     */
    public static setDefaultConfigurationName(name: string) {

        if(this.dbs.has(name))

            this.default = this.dbs.get(name);

    }

    /**
     * -------------------------------------------------------------------------
     *
     */
    public static getDefaultDB() {



    }

    /**
     * -------------------------------------------------------------------------
     *
     */
    public static getDefaultMongoInstance() {

        return ModelManager.defaultMongoInstance;

    }

    /**
     * -------------------------------------------------------------------------
     *
     */
    public static getDefaultMongoInstanceName() {

        return ModelManager.defaultMongoInstanceName;

    }

    /**
     * -------------------------------------------------------------------------
     * @param {string} dbConfigurationName
     */
    public static getNamedDB(dbConfigurationName: string) {

        if(ModelManager.dbs.has(dbConfigurationName)) {

            return ModelManager.dbs.get(dbConfigurationName);

        }

        return null;

    }

    /**
     * Create connection to Mongo Database
     * -------------------------------------------------------------------------
     * @param {DBConfiguration} configuration
     * @returns {"mongoose".Connection}
     */
    private static createMongoConnection(configuration: MongoDBConfiguration) : Connection {

        const anyMongoose = Mongoose as any;
        anyMongoose.Promise = global.Promise;

        /*
            Define config parameter containers
         */
        let hostString = "";
        let portString = "";
        let replicaString = "";
        let readPreference = "";

        /*
         *  Elaborate availability of port inside config
         */
        if(configuration.hasOwnProperty("port")) {

            if(typeof configuration.port === "number")

                portString  = ':' + String(configuration.port);

        }

        /*
         *  Elaborate availability of host inside config
         */
        if(configuration.hasOwnProperty("host")) {

            /*
                Multiple hosts meaning we may deal with the
                distributed host schema
             */
            if(Array.isArray(configuration.host)) {

                hostString = ModelManager
                    .distributeDatabaseHostStringArray(
                        configuration.host,
                        portString.length > 0 ? portString : null);

            } else if (typeof configuration.host === "string") {

                /*
                 *  Check if this string is a comma-separated string
                 */
                let hostStringArray = configuration.host.split(',');

                /*
                 *  If string was serialized array of hosts
                 */
                if(hostStringArray.length > 1) {

                    hostString = ModelManager
                        .distributeDatabaseHostStringArray(
                            hostStringArray,
                            portString.length > 0 ? portString : null);

                }
                /*
                 *  If string was just single host passed to program
                 */
                else {

                    hostString = configuration.host;

                    /*
                     *  Check that host string may have port built-in
                     */
                    const withPort = hostString.split(':');

                    /*
                     *  If there was no port defined — apply port from configuration port
                     */
                    if(withPort.length === 1)
                        hostString += portString;

                }

            }

        }

        if(configuration.hasOwnProperty("replicaName")) {

            /*
                Check configuration provided value to full equality
             */
            if(typeof configuration.replicaName === "string") {
                replicaString = configuration.replicaName;
            }

        }

        if(configuration.hasOwnProperty("readPreference")) {

            /*
                Check configuration provided value to full equality
             */
            if(typeof configuration.readPreference === "string") {
                readPreference = configuration.readPreference;
            }

        }

        /*
         *  Define empty credentials
         */
        let user = null;
        let pass = null;

        /*
         *  Fill user credential
         */
        if(typeof configuration.user !== "undefined") {
            if(configuration.user !== null)
                user = configuration.user;
        }

        /*
         *  Fill password
         */
        if(typeof configuration.password !== "undefined") {
            if(configuration.password !== null)
                pass = configuration.password;
        }

        let queryParams = "";

        /*
         *  Check if authentication is required
         */
        let authenticationPoint = user !== null ? "authSource=admin" : "";

        /*
         *  Define default auth options
         *  ---------------------------
         *  Check if user isn't null and isn't empty.
         *
         *  Following checkups by Mongo driver may be broken or failed if user
         *  will not be defined or empty object passed as auth
         */
        let authOptions = user !== null && user.length > 0 ? {user: user, password: pass, authSource: "admin"} : null;

        if(authenticationPoint.length > 0) {

            if(queryParams.length === 0)
                queryParams = "?";

            queryParams += authenticationPoint;

        }

        /*
         *  Append replica string
         */
        if(replicaString.length > 0) {

            if(queryParams.length === 0)
                queryParams = "?";
            else
                queryParams += "&";

            queryParams += "replicaSet="+replicaString;

        }

        /*
         *  Append Read preference
         */
        if(readPreference.length > 0) {

            if(queryParams.length === 0)
                queryParams = "?";
            else
                queryParams += "&";

            queryParams += "readPreference="+readPreference;

        }

        /*
         *  Build connection string
         */
        let connString = "".concat(
            "mongodb://", hostString,
            '/', configuration.dbname,
            queryParams);

        /*
         *  Build options
         */
        let options: any = {
            auth: authOptions,
            useMongoClient: true,
            keepAlive: true,
            reconnectTries: 9999,
            reconnectInterval: 1000,
            autoReconnect: true,
            poolSize: 25
        };

        try {

            const conn = Mongoose.createConnection(connString, options);

            ModelManager.initializeMongoConnection(conn);

            this.awaitForReadyState[this.awaitingForReadyStateIdMap["mongo"]] = true;

            return conn;

        } catch (e) {

            App.error("Cannot connect to Mongo server. Error: " + e.message, "app:orm");

        }

    }

    /**
     * Initialize connection to Mongo Database
     * -------------------------------------------------------------------------
     * @param {any} connection
     */
    private static initializeMongoConnection(connection: any) : void {

        const MongooseLong = require("mongoose-long");

        MongooseLong(Mongoose);

        connection.on("error", (error: any) => {

            App.error("System wasn't able to connect to Mongo instance at: "
                + connection.host + " port: " + connection.port + " Error:" + error,
                "db:mongo");

        });

        connection.on("connected", () => {

            App.success("Succesfully connected to Mongo at: "
                + connection.host + " port: " + connection.port,
                "db:mongo");

        });

        this.mongoConnectionInitialized = true;

    }

    /**
     * Initialize connection to MYSQL Database
     * -------------------------------------------------------------------------
     * @param {DBConfiguration} configuration
     */
    private static createMYSQLConnectionString(configuration: DBConfiguration) : void {



    }

    /**
     * Initialize connection to Redis Database
     * -------------------------------------------------------------------------
     * @param {DBConfiguration} configuration
     */
    private static createRedisConnectionString(configuration: DBConfiguration) : void {



    }

    /**
     * Combine array of strings to formatted string with port assigned
     * -------------------------------------------------------------------------
     * @param arr
     * @param port
     */
    private static distributeDatabaseHostStringArray(arr: string[], port: string = null) : string {

        let out = "";

        /*
         *  Work on every host passed through hosts array
         */
        for(let i = 0; i < arr.length; i++) {

            let host = arr[i].trim();

            /*
             *  Check that host string may have port built-in
             */
            const withPort = host.split(':');

            /*
             *  If there was no port defined — apply port from configuration port
             */
            if(withPort.length === 1 && port !== null)
                host += port;

            out += host;

            if(i !== (arr.length - 1))

                out += ',';

        }

        return out;

    }

}