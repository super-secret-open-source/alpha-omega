/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Query executor pipeline element, abstracts
 *              execution part from query construction part
 *
 ***************************************************************/
import {QueryExecutorAssembledInterface, QueryExecutorInterface, QueryExecutorSpecificInterface} from "./query";
import {QueryConstructorProduct} from "./query.constructor";

export class QueryExecutor implements QueryExecutorAssembledInterface {

    public queryConstructor: any = null;

    constructor(public query: any,
                private executor: QueryExecutorSpecificInterface,
                private exemplar: Function) {

    }

    public getOne<T>() : Promise<T> {

        if(this.queryConstructor !== null)
            this.query = this.executor.__constructQuery(this.queryConstructor);

        return this.executor.getOne(this.query, this.exemplar);

    }

    public getMany<T>() : Promise<Array<T>> {

        if(this.queryConstructor !== null)
            this.query = this.executor.__constructQuery(this.queryConstructor);

        return this.executor.getMany(this.query, this.exemplar);

    }

    public __constructQuery(qc: QueryConstructorProduct): { [p: string]: any } {
        return this.executor.__constructQuery(qc);
    }

    public getConstructorProduct(): QueryConstructorProduct {
        return this.executor.getConstructorProduct();
    }

    setConstructorProduct(p: QueryConstructorProduct): void {
        this.queryConstructor = p;
        this.executor.setConstructorProduct(p);
    }

}