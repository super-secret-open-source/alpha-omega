/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Classes and interfaces to represent query
 *
 ***************************************************************/
import {QueryExecutor} from "./query.executor";
import {MongoQuery} from "../../mongo/query/mongo.executor";
import {QueryConstructor, QueryConstructorProduct} from "./query.constructor";
import {PaginationInterface} from "../model";

/**
 *  Representation of abstract query
 *  ------------------------------------------------------------------------
 *
 *  Defines query object which is serving as an starting point for one
 *  of the several ways to build a query for persistent storage
 */
export class Query<T> implements QueryInterface<T>, MongoQueryInterface<T> {

    constructor(private executor: QueryExecutorSpecificInterface,
                private entityExemplar: Function) {

    }

    /**
     * Pass query in a syntax of the persistent storage
     * ------------------------------------------------------------------------
     * @param abstractQuery
     * @returns {QueryExecutor}
     */
    public raw(abstractQuery: any) {

        return new QueryExecutor(abstractQuery, this.executor, this.entityExemplar);

    }

    /**
     * Construct query with abstract constructor
     * ------------------------------------------------------------------------
     * @param {PaginationInterface} pagination
     * @returns {QueryConstructor<T>}
     */
    public construct(pagination?: PaginationInterface) {

        return new QueryConstructor<T>(
            new QueryExecutor(
                null,
                this.executor,
                this.entityExemplar),
            pagination);

    }

}

/**
 *  Query interface
 */
export interface QueryInterface<T> {

    raw(query: any) : QueryExecutor;
    construct(pagination?: PaginationInterface) : QueryConstructor<T>;

}

/**
 *  Representation of Mongo Query Interface (specified to MongoQuery object)
 */
export interface MongoQueryInterface<T> extends QueryInterface<T> {

    raw(query: MongoQuery) : QueryExecutor;

}

/**
 *  Representation of Query Executor
 */
export interface QueryExecutorInterface {

    setConstructorProduct(p: QueryConstructorProduct) : void;

    getConstructorProduct() : QueryConstructorProduct;

    __constructQuery(qc: QueryConstructorProduct) : {[key:string]:any}

}

export interface QueryExecutorSpecificInterface extends QueryExecutorInterface {

    getOne<T>(query: any, exemplar: Function) : Promise<T>;

    getMany<T>(query: any, exemplar: Function) : Promise<Array<T>>;

}

export interface QueryExecutorAssembledInterface extends QueryExecutorInterface {

    getOne<T>() : Promise<T>;

    getMany<T>() : Promise<Array<T>>;

}