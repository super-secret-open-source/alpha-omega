/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Query constructor pipeline, abstracts query
 *              construction from query execution
 *
 ***************************************************************/
import {QueryExecutor} from "./query.executor";
import {ModelInterface, OrderByModelInterface, PaginationInterface} from "../model";
import {TypedStructure} from "../typed.structure";
import {QueryExecutorAssembledInterface} from "./query";
import {fetchRelationsForPropertyName} from "../relations";

export class QueryConstructor<T>
    implements QueryConstructorExecutive<T>, QueryConstructorNonExecutive<T> {

    public queryPipeline: QueryConstructorProduct = null;

    protected openedParticleExists: QueryConstructorProductPipelineProduct = null;

    /**
     * Constructor
     * ------------------------------------------------------------------------------------
     * @param {QueryExecutor} executor          : Provide executor for assemble constructor
     *                                          query to the executable query
     *
     * @param {PaginationInterface} pagination  : Provide pagination interface object
     *                                          for constructor will be able to add
     *                                          pagination to it's product
     *
     * @param {T} nonExecutiveObjectToReturn    : Provide here some object of Generic type
     *                                          as a value to be returned with
     *                                          @link QueryConstructor.among method
     */
    constructor(public executor: QueryExecutorAssembledInterface,
                public pagination?: PaginationInterface,
                public nonExecutiveObjectToReturn?: T ) {

        this.queryPipeline = new QueryConstructorProduct();

        if(this.pagination && this.pagination.pagination) {
            this.queryPipeline.offset = this.pagination.offset;
            this.queryPipeline.limit = this.pagination.items;
        }

    }

    /**
     * Define parameter name to search for
     * ------------------------------------------------------------------------------------
     * @param {string} name
     * @returns {QueryOperatorConstructor<T>}
     */
    public where(name: string) : QueryOperatorConstructor<T> {

        let particle: QueryConstructorProductPipelineProduct = null;

        if(this.openedParticleExists !== null) {

            particle = this.openedParticleExists;
            this.openedParticleExists = null;

        }
        else

            particle = this.queryPipeline.append();

        particle.name = name;

        return new QueryOperatorConstructor<T>(this, particle);

    }

    /**
     * Define next where to be added as OR statement
     * ------------------------------------------------------------------------------------
     * @returns {this}
     */
    public or() : this {

        let particle = this.queryPipeline.append();
        particle.relation = QueryConstructorRelation.Or;
        return this;

    }

    /**
     * Define next where to be added as AND statement
     * ------------------------------------------------------------------------------------
     * @returns {this}
     */
    public and() : this {

        let particle = this.queryPipeline.append();
        particle.relation = QueryConstructorRelation.And;
        return this;

    }

    /**
     * Make executor to Get single result from the persistent storage
     * ------------------------------------------------------------------------------------
     * @returns {Promise<T>}
     */
    public getOne() : Promise<T> {

        this.executor.setConstructorProduct(this.queryPipeline);
        return this.executor.getOne<T>();

    }

    /**
     * Make executor to Get Array of results from the persistent storage
     * ------------------------------------------------------------------------------------
     * @returns {Promise<Array<T>>}
     */
    public getMany() : Promise<Array<T>> {

        this.executor.setConstructorProduct(this.queryPipeline);
        return this.executor.getMany<T>();

    }

    /**
     * Will return constructor to the object which it was called from
     * ------------------------------------------------------------------------------------
     * If constructor should be used in non-executive manner, but for
     * just creating request assembly pipeline where it's results can
     * be chained with some other methods.
     *
     * This function will be available only if constructor created as
     * NonExecutive inteface
     *
     * @returns {T}
     */
    public among() : T {

        const a: ModelInterface = this.nonExecutiveObjectToReturn as any;

        if(typeof a.__useSeekForNextRequest === "function")
            a.__useSeekForNextRequest(this.queryPipeline);

        return this.nonExecutiveObjectToReturn;

    }

}

/**
 * Constructor operators
 * ------------------------------------------------------------------------------------
 */
export class QueryOperatorConstructor<T> {

    constructor(public qc: QueryConstructor<T>,
                public particle: QueryConstructorProductPipelineProduct) {}

    public equals(value: number|string) {
        this.particle.operator = QueryConstructorOperators.Equal;
        this.particle.value = value;
        return this.qc;
    }

    public in(value: Array<number|string>) {
        this.particle.operator = QueryConstructorOperators.In;
        this.particle.value = value;
        return this.qc;
    }

    public notEquals(value: number|string) {
        this.particle.operator = QueryConstructorOperators.NotEqual;
        this.particle.value = value;
        return this.qc;
    }

    public greater(value: number|string) {
        this.particle.operator = QueryConstructorOperators.Greater;
        this.particle.value = value;
        return this.qc;
    }

    public lesser(value: number|string) {
        this.particle.operator = QueryConstructorOperators.Lesser;
        this.particle.value = value;
        return this.qc;
    }

}

/**
 * Constructor Product
 * ------------------------------------------------------------------------------------
 *
 * Special pipeline which represents actions defined through constructor
 *
 */
export class QueryConstructorProduct {

    public limit: number = null;

    public offset: number = null;

    public cursor: number = 0;

    public order: Array<OrderByModelInterface> = [];

    private pipeIndex: number = -1;

    private pipeline: Array<QueryConstructorProductPipelineProduct> = new Array(32);

    public clone() : QueryConstructorProduct {

        const qcp = new QueryConstructorProduct();
        qcp.limit = this.limit;
        qcp.offset = this.offset;
        qcp.cursor = this.cursor;
        qcp.order = this.order;
        qcp.pipeIndex = this.pipeIndex;
        qcp.pipeline = this.pipeline.slice();

        return qcp;

    }

    public prepend() : QueryConstructorProductPipelineProduct {

        const newPipeline: Array<QueryConstructorProductPipelineProduct> = new Array(32);

        newPipeline[0] = {
            relation: QueryConstructorRelation.Initial,
            name: null,
            operator: null,
            value: null
        };

        for(let i = 0; i < this.pipeIndex + 1; i++) {

            newPipeline[i + 1] = this.pipeline[i];

        }

        this.pipeline = newPipeline;
        this.pipeIndex++;

        return this.pipeline[0];

    }

    public append() : QueryConstructorProductPipelineProduct {

        this.pipeline[++this.pipeIndex] = {
            relation: QueryConstructorRelation.Initial,
            name: null,
            operator: null,
            value: null
        };

        return this.pipeline[this.pipeIndex];

    }

    public current() : QueryConstructorProductPipelineProduct {

        if(this.cursor <= this.pipeIndex)
            return this.pipeline[this.cursor];

        return null;

    }

    public next() : QueryConstructorProductPipelineProduct | null {

        if(this.cursor <= this.pipeIndex)
            return this.pipeline[this.cursor++];
        else
            this.cursor++;

        return null;

    }

    public prev() : QueryConstructorProductPipelineProduct | null {

        if(this.cursor >= 0)
            return this.pipeline[this.cursor--];
        else
            this.cursor--;

        return null;

    }

    public rewind() {

        this.cursor = 0;

    }

    public end() {

        this.cursor = this.pipeIndex;

    }

    public isAtBeginning() {

        return this.cursor <= 0;

    }

    public length() {

        return this.pipeIndex + 1;

    }

    public getPipeline() : Array<QueryConstructorProductPipelineProduct> {

        return this.pipeline;

    }

}

/**
 * Constructor to perform relational query seek and retrieve
 * ------------------------------------------------------------------------------------
 */
export class RelationalQueryConstructor<T>
    extends QueryConstructor<T>
    implements RelationalQueryConstructorInterface {

    /**
     * Constructor
     * ------------------------------------------------------------------------------------
     * @param {TypedStructure} relationalPropertyName
     * @param {TypedStructure} mainRelationStruct
     * @param {TypedStructure} seekRelationStruct
     * @param {QueryExecutor} executor          : Provide executor for assemble constructor
     *                                          query to the executable query
     *
     * @param {PaginationInterface} pagination  : Provide pagination interface object
     *                                          for constructor will be able to add
     *                                          pagination to it's product
     * @param orderBy
     */
    constructor(public relationalPropertyName: string,
                public mainRelationStruct: TypedStructure,
                public seekRelationStruct: TypedStructure,
                executor: QueryExecutorAssembledInterface,
                pagination?: PaginationInterface,
                orderBy?: OrderByModelInterface[]) {

        super(executor, pagination);

        if(orderBy !== null && typeof orderBy !== "undefined")
            this.queryPipeline.order = orderBy;

    }

    /**
     * Get main structure for relational poll
     * @returns {TypedStructure}
     */
    public mainStructure() {
        return this.mainRelationStruct;
    }

    /**
     * Get seek structure for relational poll
     * @returns {TypedStructure}
     */
    public seekStructure() {
        return this.seekRelationStruct;
    }

    /**
     * Make executor to Get single result from the persistent storage
     * ------------------------------------------------------------------------------------
     * @returns {Promise<T>}
     */
    public async getOne() : Promise<T> {

        this.executor.setConstructorProduct(this.queryPipeline);

        this.pagination.items = 1;

        const r = await fetchRelationsForPropertyName(
            this.relationalPropertyName,
            this.mainRelationStruct,
            this.seekRelationStruct,
            this.executor,
            this.pagination);

        return r as Promise<T>;

    }

    /**
     * Make executor to Get Array of results from the persistent storage
     * ------------------------------------------------------------------------------------
     * @returns {Promise<Array<T>>}
     */
    public async getMany() : Promise<Array<T>> {

        this.executor.setConstructorProduct(this.queryPipeline);

        const r = await fetchRelationsForPropertyName(
            this.relationalPropertyName,
            this.mainRelationStruct,
            this.seekRelationStruct,
            this.executor,
            this.pagination);

        return r as Promise<Array<T>>;

    }

}

/**
 *  Interface to fetch Relational Queries
 */
export interface RelationalQueryConstructorInterface {
    mainStructure: () => TypedStructure;
    seekStructure: () => TypedStructure;
}

/**
 *  Representation of some action for request executor to assemble into query
 */
export interface QueryConstructorProductPipelineProduct {
    relation: QueryConstructorRelation
    name: string,
    operator: QueryConstructorOperators,
    value: any
}

/**
 *  Actions defining Query constructor
 */
export interface QueryConstructorActions<T> {
    where: (name: string) => QueryOperatorConstructor<T>;
    or: () => QueryConstructorExecutive<T>;
    and: () => QueryConstructorExecutive<T>;
}

/**
 *  Interface for return in methods where constructor should provide
 *  some finished GET Method execution
 */
export interface QueryConstructorExecutive<T> extends QueryConstructorActions<T> {
    getOne: () => Promise<T>;
    getMany: () => Promise<Array<T>>;
}

/**
 *  Interface for return in methods where constructor should be used only as
 *  an assembly plant for software later to decide on how to perform that constructed
 *  assembly line
 */
export interface QueryConstructorNonExecutive<T> extends QueryConstructorActions<T> {
    among: () => T;
}

/**
 *  Operators which constructor using for pipeline
 */
export enum QueryConstructorOperators {
    Equal,
    NotEqual,
    In,
    Greater,
    Lesser
}

/**
 *  Relation types for pipeline parts
 */
export enum QueryConstructorRelation {
    Initial,
    And,
    Or
}