/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Relationship schema definer and relationship renderer
 *
 ***************************************************************/
import {
    DatabaseConfigurationException,
    ModelInterface,
    ModelJoinerInterface,
    ModelStaticInterface,
    OrderByModelInterface,
    PaginationInterface,
    SeekForRelationInterface
} from "./model";
import {PrimaryIndex, TypedStructure} from "./typed.structure";
import {App, Type} from "../../app";
import {DataValidationException} from "ts-exceptions";
import {QueryConstructorOperators, QueryConstructorRelation} from "./query/query.constructor";
import {QueryExecutorInterface} from "./query/query";

export class Relationship {

    private initialized: boolean = false;

    private prepared: boolean = false;

    private MTMConfirmed: boolean = false;

    private OTMConfirmed: boolean = false;

    private OTOConfirmed: boolean = false;

    private MTMMasterFor: Map<number, BelongsToManyRelation> = new Map();

    private MTMSlaveFor: Map<number, HasManyRelation> = new Map();

    private OTMMasterFor: Map<number, BelongsToOneRelation> = new Map();

    private OTMSlaveFor: Map<number, HasManyRelation> = new Map();

    private OTOMasterFor: Map<number, BelongsToOneRelation> = new Map();

    private OTOSlaveFor: Map<number, HasOneRelation> = new Map();

    private pending: Array<RelationDefinition> = [];

    private list: Array<RelationDefinition> = [];

    /**
     * Submit relationship for review and align with corresponding relationship
     * ---------------------------------------------------------------------------------
     *
     * @param {RelationDefinition} rel
     */
    public submit(rel: RelationDefinition) : void {

        // Add relationship to pending relationships which should be resolved on init
        if(rel.type > 0 && rel.type < 5) {

            this.pending.push(rel);
            this.list.push(rel);

        } else {

            throw new DatabaseConfigurationException(
                "Relationship you trying to submit for source: <"
                + rel.sourceStructure.className +
                "> to: <"
                + rel.foreignStructure.className +
                "> have improper type definition: " + rel.type);

        }

    }

    /**
     * Will do initialization of pending relationships to their final state
     * ---------------------------------------------------------------------------------
     *
     * Should be called initially for start resolution of relationship linkage
     */
    public init() {

        /*
         *  Pre-render definitions
         */
        this.pending.map(relation => {

            if(relation.__unresolvedRef !== null) {

                let ref = relation.__unresolvedRef();

                // Be sure that model relation has proper model assigned
                if(typeof ref !== "function")

                    throw new DatabaseConfigurationException(
                        "Wrong relation target passed to relation definition. "+
                        "Target should be function returns Type which model is related to");

                if(typeof ref.__getStructureStatic !== "function")

                    throw new DatabaseConfigurationException(
                        "Wrong relation reference. "+
                        "Reference should be implementing __getStructureStatic method, "+
                        "or should be extending of one of the system models like MongoModel or MySQLModel");

                relation.foreignPrototype = ref;

                relation.foreignStructure = relation.foreignPrototype
                    .__getStructureByName(Relationship.functionNameToString(relation.foreignPrototype));

            }

            relation.__unresolvedRef = null;

        });

        this.initialized = true;

    }

    /**
     * Will make necessary preparations of relationships to confirm their mutuality
     * ---------------------------------------------------------------------------------
     *
     * Should be called after initialization process is done for a Relationship
     */
    public prepare() {

        if(!this.initialized)
            throw new DatabaseConfigurationException("Relationship can't be prepared before it is initialized");

        const relationsProcessed: {[key: number] : boolean} = {};

        /*
         *  Post-process definitions
         */
        this.pending.map(rel => {

            let sourceStruct: TypedStructure = rel.sourceStructure;
            let sourceName: string = sourceStruct.className;
            let foreignStruct: TypedStructure = rel.foreignStructure;
            let foreignName: string = foreignStruct.className;

            /*
             * Check if foreign structure has some of relations
             * to be confirmed for our current relation definition
             */
            if(foreignStruct.relationStructure !== null) {

                let connections = foreignStruct.relationStructure.getRelationsForForeignClassName(sourceName);

                connections.map(cRel => {

                    /*
                     *  Has One relation to other relations checkup
                     */
                    if(rel.type === 1 && !relationsProcessed.hasOwnProperty(rel.id)) {

                        // Belongs to one parent
                        if(cRel.type === 3 && cRel.mutual === false) {

                            cRel.mutual = true;
                            rel.mutual = true;

                            this.OTOConfirmed = true;
                            this.OTOMasterFor.set(cRel.id, cRel as BelongsToOneRelation);

                            foreignStruct.relationStructure.__confirm(cRel.id, rel);

                            // If specially defined foreign property name wasn't found for master
                            // relation then use source property name from slave property
                            if(rel.foreignPropertyName === null)
                                rel.foreignPropertyName = cRel.sourcePropertyName;

                            let masterCompiled = new CompiledRelation(
                                sourceStruct.classPrototype,
                                rel.foreignPrototype,
                                sourceStruct.primaryIndex.name,
                                rel.sourcePropertyName,
                                rel.foreignPropertyName);

                            masterCompiled.weak = rel.weak;
                            masterCompiled.foreignPropertyName = cRel.sourcePropertyName;
                            masterCompiled.selfEntity = cRel.foreignPrototype;

                            sourceStruct.setRelationForPropertyName(rel.sourcePropertyName, masterCompiled);

                            // Define cascade dependency for master
                            sourceStruct.foreignCascadeDependency = true;
                            sourceStruct.foreignCascadeDependencyNames.push(rel.sourcePropertyName);

                            // If specially defined foreign property name wasn't found for slave
                            // relation then use source property name from master property
                            if(cRel.foreignPropertyName === null)
                                cRel.foreignPropertyName = rel.sourcePropertyName;

                            let slaveCompiled = new CompiledRelation(
                                foreignStruct.classPrototype,
                                cRel.foreignPrototype,
                                cRel.sourceStructure.primaryIndex.name,
                                cRel.sourcePropertyName,
                                sourceStruct.primaryIndex.name);

                            slaveCompiled.foreignPropertyName = rel.sourcePropertyName;
                            slaveCompiled.foreignCheck = true;
                            slaveCompiled.master = false;
                            slaveCompiled.weak = cRel.weak;
                            slaveCompiled.selfEntity = rel.foreignPrototype;

                            foreignStruct.setRelationForPropertyName(cRel.sourcePropertyName, slaveCompiled);

                            // Define foreign dependency for slave
                            foreignStruct.foreignDependency = true;
                            foreignStruct.foreignDependencyNames.push(cRel.sourcePropertyName);

                            masterCompiled.joinSlave = slaveCompiled;

                            App.log("Relation: One-to-One, for Master: <"
                                + rel.sourceStructure.className
                                + "> <ID:" +rel.id+ "> and Slave: <"
                                + cRel.sourceStructure.className
                                + "> <ID:" +cRel.id+ "> instantiated as mutual relation" +
                                "on properties: '" + rel.sourcePropertyName + "' " +
                                "<--> '" + rel.foreignPropertyName + "'","app:orm");

                            __deferredRelationDefinitionAppend(masterCompiled);
                            __deferredRelationDefinitionAppend(slaveCompiled);

                            relationsProcessed[rel.id] = true;

                        }

                    }

                    /*
                     *  Has Many relation to other relations checkup
                     */
                    if(rel.type === 2 && !relationsProcessed.hasOwnProperty(rel.id)) {

                        // Belongs to one parent
                        if(cRel.type === 3 && cRel.mutual === false) {

                            cRel.mutual = true;
                            rel.mutual = true;

                            this.OTMConfirmed = true;
                            this.OTMMasterFor.set(cRel.id, cRel as BelongsToOneRelation);

                            foreignStruct.relationStructure.__confirm(cRel.id, rel);

                            // If specially defined foreign property name wasn't found for master
                            // relation then use source property name from slave property
                            if(rel.foreignPropertyName === null)
                                rel.foreignPropertyName = cRel.sourcePropertyName;

                            let masterCompiled = new CompiledRelation(
                                sourceStruct.classPrototype,
                                rel.foreignPrototype,
                                sourceStruct.primaryIndex.name,
                                rel.sourcePropertyName,
                                rel.foreignPropertyName);

                            masterCompiled.foreignPropertyName = cRel.sourcePropertyName;
                            masterCompiled.outputMany = true;
                            masterCompiled.weak = rel.weak;
                            masterCompiled.selfEntity = cRel.foreignPrototype;

                            sourceStruct.setRelationForPropertyName(rel.sourcePropertyName, masterCompiled);

                            // Define cascade dependency for master
                            sourceStruct.foreignCascadeDependency = true;
                            sourceStruct.foreignCascadeDependencyNames.push(rel.sourcePropertyName);

                            // If specially defined foreign property name wasn't found for slave
                            // relation then use source property name from master property
                            if(cRel.foreignPropertyName === null)
                                cRel.foreignPropertyName = rel.sourcePropertyName;

                            let slaveCompiled = new CompiledRelation(
                                foreignStruct.classPrototype,
                                cRel.foreignPrototype,
                                cRel.sourceStructure.primaryIndex.name,
                                cRel.sourcePropertyName,
                                sourceStruct.primaryIndex.name);

                            slaveCompiled.foreignPropertyName = rel.sourcePropertyName;
                            slaveCompiled.foreignCheck = true;
                            slaveCompiled.master = false;
                            slaveCompiled.weak = cRel.weak;
                            slaveCompiled.selfEntity = rel.foreignPrototype;

                            foreignStruct.setRelationForPropertyName(cRel.sourcePropertyName, slaveCompiled);

                            // Define foreign dependency for slave
                            foreignStruct.foreignDependency = true;
                            foreignStruct.foreignDependencyNames.push(cRel.sourcePropertyName);

                            masterCompiled.joinSlave = slaveCompiled;

                            App.log("Relation: One-to-Many, for Master: <"
                                + rel.sourceStructure.className
                                + "> <ID:" +rel.id+ "> and Slave: <"
                                + cRel.sourceStructure.className
                                + "> <ID:" +cRel.id+ "> instantiated as mutual relation" +
                                "on properties: '" + rel.sourcePropertyName + "' " +
                                "<--> '" + rel.foreignPropertyName + "'","app:orm");

                            __deferredRelationDefinitionAppend(masterCompiled);
                            __deferredRelationDefinitionAppend(slaveCompiled);

                            relationsProcessed[rel.id] = true;

                        }

                        // Belongs to many parents
                        else if(cRel.type === 4 && cRel.mutual === false) {

                            cRel.mutual = true;
                            rel.mutual = true;

                            let mutualName: string = sourceName.toLowerCase()
                                + "_" + foreignName.toLowerCase() + "_join";

                            rel.mutualName = mutualName;
                            cRel.mutualName = mutualName;

                            this.MTMConfirmed = true;
                            this.MTMMasterFor.set(cRel.id, cRel as BelongsToManyRelation);

                            foreignStruct.relationStructure.__confirm(cRel.id, rel);

                            let masterCompiled = new CompiledRelation(
                                sourceStruct.classPrototype,
                                rel.foreignPrototype,
                                rel.sourceStructure.primaryIndex.name,
                                rel.sourcePropertyName,
                                foreignStruct.primaryIndex.name);

                            masterCompiled.outputMany = true;
                            masterCompiled.weak = rel.weak;

                            masterCompiled.joinName = mutualName;
                            masterCompiled.joinOwnParamName = sourceName.toLowerCase();
                            masterCompiled.joinForeignParamName = foreignName.toLowerCase();
                            masterCompiled.relatedByEntity = true;
                            masterCompiled.selfEntity = cRel.foreignPrototype;

                            masterCompiled.foreignPropertyName = cRel.sourcePropertyName;

                            sourceStruct.setRelationForPropertyName(rel.sourcePropertyName, masterCompiled);
                            // Define cascade dependency for Master Structure
                            sourceStruct.foreignCascadeDependency = true;
                            sourceStruct.foreignCascadeDependencyNames.push(rel.sourcePropertyName);

                            let slaveCompiled = new CompiledRelation(
                                foreignStruct.classPrototype,
                                cRel.foreignPrototype,
                                cRel.sourceStructure.primaryIndex.name,
                                cRel.sourcePropertyName,
                                sourceStruct.primaryIndex.name);

                            slaveCompiled.foreignCheck = true;
                            slaveCompiled.outputMany = true;
                            slaveCompiled.weak = cRel.weak;
                            slaveCompiled.foreignPropertyName = rel.sourcePropertyName;
                            slaveCompiled.selfEntity = rel.foreignPrototype;

                            slaveCompiled.joinName = mutualName;
                            slaveCompiled.joinOwnParamName = foreignName.toLowerCase();
                            slaveCompiled.joinForeignParamName = sourceName.toLowerCase();
                            slaveCompiled.relatedByEntity = true;
                            slaveCompiled.master = false;

                            foreignStruct.setRelationForPropertyName(cRel.sourcePropertyName, slaveCompiled);

                            masterCompiled.joinSlave = slaveCompiled;
                            // Define foreign dependency for Slave Structure
                            foreignStruct.foreignDependency = true;
                            foreignStruct.foreignDependencyNames.push(cRel.sourcePropertyName);

                            App.log("Relation: Many-to-Many, for Master: <"
                                + rel.sourceStructure.className
                                + "> <ID:" +rel.id+ "> and Slave: <"
                                + cRel.sourceStructure.className
                                + "> <ID:" +cRel.id+ "> instantiated as mutual relation " +
                                "on properties: '" + rel.sourcePropertyName + "' " +
                                "<--> '" + rel.foreignPropertyName + "'","app:orm");

                            __deferredRelationDefinitionAppend(masterCompiled);
                            __deferredRelationDefinitionAppend(slaveCompiled);

                            relationsProcessed[rel.id] = true;

                        }

                    }

                });

            }

            /*
             * If Relation wasn't mutually confirmed then it should
             * be processed as relaying on one of the properties
             */
            if(!rel.mutual && rel.foreignPropertyName !== null && !relationsProcessed.hasOwnProperty(rel.id)) {

                // Check if primary index is defined on source structure of relation
                if(rel.sourceStructure.primaryIndex === null)

                    throw new DatabaseConfigurationException("Relation composition error: "+
                        "Primary index should be defined for Master model for it can relay "+
                        " as non mutual Relationship to some other Model");

                let propName = sourceStruct.primaryIndex.type;
                let propType = sourceStruct.primaryIndex.type;

                let fProps = foreignStruct.properties;

                // Check if foreign structure have no such property to manage relation
                if(!fProps.hasOwnProperty(rel.foreignPropertyName))

                    throw new DatabaseConfigurationException("Relation composition error: "+
                        "Foreign property was not found for complete non mutual Relationship "+
                        " as not mutual One-to-Many to some other Model");

                let fProp = fProps[rel.foreignPropertyName];

                // Check if type of foreign property is equals to type of source property
                if(fProp.type !== propType)

                    throw new DatabaseConfigurationException("Relation composition error: " +
                        "Foreign property Type definition is different from Master property " +
                        propName + " carrying type " + Type[propType] + ". " +
                        "unable to proceed with non mutual relationship"
                    );

                let compiled = new CompiledRelation(sourceStruct.classPrototype,
                    rel.foreignPrototype,
                    rel.sourceStructure.primaryIndex.name,
                    rel.sourcePropertyName,
                    rel.foreignPropertyName);

                // Has many should output multiple
                if (rel.type === 2)
                    compiled.outputMany = true;

                rel.sourceStructure.setRelationForPropertyName(rel.sourcePropertyName, compiled);

                App.log("Relation with no mutual relation confirmation created for Master: <"
                    + sourceName
                    + "> and Slave: <"
                    + foreignName
                    + "> through Slave property: ["
                    + rel.foreignPropertyName
                    + "] and pulling type: " + (compiled.outputMany ? "[Many]":"[One]"),"app:orm");

                // Apply compiled definition to Entity properties and get/set definitions
                __deferredRelationDefinitionAppend(compiled);

                relationsProcessed[rel.id] = true;

            }

        });

        this.prepared = true;

    }

    /**
     * Will make a confirmation of some relationship came from Master side
     * ---------------------------------------------------------------------------------
     *
     * @param {number} id
     * @param {RelationDefinition} target
     * @private
     */
    public __confirm(id: number, target: RelationDefinition) {

        let rel: RelationDefinition = null;

        this.pending.map(r => rel = (r !== null && r.id === id) ? r : null);

        if(rel !== null) {

            // Confirm belongs to Many
            if(rel.type === 4 && rel.mutual) {

                this.MTMSlaveFor.set(rel.id, target as HasManyRelation);

            } else if (rel.type === 3 && rel.mutual) {

                this.OTMSlaveFor.set(rel.id, target as HasManyRelation);

            }

        }

    }

    /*
     *  Many to Many section
     */

    public isManyToMany() : boolean {
        return this.MTMConfirmed;
    }

    public isManyToManyMasterFor(structureId: number) : boolean {
        return this.MTMConfirmed && this.MTMMasterFor.has(structureId);
    }

    public isManyToManySlaveFor(structureId: number) : boolean {
        return this.MTMConfirmed && this.MTMSlaveFor.has(structureId);
    }

    public getSlaveRelationsForMTMMaster(structureId: number) : BelongsToManyRelation | null {
        if(!this.MTMMasterFor.has(structureId))
            return null;
        return this.MTMMasterFor.get(structureId);
    }

    public getMasterRelationsForMTMSlave(structureId: number) : HasManyRelation | null {
        if(!this.MTMSlaveFor.has(structureId))
            return null;
        return this.MTMSlaveFor.get(structureId);
    }

    /*
     *  One to Many section
     */

    public isOneToMany() : boolean {
        return this.OTMConfirmed;
    }

    public isOneToManyMasterFor(structureId: number) : boolean {
        return this.OTMConfirmed && this.OTMMasterFor.has(structureId);
    }

    public isOneToManySlaveFor(structureId: number) : boolean {
        return this.OTMConfirmed && this.OTMSlaveFor.has(structureId);
    }

    public getSlaveRelationsForOTMMaster(structureId: number) : BelongsToOneRelation | null {
        if(!this.OTMMasterFor.has(structureId))
            return null;
        return this.OTMMasterFor.get(structureId);
    }

    public getMasterRelationsForOTMSlave(structureId: number) : HasManyRelation | null {
        if(!this.OTMSlaveFor.has(structureId))
            return null;
        return this.OTMSlaveFor.get(structureId);
    }

    /*
     *  One to One section
     */

    public isOneToOne() : boolean {
        return this.OTOConfirmed;
    }

    public isOneToOneMasterFor(structureId: number) : boolean {
        return this.OTOConfirmed && this.OTOMasterFor.has(structureId);
    }

    public isOneToOneSlaveFor(structureId: number) : boolean {
        return this.OTOConfirmed && this.OTOSlaveFor.has(structureId);
    }

    public getSlaveRelationsForOTOMaster(structureId: number) : BelongsToOneRelation | null {
        if(!this.OTOMasterFor.has(structureId))
            return null;
        return this.OTOMasterFor.get(structureId);
    }

    public getMasterRelationsForOTOSlave(structureId: number) : HasOneRelation | null {
        if(!this.OTOSlaveFor.has(structureId))
            return null;
        return this.OTOSlaveFor.get(structureId);
    }

    public getPendingRelationById(id: number) : RelationDefinition | null {

        let search: RelationDefinition = null;

        this.pending.map((rel) => search = rel.id === id? rel : null);

        return search;

    }

    public getRelationById(id: number) : RelationDefinition | null {

        let search: RelationDefinition = null;

        this.list.map((rel) => search = rel.id === id? rel : null);

        return search;

    }

    public getRelationsForForeignClassName(name: string) : Array<RelationDefinition> {

        return this.list.filter((rel) => rel.foreignStructure.className === name);

    }

    public getRelations() : Array<RelationDefinition> {

        return this.list;

    }

    public getPendingRelations() : Array<RelationDefinition> {

        return this.pending;

    }

    /**
     * Function Name to String
     * -------------------------------------------------------------------------
     * @param {Function} func
     * @returns {string}
     */
    private static functionNameToString(func: Function) : string {

        let classNameRegEx = /(?:\S+\s+){1}([a-zA-Z_$][0-9a-zA-Z_$]*)/;
        return  classNameRegEx.exec(func.toString())[1];

    }

}

/**
 * Factory for produce UNIQ ID for every relation created on system
 * -------------------------------------------------------------------------
 */
export class RelationCounter {

    private static c: number = 0;

    public static incrementAndGet() {
        return ++this.c;
    }

}

/**
 * Has one record linked record as One-to-One relation
 * -------------------------------------------------------------------------
 */
export class HasOneRelation implements RelationDefinition  {

    public id:number = null;

    public type = 1;

    public sourceStructure: TypedStructure = null;

    public sourcePropertyName: string = null;

    public foreignStructure: TypedStructure = null;

    public foreignPrototype: any = null;

    public foreignPropertyName: string = null;

    public localPropertyName: string = null;

    public mutual: boolean = false;

    public weak: boolean = false;

    public __unresolvedRef: Function = null;

    constructor() {
        this.id = RelationCounter.incrementAndGet();
    }

}

/**
 * Has many records (can be Many-to-Many relation master)
 * -------------------------------------------------------------------------
 */
export class HasManyRelation implements RelationDefinition  {

    public id:number = null;

    public type = 2;

    public sourceStructure: TypedStructure = null;

    public sourcePropertyName: string = null;

    public foreignStructure: TypedStructure = null;

    public foreignPrototype: any = null;

    public foreignPropertyName: string = null;

    public localPropertyName: string = null;

    public mutual: boolean = false;

    public mutualName: string = null;

    public weak: boolean = false;

    public __unresolvedRef: Function = null;

    constructor() {
        this.id = RelationCounter.incrementAndGet();
    }

}

/**
 *  Belongs to One relation (One-to-Many) case
 *  -------------------------------------------------------------------------
 */
export class BelongsToOneRelation implements RelationDefinition  {

    public id:number = null;

    public type = 3;

    public sourceStructure: TypedStructure = null;

    public sourcePropertyName: string = null;

    public foreignStructure: TypedStructure = null;

    public foreignPrototype: any = null;

    public foreignPropertyName: string = null;

    public localPropertyName: string = null;

    public mutual: boolean = false;

    public weak: boolean = false;

    public __unresolvedRef: Function = null;

    constructor() {
        this.id = RelationCounter.incrementAndGet();
    }

}

/**
 *  Belongs to Many (required for Many-to-Many case) definition
 *  -------------------------------------------------------------------------
 */
export class BelongsToManyRelation implements RelationDefinition {

    public id:number = null;

    public type = 4;

    public sourceStructure: TypedStructure = null;

    public sourcePropertyName: string = null;

    public foreignStructure: TypedStructure = null;

    public foreignPrototype: any = null;

    public foreignPropertyName: string = null;

    public mutual: boolean = false;

    public mutualName: string = null;

    public weak: boolean = false;

    public __unresolvedRef: Function = null;

    constructor() {
        this.id = RelationCounter.incrementAndGet();
    }

}

export interface RelationDefinition {

    id: number;

    type: number;

    sourceStructure: TypedStructure;

    sourcePropertyName: string;

    foreignStructure: TypedStructure;

    foreignPrototype: any;

    foreignPropertyName: string;

    localPropertyName?: string;

    mutual: boolean;

    mutualName?: string;

    weak: boolean;

    __unresolvedRef: Function;

}

/**
 * Compiled relation representation
 * -------------------------------------------------------------------------
 *
 * It is representation of relation completed all checkups and ready to
 * serve as definition for data push/pull
 */
export class CompiledRelation {

    public master: boolean = true;

    public foreignCheck: boolean = false;

    public assemblingPrototype: Object = null;

    public selfPrimaryIndexName: string = null;

    public joinSlave: CompiledRelation = null;

    public joinModel: ModelJoinerInterface = null;

    public joinName: string = null;

    public joinOwnParamName: string = null;

    public joinForeignParamName: string = null;

    public foreignPropertyName: string = null;

    public throughEntity: ModelInterface & ModelStaticInterface = null;

    public queryForProperty: string = null;

    public outputMany: boolean = false;

    public relatedByEntity: boolean = false;

    public propertyName: string = null;

    public propertyStorageName: string = null;

    public propertyIdValueName: string = null;

    public propertyStatusName: string = null;

    public propertyExclusionName: string = null;

    public propertyInclusionName: string = null;

    public weak: boolean = false;

    public selfEntity: ModelInterface & ModelStaticInterface = null;

    constructor(prototype: Object,
                staticPrototype: ModelInterface & ModelStaticInterface,
                selfPrimaryIndexName: string,
                selfRelationPropertyName: string,
                relatedForeignPropertyName: string) {

        this.throughEntity = staticPrototype;
        this.assemblingPrototype = prototype;
        this.foreignPropertyName = "";
        this.selfPrimaryIndexName = selfPrimaryIndexName;
        this.propertyName = selfRelationPropertyName;
        this.propertyStorageName = "_rel_" + this.propertyName;
        this.propertyIdValueName = "_rel_" + this.propertyName + "_id";
        this.propertyStatusName = "_rel_" + this.propertyName + "_s";
        this.propertyExclusionName = "_rel_" + this.propertyName + "_e";
        this.propertyInclusionName = "_rel_" + this.propertyName + "_i";
        this.queryForProperty = relatedForeignPropertyName;

    }

}

/**
 * Substitute getters and setters belonging to relational data on Models
 * -------------------------------------------------------------------------
 *
 * This method will take property defined for to act as a part of relation
 * and transform it to multiple properties which will be reused at the model
 * level to maintain
 *
 *
 * @param {CompiledRelation} compiled
 * @private
 */
export function __deferredRelationDefinitionAppend(compiled: CompiledRelation) {

    /*
     *  Deferred relations initializer for Entities which relations defined on
     */
    setTimeout(() => {

        const inst: Object = compiled.assemblingPrototype;
        const pullingModel: ModelStaticInterface = compiled.throughEntity as any;

        // TODO Check on double initialization of some properties, should not do any reinitialization
        // TODO As for now implemented as path
        const p = Object.getOwnPropertyDescriptor(inst, compiled.propertyName);

        if(typeof p !== "undefined" && p.configurable === false)
            return;

        // If it is Foreign Dependency of kind of single value then ID storage
        // for that value should be defined as well
        if(compiled.foreignCheck && !compiled.outputMany) {

            Object.defineProperty(inst, compiled.propertyIdValueName, {
                value: null,
                writable: true
            });

        }

        // Definition of status of relation data, pulled or not pulled
        Object.defineProperty(inst, compiled.propertyStatusName, {
            value: false,
            writable: true
        });

        // Actual storage of relation data
        Object.defineProperty(inst, compiled.propertyStorageName, {
            value: null,
            writable: true
        });

        // Definition of status of relation data, pulled or not pulled
        Object.defineProperty(inst, compiled.propertyExclusionName, {
            value: null,
            writable: true
        });

        // Definition of status of relation data, pulled or not pulled
        Object.defineProperty(inst, compiled.propertyInclusionName, {
            value: null,
            writable: true
        });

        /** -----------------------------------------------------------------------------
         *  Main relational property Get/Set definition defining pulling/pushing
         *  mechanics for relation linked to that property at the model description
         *
         *  As of documentation for Fy framework - description of Getter for property
         *  concludes to two types of output:
         *
         *  1) If getter called first time — it will be promise object required unboxing
         *
         *  2) If getter called second time - it will be resulting data of 1st step
         *  promise unwrapping
         *
         *  3) If getter called after <Entity>.refresh() operation then all relation
         *  properties will be turned into promise actions again
         *  -----------------------------------------------------------------------------
         */
        Object.defineProperty(inst, compiled.propertyName, {

            /** -------------------------------------------------------------------------
             *  Relation property main Getter
             *  -------------------------------------------------------------------------
             *  @returns {Promise|*}
             */
            get: function () : Promise<any> | any {

                let that: ModelInterface = this;

                // Return as async function if data is not presented yet
                // or refresh method was run prior to getter
                let resultPromise = async () => {

                    // Request pagination object from entity if any defined
                    const pagination = that.__usePaginationForNextRequest();
                    // Request seek object
                    const seek = that.__useSeekForNextRequest();
                    // Request order object
                    const order = that.__useOrderForNextRequest();

                    // Case for MTM when model should use MTM joiner first
                    if(compiled.joinModel !== null) {

                        /*
                         *  Go in the way of searching for some property inside the dependency
                         */
                        if(seek !== null) {

                            let resultsData: Array<any> = [];

                            /*
                             *  Execute seek for master relation
                             */
                            if(compiled.master) {

                                resultsData = await seekForMTMRelationMaster(
                                    seek, compiled, pagination, pullingModel, that);

                            }
                            /*
                             *  Execute seek for slave relation
                             */
                            else {

                                resultsData = await seekForMTMRelationSlave(
                                    seek, compiled, pagination, pullingModel, that);

                            }

                            // Apply results to the named storage for property
                            this[compiled.propertyStorageName] = resultsData;

                            // Set pull reference flag to true (pulled)
                            this[compiled.propertyStatusName] = true;

                        }
                        else if (order !== null) {

                            // Apply results to the named storage for property
                            this[compiled.propertyStorageName] = await performCrossListForMTM(compiled,
                                pullingModel, that, pagination, order);

                            // Set pull reference flag to true (pulled)
                            this[compiled.propertyStatusName] = true;

                        }
                        /*
                         *  Go in the way of simple fetch with no seek
                         */
                        else {

                            let listData: Array<number> = [];

                            if (compiled.master)

                                // Get Slave index data from sub collection
                                listData = await compiled
                                    .joinModel.getWhereMasterIs(this[compiled.selfPrimaryIndexName], pagination);

                            else

                                // Get Master index data from sub collection
                                listData = await compiled
                                    .joinModel.getWhereSlaveIs(this[compiled.selfPrimaryIndexName], pagination);

                            // Output many as default behavior for MTM
                            this[compiled.propertyStorageName] = await pullingModel
                                .findList(listData, 1, listData.length);

                            // Set pull reference flag to true (pulled)
                            this[compiled.propertyStatusName] = true;

                        }

                    }
                    // Case for simplified relation where Master related to slave by one value
                    else {

                        let searchValue: number | string = null;

                        // For master search value will be it's primary key
                        if(compiled.master)
                            searchValue = this[compiled.selfPrimaryIndexName];
                        // For slave search value will be stored property key
                        else
                            searchValue = this[compiled.propertyIdValueName];

                        // Define empty results object
                        let resultData: Object[] = [];

                        if(seek !== null) {

                            /*
                             * Perform seek and feel result object with Array of data
                             */
                            resultData = await seekForOTMRelation(
                                seek, compiled, searchValue, pagination, pullingModel);

                        }
                        /*
                         *  If seek interface is not defined - do simple search
                         */
                        else {

                            let page = 1;
                            let items = 100;

                            if(pagination.pagination) {
                                page = Math.floor(pagination.offset / pagination.items) + 1;
                                items = pagination.items;
                            }

                            // Take only one item if relation says
                            if (!compiled.outputMany)
                                items = 1;

                            resultData = await pullingModel
                                .findListForProperty(compiled.queryForProperty,
                                    searchValue, page, items, order);

                        }

                        //Output depends on what compiled relation saying
                        if (compiled.outputMany)
                            this[compiled.propertyStorageName] = resultData;
                        else
                            this[compiled.propertyStorageName] = resultData.length > 0 ? resultData[0] : null;

                        // Set pull reference flag to true (pulled)
                        this[compiled.propertyStatusName] = true;

                    }

                    // Return or cached or updated list of data
                    return this[compiled.propertyStorageName];

                };

                return resultPromise();

            },
            /** -------------------------------------------------------------------------
             *                       Relation property main setter
             *  -------------------------------------------------------------------------
             *  @param {ModelInterface | Array<ModelInterface>} setterData
             */
            set: function(setterData: ModelInterface & ModelInterface[]) {

                if(setterData !== null) {

                    // Check if relation expecting data to be list
                    if(compiled.outputMany) {

                        if(!Array.isArray(setterData))

                            throw new DataValidationException(
                                "Data you trying to set to entity should be format of Array");

                    }
                    // Check if relation expecting data to be certain type
                    else {

                        if(typeof setterData !== "object")

                            throw new DataValidationException(
                                "Data you trying to set to relation property [" + compiled.propertyName + "] " +
                                "is not the type of object and couldn't be applied as relationship " +
                                "type of presented data is: " + (typeof setterData));

                        if(typeof setterData.__isPersistent !== "function")

                            throw new DataValidationException(
                                "Data you trying to set to relation property [" + compiled.propertyName + "] " +
                                "is not the type of " +
                                "interface <ModelInterface>");

                        if(!setterData.__isPersistent())

                            throw new DataValidationException(
                                "Data you trying to set to relation property must be saved prior to be "+
                                "assigned as relation to the entity parameter: [" + compiled.propertyName + "]");

                        const passingStructure: TypedStructure = setterData.__getStructure();

                        const pIndex: PrimaryIndex = passingStructure.primaryIndex;

                        const e = setterData as any;

                        // Assign entity id to relation id storage property
                        this[compiled.propertyIdValueName] = e[pIndex.name];

                    }

                }

                // Apply data to setter storage
                if(this.__useExclusionForNextRequest())
                    // Assign data which should be excluded from relationship on next save/update
                    this[compiled.propertyExclusionName] = setterData;
                else
                    // Assign exact entity to relation storage property
                    this[compiled.propertyInclusionName] = setterData;

            }

        });

    },0);

}

/**
 * Perform cross search on list of parameters for MTM Models
 * ------------------------------------------------------------------------------
 * @param {CompiledRelation} relation
 * @param {ModelStaticInterface} model
 * @param {ModelInterface} instance
 * @param {PaginationInterface} pagination
 * @param {Array<OrderByModelInterface>} order
 * @returns {Promise<Array<any>>}
 */
async function performCrossListForMTM(relation: CompiledRelation,
                                      model: ModelStaticInterface,
                                      instance: ModelInterface,
                                      pagination: PaginationInterface,
                                      order: Array<OrderByModelInterface>) : Promise<Array<any>> {

    /*
     *  Define operational parameters
     */
    const slaveModel = relation.throughEntity;
    const thisModel: any = instance;

    /*
     *  Define working set to search into
     */
    let workingSetCount = 0;

    /*
     *  Select matching function for proper MTM data retrieval
     */
    let matchingFunction: (
        v: number,
        list: Array<number>,
        pagination?: PaginationInterface) => Promise<number[]> = null;

    // For master
    if(relation.master) {

        matchingFunction = relation.joinModel.getWhereMasterCrossingList;
        workingSetCount = await relation.joinModel.countMasterPairs(thisModel[relation.selfPrimaryIndexName]);

    }
    // For slave
    else {

        matchingFunction = relation.joinModel.getWhereSlaveCrossingList;
        workingSetCount = await relation.joinModel.countSlavePairs(thisModel[relation.selfPrimaryIndexName]);

    }

    const maxThread = 8;

    /*
     *  Define parameters for last section of data accumulation to cut-off if those
     *  will go over the limits
     */
    const offset = pagination.offset;
    const limit = pagination.items;

    /*
     *  Define iteration steps for search through chunks of data
     */
    const searchLimit = 10000;
    let searchPage = 1;

    /*
     *  Result counter (break results on fulfilment) and result array.
     */
    let results: Array<Object> = [];

    let resultsId: Array<number|string> = [];

    /* ------------------------------------------------------------------------------------
     *
     *                              RUN FETCH PIPELINES
     *
     *          For one of two cases
     *
     * ------------------------------------------------------------------------------------
     */

    /*
     *  The way to fetch pairs if those are under some limit number
     */
    if(workingSetCount < 32000) {

        /*
         *  Create new promise to retrieve all possible values
         */
        resultsId = await new Promise<Array<number | string>>((resolve, reject) => {

            if (relation.master) {

                relation.joinModel.getWhereMasterIs(
                    thisModel[relation.selfPrimaryIndexName],
                    {pagination: false, offset:0, items: 0})
                    .then(list => {

                        resolve(list);

                    }, err => {

                        resolve([]);

                    });

            } else {

                relation.joinModel.getWhereSlaveIs(
                    thisModel[relation.selfPrimaryIndexName],
                    {pagination: false, offset:0, items: 0})
                    .then(list => {

                    resolve(list);

                }, err => {

                    resolve([]);

                });

            }

        });

        /*
         *  Collect results based on matched set of ID's and ordered in a proper way
         */
        if (resultsId.length > 0)

            results = await model.findList(
                resultsId,
                (pagination.offset/pagination.items + 1),
                pagination.items,
                order
            );

    }
    /*
     *  The way to fetch pairs if those are above normal fetching limit number
     */
    else {

        /*
         *  Create new promise to retrieve all possible values
         */
        resultsId = await new Promise<Array<number | string>>((resolve, reject) => {

            let shouldBreak = false;
            let resId: Array<number | string> = [];
            let sent = 0;
            let received = 0;

            let sequences: { [key: number]: Array<any> } = {};
            let sequencesTotal = 0;
            let sequencesDataCount = 0;
            let sequenceDataLimit = limit * maxThread;

            const iv = setInterval(() => {

                /*
                 *  Clear polling mechanics when both of 2 events will happen (Exit sequence)
                 *
                 *      1) Stopping point will occur in execution
                 *      2) Threads will join on data
                 */
                if (shouldBreak && sent === received) {

                    // Close polling mechanics
                    clearInterval(iv);

                    // Define supposed size of selection for every next sequence in loop
                    let supposedSize = 0;
                    // Define current slice of data we going through
                    let currentOffset = 0;

                    // Take results is sequental manner from all of the sequences came back
                    for (let i = 1; i <= sequencesTotal; i++) {

                        const seq = sequences[i];

                        supposedSize = supposedSize + seq.length;

                        // Check supposed Size if it will be greater than offset
                        // during next loop
                        if (supposedSize >= offset) {

                            for (let k = 0; k < seq.length; k++) {

                                // Check if currentOffset is passing offset requirements for data
                                // will go to the result-set
                                if (currentOffset >= offset)
                                    resId.push(seq[k]);

                                // Increment current offset
                                currentOffset++;

                                // Check if container fulfilled enough for we can output results
                                if (resId.length >= limit)
                                    break;

                            }

                        }
                        // If in current sequence there is not applicable offset
                        // then just increment offset with empty data
                        else

                            currentOffset = supposedSize;

                        // Check if container fulfilled enough to be resolved, or go to next sequence
                        if (resId.length >= limit)
                            break;

                    }

                    // Resolve Promise
                    resolve(resId);

                }

                /*
                 *  Limit polling by current amount of working IO threads or if waiting for thread JOIN
                 *
                 *      1) Difference between active threads and finished threads should not be
                 *          greater than maximum allowed thread amount
                 *
                 *      2) If Should brake flag is true then interval will be awaiting for data JOIN
                 */
                if (((sent - received) >= maxThread) || shouldBreak)
                    return;

                // Record sequence Id for make ordered sequence data JOIN possible
                const sequenceId = searchPage;

                /*
                 *  @Promise
                 *  Lookup for list of data which obeys defined order for slave model
                 */
                slaveModel.__idList(searchPage++, searchLimit, order).then(res => {

                    // When we hit empty list of results we should signal that search came to the end
                    //  mark session as received, close thread session with return
                    if (res.length === 0) {
                        shouldBreak = true;
                        received++;
                        return;
                    }

                    /*
                     *  @Promise
                     *  Start cross search for master indexes
                     *  related to slave model indexes provided above
                     */
                    matchingFunction.call(relation.joinModel,
                        thisModel[relation.selfPrimaryIndexName],
                        res as Array<number>,
                        {
                            pagination: true,
                            offset: 0,
                            items: res.length
                        })
                        .then((related: number[]) => {

                            // If no cross information was found then confirm thread and close it
                            //  applying empty sequence data
                            if (related.length === 0) {

                                received++;
                                sequences[sequenceId] = [];
                                sequencesTotal++;

                                return;

                            }

                            // Define index of presence for that data
                            const resMap: { [key: number]: any } = {};

                            // Define sub-result container
                            let subResultIdList: Array<number | string> = [];

                            // Fill index of values that hits in
                            for (let k = 0; k < res.length; k++)

                                resMap[related[k]] = true;

                            /*
                             *  Apply search results according to map built for related entities
                             */

                            // Traverse slave data results to find entities that popping up in related search
                            for (let i = 0; i < res.length; i++) {

                                const r = res[i];

                                // Pass on entity of entity index isn't on a resMap
                                if (!resMap.hasOwnProperty(r))
                                    continue;

                                // Increment sequences counter
                                sequencesDataCount++;
                                subResultIdList.push(r);

                                // Check limits on current data count and defined limits for data collection
                                if (sequencesDataCount > offset + sequenceDataLimit)
                                    shouldBreak = true;

                            }

                            // Increment sequences with dataset
                            sequences[sequenceId] = subResultIdList;
                            sequencesTotal++;
                            received++;

                        }, (err: any) => {

                            received++;
                            sequences[sequenceId] = [];
                            sequencesTotal++;

                        });

                }, e => {

                    received++;
                    sequences[sequenceId] = [];
                    sequencesTotal++;

                });

                sent++;

            }, 2);

        });

        /*
         *  Check if some ID got back from cross-search.
         *  Pull available data from slave model related to list of those ID.
         */
        if (resultsId.length > 0)

            results = await model.findList(resultsId, 1, pagination.items, order);

    }

    return results;

}

/**
 * Will perform seek action for OTM relation data
 * ------------------------------------------------------------------------------
 * @param {SeekForRelationInterface} seek
 * @param {CompiledRelation} relation
 * @param {number | string} searchForRelationValue
 * @param {PaginationInterface} pagination
 * @param {ModelStaticInterface} model
 *
 * @returns {Promise<Array<Object>>}    : Outputs an array of data related to that
 *                                      search. After output other part of the
 *                                      program can decide what to do with that
 *                                      exact output.
 *
 *                                      Size of output may be adjusted with pagination
 */
async function seekForOTMRelation(seek: SeekForRelationInterface,
                                  relation: CompiledRelation,
                                  searchForRelationValue: number | string,
                                  pagination: PaginationInterface,
                                  model: ModelStaticInterface) : Promise<Array<any>> {

    const pRel = seek.query.prepend();
    pRel.relation = QueryConstructorRelation.And;

    const pVal = seek.query.prepend();
    pVal.name = relation.queryForProperty;
    pVal.value = searchForRelationValue;
    pVal.operator = QueryConstructorOperators.Equal;

    seek.query.limit = pagination.items;
    seek.query.offset = pagination.offset;

    const rawQuery = seek.executor.__constructQuery(seek.query);

    return await model.query().raw(rawQuery).getMany();

}

/**
 * Will perform seek action for MTM relation master data
 * ------------------------------------------------------------------------------
 * @param {SeekForRelationInterface} seek
 * @param {CompiledRelation} relation
 * @param {PaginationInterface} pagination
 * @param {ModelStaticInterface} model
 * @param {ModelInterface | any} instance
 * @returns {Promise<Array<any>>}
 */
async function seekForMTMRelationMaster(seek: SeekForRelationInterface,
                                        relation: CompiledRelation,
                                        pagination: PaginationInterface,
                                        model: ModelStaticInterface,
                                        instance: ModelInterface|any) : Promise<Array<any>> {

    const offset = pagination.offset;
    const limit = pagination.items;

    let resultCount: number = 0;
    let results: Array<Object> = [];
    let listData: Array<number> = [];

    const seekPagination: PaginationInterface = {
        pagination: true,
        offset: 0,
        items: 4096
    };

    /*
     *  Iterate over 9 billion tryouts to get data from some relational structure
     */
    for(let i = 0; i < 9999999999; i++) {

        seekPagination.offset = i * seekPagination.items;

        // Get Master index data from sub collection
        listData = await relation.joinModel
            .getWhereMasterIs(instance[relation.selfPrimaryIndexName], seekPagination);

        if(listData.length === 0)
            break;

        // Clone seek object to swap search values
        const clonedSeek = seek.query.clone();

        // Start with appending AND query to the seek Constructor
        const pRel = clonedSeek.prepend();
        pRel.relation = QueryConstructorRelation.And;

        // Next append named search for Constructor to find indexed values of some property
        const pVal = clonedSeek.prepend();
        pVal.name = relation.queryForProperty;
        pVal.value = listData;
        pVal.operator = QueryConstructorOperators.In;

        // Append next pagination
        clonedSeek.limit = limit;
        clonedSeek.offset = offset;

        const rawQuery = seek.executor.__constructQuery(clonedSeek);

        // Output RAW query for debug purposes
        // console.log(JSON.stringify(rawQuery));

        // Run RAW query
        let data = await model.query().raw(rawQuery).getMany();

        // If results not empty
        if(data.length > 0) {

            // Calculate if result count will be sufficient to start accumulating results
            const supposedResultValue = resultCount + data.length;

            // Check if supposed counter will be greater than offset during current data iteration
            if(supposedResultValue >= offset) {

                // Traverse results if allowed
                for (let di = 0; di < data.length; di++) {

                    // Append to counter
                    resultCount++;

                    // If offset point was passed - apply results
                    if(resultCount >= offset)

                        results.push(data[di]);

                    // If results is pretty over the limit then break
                    if(results.length >= limit)

                        break;

                }

            }
            // If counter is not yet reachable for offset then just iterate counter value
            else

                resultCount = supposedResultValue;

        }

        // Break if results are over the limit
        if(results.length >= limit)

            break;

    }

    return results;

}

/**
 * Will perform seek action for MTM relation slave data
 * ------------------------------------------------------------------------------
 * @param {SeekForRelationInterface} seek
 * @param {CompiledRelation} relation
 * @param {PaginationInterface} pagination
 * @param {ModelStaticInterface} model
 * @param {ModelInterface | any} instance
 * @returns {Promise<Array<Object>>}
 */
async function seekForMTMRelationSlave(seek: SeekForRelationInterface,
                                       relation: CompiledRelation,
                                       pagination: PaginationInterface,
                                       model: ModelStaticInterface,
                                       instance: ModelInterface|any) {

    const offset = pagination.offset;
    const limit = pagination.items;

    let resultCount: number = 0;
    let results: Array<Object> = [];
    let listData: Array<number> = [];

    const seekPagination: PaginationInterface = {
        pagination: true,
        offset: 0,
        items: 4096
    };

    /*
     *  Iterate over 9 billion tryouts to get data from some relational structure
     */
    for(let i = 0; i < 9999999999; i++) {

        seekPagination.offset = i * seekPagination.items;

        // Get Slave index data from sub collection
        listData = await relation.joinModel
            .getWhereSlaveIs(instance[relation.selfPrimaryIndexName], seekPagination);

        // Break search if list data has no new Identifiers
        if(listData.length === 0)
            break;

        // Clone seek object to swap search values
        const clonedSeek = seek.query.clone();

        // Start with appending AND query to the seek Constructor
        const pRel = clonedSeek.prepend();
        pRel.relation = QueryConstructorRelation.And;

        // Next append named search for Constructor to find indexed values of some property
        const pVal = clonedSeek.prepend();
        pVal.name = relation.queryForProperty;
        pVal.value = listData;
        pVal.operator = QueryConstructorOperators.In;

        // Append next pagination
        clonedSeek.limit = limit;
        clonedSeek.offset = offset;

        // Construct RAW query using attached constructor
        const rawQuery = seek.executor.__constructQuery(clonedSeek);

        // Run RAW query
        let data = await model.query().raw(rawQuery).getMany();

        // If results not empty
        if(data.length > 0) {

            // Calculate if result count will be sufficient to start accumulating results
            const supposedResultValue = resultCount + data.length;

            // Check if supposed counter will be greater than offset during current data iteration
            if(supposedResultValue >= offset) {

                // Traverse results if allowed
                for (let di = 0; di < data.length; di++) {

                    // Append to counter
                    resultCount++;

                    // If offset point was passed - apply results
                    if(resultCount >= offset)

                        results.push(data[di]);

                    // If results is pretty over the limit then break
                    if(results.length >= limit)

                        break;

                }

            }
            // If counter is not yet reachable for offset then just iterate counter value
            else

                resultCount = supposedResultValue;

        }

        // Break if results are over the limit
        if(results.length >= limit)

            break;

    }

    return results;

}

/**
 * Fetch 2 relational Tables by some property name
 * ------------------------------------------------------------------------------
 * @param {string} propName
 * @param {TypedStructure} mainStructure
 * @param {TypedStructure} slaveStructure
 * @param {QueryExecutorInterface} executor
 * @param {PaginationInterface} pagination
 * @returns {Promise<void>}
 */
export async function fetchRelationsForPropertyName(propName: string,
                                                    mainStructure: TypedStructure,
                                                    slaveStructure: TypedStructure,
                                                    executor: QueryExecutorInterface,
                                                    pagination: PaginationInterface) {


    const rel = mainStructure.getRelationForName(propName);

    const rel2 = slaveStructure.getRelationForName(rel.foreignPropertyName);

    const joiner = rel.joinModel;

    /*
     *  Resolve OTM case
     */
    if(joiner === null)

        return fetchRelationsForPropertyOTM(rel, rel2, executor, pagination);

    /*
     *  Resolve MTM case
     */
    else

        return fetchRelationsForPropertyMTM(rel, rel2, executor, pagination);

}

/**
 * Fetch relations for OTM property
 * ------------------------------------------------------------------------------
 * @param {CompiledRelation} rel
 * @param {CompiledRelation} slaveRel
 * @param {QueryExecutorInterface} executor
 * @param {PaginationInterface} pagination
 * @returns {Promise<any>}
 */
async function fetchRelationsForPropertyOTM(rel: CompiledRelation,
                                            slaveRel: CompiledRelation,
                                            executor: QueryExecutorInterface,
                                            pagination: PaginationInterface) {

    const constructor = executor.getConstructorProduct();

    /*
     *  Check if some OTM relation is master
     *  ----------------------------------------------------------------------------
     *  looking for a slave relation to fulfill fetch operation
     */
    if(rel.master) {

        const pRel = constructor.prepend();
        pRel.relation = QueryConstructorRelation.And;

        const pVal = constructor.prepend();
        pVal.name = rel.queryForProperty;
        pVal.value = null;
        pVal.operator = QueryConstructorOperators.NotEqual;

        constructor.limit = pagination.items;
        constructor.offset = pagination.offset;

        const rawQuery = executor.__constructQuery(constructor);

        // Return array if items required to be multiple
        if(pagination.items > 1)

            return await rel.throughEntity.query().raw(rawQuery).getMany();

        // Otherwise return single query object
        else

            return await rel.throughEntity.query().raw(rawQuery).getOne();

    }
    /*
     *  Check if some relation is slave
     *  ----------------------------------------------------------------------------
     *  If presented then cycle should pop-up relations which
     *  applicable to be searched and then fulfill the designated
     *  boundaries for the search
     */
    else {

        const resultsPrimaryIndex = slaveRel.selfPrimaryIndexName;
        let seekOffset = 0;
        let seekItems = 8192;
        let resultCount = 0;
        let results: Array<Object> = [];
        let resultsReducer: {[key: string] : boolean} = {};
        let limit = pagination.items;
        let offset = pagination.offset;

        /*
         *  Iterate over 9 billion tryouts to get data from some relational structure
         */
        for(let i = 0; i < 9999999999; i++) {

            // @TODO Heavy: This operation is clearly heavy and excessive for that behavior
            // @TODO Heavy: and should be reduced to only required parameters projection implementation
            let entityList = await rel.selfEntity
                .query<any>()
                .construct({
                    pagination: true,
                    offset: seekOffset++ * seekItems,
                    items: seekItems
                })
                .where(slaveRel.queryForProperty)
                .notEquals(null)
                .getMany();

            if(entityList.length === 0)

                break;

            const inList = [];
            const reducer: {[key: string]: boolean} = {};

            for(const e of entityList) {

                const prop = e[rel.propertyIdValueName];

                if(!reducer.hasOwnProperty(prop)) {
                    reducer[prop] = true;
                    inList.push(prop);
                }

            }

            // Clone seek object to swap search values
            const clonedSeek = constructor.clone();

            // Start with appending AND query to the seek Constructor
            const pRel = clonedSeek.prepend();
            pRel.relation = QueryConstructorRelation.And;

            // Next append named search for Constructor to find indexed values of some property
            const pVal = clonedSeek.prepend();
            pVal.name = rel.queryForProperty;
            pVal.value = inList;
            pVal.operator = QueryConstructorOperators.In;

            // Append next pagination
            clonedSeek.limit = limit * 2;
            clonedSeek.offset = 0;

            // Construct RAW query using attached constructor
            const rawQuery = executor.__constructQuery(clonedSeek);

            const data = await rel.throughEntity.query().raw(rawQuery).getMany();

            // If results not empty
            if(data.length > 0) {

                // Calculate if result count will be sufficient to start accumulating results
                const supposedResultValue = resultCount + data.length;

                // Check if supposed counter will be greater than offset during current data iteration
                if(supposedResultValue >= offset) {

                    // Traverse results if allowed
                    for (let di = 0; di < data.length; di++) {

                        const res: any = data[di];
                        const index: string|number = res[resultsPrimaryIndex];

                        // Append to counter
                        resultCount++;

                        // If offset point was passed - reduce and apply results
                        if(resultCount >= offset) {

                            if(!resultsReducer.hasOwnProperty(index)) {
                                resultsReducer[index] = true;
                                results.push(res);
                            }

                        }

                        // If results is pretty over the limit then break
                        if(results.length >= limit)

                            break;

                    }

                }
                // If counter is not yet reachable for offset then just iterate counter value
                else

                    resultCount = supposedResultValue;

            }

            // Break if results are over the limit
            if(results.length >= limit)

                break;

        }

        // Return array if items required to be multiple
        if(pagination.items > 1)

            return results;

        // Return one result if it was required to have one result
        else if(results.length > 0)

            return results[0];

        // Return Null if nothing fits
        return null;

    }

}

/**
 * Fetch relations for MTM property
 * ------------------------------------------------------------------------------
 * @param {CompiledRelation} rel
 * @param {CompiledRelation} slaveRel
 * @param {QueryExecutorInterface} executor
 * @param {PaginationInterface} pagination
 * @returns {Promise<Array<Object>>}
 */
async function fetchRelationsForPropertyMTM(rel: CompiledRelation,
                                            slaveRel: CompiledRelation,
                                            executor: QueryExecutorInterface,
                                            pagination: PaginationInterface) {

    const resultsPrimaryIndex = slaveRel.selfPrimaryIndexName;
    const seek = executor.getConstructorProduct();

    const offset = pagination.offset;
    const limit = pagination.items;

    let resultCount: number = 0;
    let results: Array<Object> = [];
    let resultsReducer: {[key: string] : boolean} = {};
    let listData: Array<number> = [];

    const seekPagination: PaginationInterface = {
        pagination: true,
        offset: 0,
        items: 8192
    };

    /*
     *  Iterate over 9 billion tryouts to get data from some relational structure
     */
    for(let i = 0; i < 9999999999; i++) {

        seekPagination.offset = i * seekPagination.items;

        if(rel.master) {

            // Get Slave index data from sub collection for master relation
            listData = await rel.joinModel
                .listSlave(seekPagination);

        } else {

            // Get master index data from sub collection for slave relation
            listData = await rel.joinModel
                .listMaster(seekPagination);

        }

        // Break search if list data has no new Identifiers
        if(listData.length === 0)
            break;

        // Clone seek object to swap search values
        const clonedSeek = seek.clone();

        // Start with appending AND query to the seek Constructor
        const pRel = clonedSeek.prepend();
        pRel.relation = QueryConstructorRelation.And;

        // Next append named search for Constructor to find indexed values of some property
        const pVal = clonedSeek.prepend();
        pVal.name = rel.queryForProperty;
        pVal.value = listData;
        pVal.operator = QueryConstructorOperators.In;

        // Append next pagination
        clonedSeek.limit = limit;
        clonedSeek.offset = offset;

        // Construct RAW query using attached constructor
        const rawQuery = executor.__constructQuery(clonedSeek);

        // Run RAW query
        let data = await rel.throughEntity.query().raw(rawQuery).getMany();

        // If results not empty
        if(data.length > 0) {

            // Calculate if result count will be sufficient to start accumulating results
            const supposedResultValue = resultCount + data.length;

            // Check if supposed counter will be greater than offset during current data iteration
            if(supposedResultValue >= offset) {

                // Traverse results if allowed
                for (let di = 0; di < data.length; di++) {

                    const res: any = data[di];
                    const index: string|number = res[resultsPrimaryIndex];

                    // Append to counter
                    resultCount++;

                    // If offset point was passed - reduce and apply results
                    if(resultCount >= offset) {

                        if(!resultsReducer.hasOwnProperty(index)) {
                            resultsReducer[index] = true;
                            results.push(res);
                        }

                    }

                    // If results is pretty over the limit then break
                    if(results.length >= limit)

                        break;

                }

            }
            // If counter is not yet reachable for offset then just iterate counter value
            else

                resultCount = supposedResultValue;

        }

        // Break if results are over the limit
        if(results.length >= limit)

            break;

    }

    /*
     *  If it was asked to return limit of 1 then return as a singular object
     */
    if(results.length > 0 && pagination.items === 1)

        return results[0];

    return results;

}