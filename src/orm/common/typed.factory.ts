/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Factory for create Typed properties for Typed
 *              structure objects
 *
 ***************************************************************/
import {DatabaseConfigurationException, PropertyType, Relations} from "./model";
import {ConfigurationException} from "../../exceptions/exceptions";
import {TypedProperty} from "./typed.structure";
import {
    BelongsToManyRelation, BelongsToOneRelation, HasManyRelation, HasOneRelation,
    RelationDefinition
} from "./relations";
import {Type} from "../../app";

/**
 * TypedProperty factory
 * -----------------------------------------------------------------------------
 *
 * API for creation of Typed Properties for Typed Schema
 */
export class TypedPropertyFactory {

    /**
     * Will create Typed Structure property from relation parameters
     * -------------------------------------------------------------------------
     * @param instance
     * @param {string} key
     * @param type
     * @param {Relations} rType
     * @param {Function} proto
     * @param through
     * @param {boolean} weak
     * @param {boolean} isUnique
     * @param {Array<string>} isUniqueWith
     * @returns {TypedProperty}
     */
    public static createFromRelationDefinition(instance: any,
                                               key: string,
                                               type: any,
                                               rType: Relations,
                                               proto: Function,
                                               through: any = null,
                                               weak: boolean = false,
                                               isUnique: boolean = false,
                                               isUniqueWith: Array<string> = []) : TypedProperty {

        let property = new TypedProperty();

        let column = key;

        let desiredType = null;

        let relation: RelationDefinition = null;

        let relThrough: string = null;

        if(typeof through !== "undefined" && through !== null)
            relThrough = through;

        if(typeof proto !== "function")

            throw new DatabaseConfigurationException(
                "\nWrong relation target passed to relation definition. "+
                "Target should be function returns Type which model is related to");

        if(typeof proto !== "function")

            throw new DatabaseConfigurationException(
                "\nRelation cannot determine type of relationship provided. "+
                "You should provide only Type you want your model to be related");

        if(rType === Relations.hasMany) {

            relation = new HasManyRelation();

        }
        else if(rType === Relations.hasOne) {

            relation = new HasOneRelation();

        }
        else if(rType === Relations.belongsToMany) {

            relation = new BelongsToManyRelation();

        }
        else if(rType === Relations.belongsToOne) {

            relation = new BelongsToOneRelation();

        }
        else
            throw new DatabaseConfigurationException("\nRelation type was not recognized by Relation Schema");

        // Set relation as unresolved
        relation.__unresolvedRef = proto;
        relation.foreignPropertyName = relThrough;
        relation.sourcePropertyName = key;
        relation.weak = weak;

        // Get parent class name from property
        property.parentName = instance.constructor.name;
        property.column = column;
        property.name = key;
        property.relation = relation;

        // If property should be Unique by the index
        if(isUnique)
            property.unique = true;

        // If property should be Unique indexed with some other parameter (relation)
        if(isUniqueWith)
            property.secondaryMergeKeys = isUniqueWith;

        // Align Types by merging desirable Types from Definition with TS system types
        property.type = this.mergeTypeDefinitions(property, type, desiredType);

        return property;

    }

    /**
     * Will create Typed Property from set of parameters
     * -------------------------------------------------------------------------
     * @param {Object} instance
     * @param {string} key
     * @param type
     * @param {PropertyType} special
     * @returns {TypedProperty}
     */
    public static createFromPropertyDefinition(instance: any,
                                               key: string,
                                               type: any,
                                               special: PropertyType) : TypedProperty {

        let property = new TypedProperty();

        let column = key;

        let desiredType = null;

        /*
         *  Check Property Type parameter for some values which
         *  may extend list of property required schema options
         */
        if(special) {

            /* ------------------------------------------------
             *
             *             COMMON PROPERTIES
             *
             ------------------------------------------------ */

            if(typeof special.column !== "undefined")
                column = String(special.column);

            if(typeof special.type !== "undefined")
                desiredType = Number(special.type);

            if(typeof special.length !== "undefined")
                property.length = Number(special.length);

            if(typeof special.unique !== "undefined")
                property.unique = Boolean(special.unique);

            // If entity should have primary index
            if(typeof special.primary !== "undefined")
                property.primary = Boolean(special.primary);

            // If index should do auto-incrementing it's value
            if(typeof special.auto !== "undefined")
                property.auto = Boolean(special.auto);

            // If that property should be indexed
            if(typeof special.index !== "undefined")
                property.secondary = Boolean(special.index);

            // If property should have this default value
            if(typeof special.default !== "undefined")
                property.defaultValue = special.default;

            // If property should not be empty or null
            if(typeof special.notNull !== "undefined")
                property.notNull = Boolean(special.notNull);

            // If property should not be empty or null
            if(typeof special.expose !== "undefined")
                property.expose = Boolean(special.expose);

            // If property should not be empty or null
            if(typeof special.protect !== "undefined")
                property.protect = Boolean(special.protect);

            // Secondary index compound keys definition (special.index should be defined)
            if(typeof special.indexWith !== "undefined") {

                // Check if passed values is Array values
                if(Array.isArray(special.indexWith))

                // Append to Typed property 1 by 1
                    special.indexWith.map(iwProperty => property.secondaryMergeKeys.push(iwProperty));

            }

        }

        // Get parent class name from property
        property.parentName = instance.constructor.name;
        property.column = column;
        property.name = key;

        // Align Types by merging desirable Types from Definition with TS system types
        property.type = this.mergeTypeDefinitions(property, type, desiredType);

        return property;

    }

    /**
     * Merge types when one or more presented, will assign default String type
     * if no suitable matches will be found for existing classes
     * -------------------------------------------------------------------------
     * @param {TypedProperty} property
     * @param systemType
     * @param {Type} definitionType
     * @returns {Type}
     */
    private static mergeTypeDefinitions(property: TypedProperty, systemType: any, definitionType?: Type) {

        if(systemType === String) {

            if(definitionType !== null && Type.String !== definitionType)

                throw new ConfigurationException(
                    "\nType of parameter: "+ property.name
                    +"\nis not correlated with narrowed type: " + Type[definitionType] + " [" +definitionType+ "] "
                    +"\nat entity: " + property.parentName +"\n",
                    400);

            return Type.String;
        }
        else if(systemType === Number) {

            if(definitionType !== null)

                if(Type.Int !== definitionType && Type.Float !== definitionType)

                    throw new ConfigurationException(
                        "\nType of parameter: "+ property.name
                        +"\nis not correlated with narrowed type: " + Type[definitionType] + " [" +definitionType+ "] "
                        +"\nat entity: " + property.parentName +"\n",
                        400);

            return definitionType;

        }
        else if(systemType === Boolean) {

            if(definitionType !== null && Type.Bool !== definitionType)

                throw new ConfigurationException(
                    "\nType of parameter: "+ property.name
                    +"\nis not correlated with narrowed type: " + Type[definitionType] + " [" +definitionType+ "] "
                    +"\nat entity: " + property.parentName +"\n",
                    400);

            return Type.Bool;

        }
        else if(systemType === Array) {

            if(definitionType !== null && Type.Array !== definitionType)

                throw new ConfigurationException(
                    "\nType of parameter: "+ property.name
                    +"\nis not correlated with narrowed type: " + Type[definitionType] + " [" +definitionType+ "] "
                    +"\nat entity: " + property.parentName +"\n",
                    400);

            return Type.Array;

        }
        else if(systemType === Date) {

            if(definitionType !== null && Type.Date !== definitionType)

                throw new ConfigurationException(
                    "\nType of parameter: "+ property.name
                    +"\nis not correlated with narrowed type: " + Type[definitionType] + " [" +definitionType+ "] "
                    +"\nat entity: " + property.parentName +"\n",
                    400);

            return Type.Date;

        }
        else if(systemType === Object) {

            // Check if Type narrowed to Map which should be default object behavior
            if(definitionType !== null && Type.Map !== definitionType)

                throw new ConfigurationException(
                    "\nType of parameter: "+ property.name
                    +"\nis not correlated with narrowed type: " + Type[definitionType] + " [" +definitionType+ "] "
                    +"\nat entity: " + property.parentName +"\n",
                    400);

            // If type is not narrowed for Object System type then it is ANY type
            // which should be narrowed to string
            else if(definitionType === null)

                return Type.String;

            return Type.Map;

        }

        return Type.String;

    }

    /**
     * Function Name to String
     * -------------------------------------------------------------------------
     * @param {Function} func
     * @returns {string}
     */
    private static functionNameToString(func: Function) : string {

        let classNameRegEx = /(?:\S+\s+){1}([a-zA-Z_$][0-9a-zA-Z_$]*)/;
        return  classNameRegEx.exec(func.toString())[1];

    }

}