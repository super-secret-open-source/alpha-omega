/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Structure maintaining ORM object definitions
 *              and relationship blueprint structures
 *
 ***************************************************************/
import {
    DatabaseConfigurationException,
    ModelCounterInterface,
    ModelSource,
    StructureMongoInterface,
    StructureMYSQLInterface
} from "./model";
import {CompiledRelation, RelationDefinition, Relationship} from "./relations";
import {Type} from "../../app";
/**
 * Typed structure representing some Entity which Mapped to Permanent storage
 * -------------------------------------------------------------------------
 *
 * Structure is a unified object which can be utilized through multiple
 * types of Database software solutions.
 */
export class TypedStructure implements StructureMongoInterface, StructureMYSQLInterface {

    readonly id: number = 0;

    public foreignDependency: boolean = false;

    public foreignDependencyNames: Array<string> = [];

    public foreignCascadeDependency: boolean = false;

    public foreignCascadeDependencyNames: Array<string> = [];

    public sourceStorageType: ModelSource = null;

    public connectionName: string = null;

    public counter: ModelCounterInterface = null;

    public connection: any = null;

    public className: string = null;

    public classPrototype: any = null;

    public properties: {[key:string] : TypedProperty} = {};

    public propertyList: Array<TypedProperty> = [];

    public propertyIndex: Set<string> = new Set();

    public primaryIndex: PrimaryIndex = null;

    public secondaryIndex: Map<string, SecondaryIndex> = new Map();

    public relationStructure: Relationship = null;

    private relations: {[key:string]: CompiledRelation} = {};

    private awaitsForIndexAligning: Map<string, SecondaryIndex> = new Map();

    private initializedForSchema: boolean = false;

    /**
     * Define new structure with id
     * ---------------------------------------------------------------------
     * @param {number} id
     */
    constructor(id: number) {

        this.id = id;

    }

    /**
     * Get structure id
     * ---------------------------------------------------------------------
     * @returns {number}
     */
    public getId() : number {

        return this.id;

    }

    /**
     * Get property entity for some name
     * ---------------------------------------------------------------------
     * @param {string} name
     * @returns {TypedProperty | null}
     */
    public getProperty(name: string) : TypedProperty | null {

        if(this.properties.hasOwnProperty(name))

            return this.properties[name];

        return null;

    }

    /**
     * Check if some property exists on the structure
     * ---------------------------------------------------------------------
     * @param {string} name
     * @returns {boolean}
     */
    public hasProperty(name: string) : boolean {

        return this.properties.hasOwnProperty(name);

    }

    /**
     * Will set compiled relation
     * ---------------------------------------------------------------------------------
     * @param {string} name
     * @param {CompiledRelation} rel
     */
    public setRelationForPropertyName(name: string, rel: CompiledRelation) {

        if(!this.properties.hasOwnProperty(name))

            throw new DatabaseConfigurationException(
                "Cannot set new relation for Structure of: " + this.className +
                " property " + name + " is not exists for parent model");

        this.relations[name] = rel;

    }

    /**
     * Will retrieve compiled relation for desired name
     * ---------------------------------------------------------------------------------
     * @param {string} name
     * @returns {CompiledRelation}
     */
    public getRelationForName(name: string) : CompiledRelation | null {

        if(this.relations.hasOwnProperty(name)) {
            return this.relations[name];
        }

        return null;

    }

    /**
     * Will assign one property to a structure
     * ---------------------------------------------------------------------
     * @param {TypedProperty} property
     */
    public assignProperty(property: TypedProperty) : void {

        this.className = property.getClassName();

        //this.properties.set(property.name, property);

        this.properties[property.name] = property;

        this.propertyIndex.add(property.name);

        this.propertyList.push(property);

        this.sourceStorageType = property.parentObjectModelStorage;

        /*
         * Check if property is relation (it should be indexed as relation then)
         */
        if(property.isRelation()) {

            if(this.relationStructure === null)
                this.relationStructure = new Relationship();

            property.relation.sourceStructure = this;

            this.relationStructure.submit(property.relation);

        }

        /*
         * Check if property marked as primary index property
         */
        if(property.primary) {

            let pi = new PrimaryIndex();
            pi.name = property.name;
            pi.type = property.type;

            if(property.auto)
                pi.auto = true;

            this.primaryIndex = pi;

        }
        /*
         *  Check if property having one or more secondary indexes which should be
         *  assembled for further propagation to Database representation layer
         */
        else if (property.secondary) {

            let sec = new SecondaryIndex();

            sec.fieldSet.push(property.name);

            sec.name = property.name;

            sec.unique = property.unique;

            let tryouts = 1;

            /*
             *  Create secondary index name, if name exists then modify it
             *  to next _index value
             */
            while (this.awaitsForIndexAligning.has(property.name)) {
                sec.name = sec.name + "_" + tryouts++;
            }

            /*
             *  Create secondary index merged context
             *  parsing array of keys provided as merge desired
             */
            if(property.secondaryMergeKeys.length > 0) {

                property.secondaryMergeKeys.map(key => {

                    if(key !== property.name) {

                        sec.fieldSet.push(key);

                    }

                });

            }

            /*
             *  Put created secondary indexes on wait list until schema
             *  will be defined on the first run or at the similar action
             */
            this.awaitsForIndexAligning.set(sec.name, sec);

        }

    }

    /**
     *  Required method to call prior to schema build
     *  ------------------------------------------------------------
     *  Will proceed with aligning of rest Indexes defined for Schema
     */
    public initForSchema() {

        // Align all indexes which waits for being validated to schema
        this.awaitsForIndexAligning.forEach((element) => {

            let falseDefinitions: number = 0;
            let signature: string = "";

            // Check every field to be presented on schema
            element.fieldSet.map(propName => {

                //if(!this.properties.has(propName))
                if(!this.properties.hasOwnProperty(propName))

                    falseDefinitions++;

                signature += propName + ',';

            });

            // If no invalid indexes presented then apply index to secondary index
            if (falseDefinitions === 0)

                this.secondaryIndex.set(element.name, element);

            // If found wrong set of definitions on property - stop program and notify
            else
                throw new DatabaseConfigurationException(
                    "\nYour set if indexes at schema for entity:\n " + this.className + " " +
                    "\nand property index:\n " + element.name + "\nwhich having signature:\n[" + signature + "]\n"+
                    "cannot be aligned to schema because your entity lacks one or more of properties defined " +
                    "for that index. \nPlease fix @Property({indexWith:[param1,... definition\n");

        });

        this.initializedForSchema = true;

    }
    
}

/**
 * Typed property of some structure
 * -------------------------------------------------------------------------
 */
export class TypedProperty {

    public parentObjectModelStorage: ModelSource = null;

    public parentName: string = null;

    public name: string = null;

    public column: string = null;

    public type: Type = Type.String;

    public defaultValue: any = null;

    public length: number = -1;

    public primary: boolean = false;

    public auto: boolean = false;

    public primaryMergeKeys: Array<string> = [];

    public secondary: boolean = false;

    public secondaryMergeKeys: Array<string> = [];

    public unique: boolean = false;

    public notNull: boolean = false;

    public relation: RelationDefinition = null;

    public expose: boolean = true;

    public protect: boolean = false;

    public getClassName() : string {

        return this.parentName;

    }

    public isRelation() : boolean {

        return this.relation !== null;

    }

}

/**
 * Primary index entity
 * -------------------------------------------------------------------------
 *
 * Particle of Typed Property
 */
export class PrimaryIndex {

    public name: string = null;

    public auto: boolean = false;

    public type: Type = null;

}

/**
 * Secondary index entity
 * -------------------------------------------------------------------------
 *
 * Particle of Typed Property
 */
export class SecondaryIndex {

    public name: string = "_def";

    public unique: boolean = false;

    public fieldSet: Array<string> = [];

}