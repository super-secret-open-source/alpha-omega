/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Describes all interfaces related to Model
 *              interaction in ORM
 *
 ***************************************************************/
import {PrimaryIndex, SecondaryIndex, TypedProperty, TypedStructure} from "./typed.structure";
import {QueryExecutorInterface, QueryInterface} from "./query/query";
import {Collection, DeleteWriteOpResultObject, UpdateWriteOpResult} from "mongodb";
import {Exception} from "ts-exceptions";
import {Type} from "../../app";
import {CompiledRelation} from "./relations";
import {
    QueryConstructorExecutive,
    QueryConstructorNonExecutive,
    QueryConstructorProduct
} from "./query/query.constructor";

/**
 * Abstracted model
 * -----------------------------------------------------------------------------
 * Can create structures of some type for Models can represent entities from
 * some Database
 */
export abstract class Model {

    private static counter = 1;

    /**
     * Create new structure
     * -------------------------------------------------------------------------
     * @param {TypedProperty} property
     * @returns {TypedStructure}
     * @private
     */
    protected static __createNewStructure(property: TypedProperty) : TypedStructure {

        let struct = new TypedStructure(this.counter++);

        struct.assignProperty(property);

        ModelStorage.addStructure(struct);

        return struct;

    }

    /**
     * Create new structure by name
     * -------------------------------------------------------------------------
     * @param name
     * @private
     */
    protected static __createNewStructureForName(name: string) : TypedStructure {

        let struct = new TypedStructure(this.counter++);

        struct.className = name;

        ModelStorage.addStructure(struct);

        return struct;

    }

}

/**
 * Model Storage
 * -----------------------------------------------------------------------------
 * Will do storing references of Model Structure defined for every Model in the
 * system.
 */
export class ModelStorage {


    private static ref: Map<number, TypedStructure> = new Map();

    /**
     * Add newly created structure to Model Storage
     * -------------------------------------------------------------------------
     * @param {TypedStructure} ts
     */
    public static addStructure(ts: TypedStructure) : void {
        this.ref.set(ts.getId(), ts);
    }

    /**
     * Get Structure by Id or get null reference
     * -------------------------------------------------------------------------
     * @param {number} id
     * @returns {TypedStructure|null}
     */
    public static getStructureById(id: number) : TypedStructure | null {

        if(this.ref.has(id))
            return this.ref.get(id);
        return null;

    }

}

/**
 * Possible model relation types
 * -------------------------------------------------------------------------
 * Enumerated list of relation types with different behavior
 *
 *      - hasOne    : will end in having one -> one/many -> relation master
 *      - hasMany   : will end in having many -> one/many -> relation master
 *      - belongsToOne  : will end in slave relation one -> many/one
 *      - belongsToMany : will end in slave relation many -> many/one
 *
 *      Those can be joined to form pretty complex list of relations for
 *      create different sort of structure behaviors
 *
 */
export enum Relations {
    hasOne,
    hasMany,
    belongsToOne,
    belongsToMany
}

/**
 * Possible source services for Model data
 * -------------------------------------------------------------------------
 */
export enum ModelSource {
    Mongo,
    MySQL,
    Postgres
}

/**
 * Defines Model definition
 * -------------------------------------------------------------------------
 */
export interface ModelType {
    sourceDb: string;
}

/**
 * Common property for a Model structure
 * -------------------------------------------------------------------------
 */
export interface PropertyType {
    column?: string;
    default?: string;
    type?: Type;
    length?: number;
    primary?: boolean;
    auto?: boolean;
    index?: boolean;
    indexWith?: Array<string>;
    unique?: boolean;
    readonly?: boolean;
    notNull?: boolean;
    relation?: RelationProperty;
    expose?:boolean;
    protect?:boolean;
}

/**
 * Definition for slave relation side
 * -------------------------------------------------------------------------
 * Can have one or few of parameters
 *
 *  - through
 *  - weak
 *  - unique
 *  - uniqueWith
 */
export interface BelongsRelationPropertyType {
    through?: string,
    weak?: boolean,
    unique?: boolean,
    uniqueWith?: Array<string>
}

/**
 *
 * -------------------------------------------------------------------------
 */
export interface RelationProperty {
    hasOne?: any,
    hasMany?: any,
    hasThrough?: string,
    belongsToOne?: any,
    belongsToMany?: any,
    belongsThrough?: string
}

/**
 *  Model Interface
 *  -----------------------------------------------------------------------
 *  Defines what methods is some DB Model require to be implemented
 */
export interface ModelInterface {

    paginate(itemsOnPage: number, page?: number) : this;
    orderBy(properties: Array<OrderByModelInterface>) : this;
    seek() : QueryConstructorNonExecutive<this>;

    fromObject(data: {[key:string]:any}) : this;
    fromJSON(data:string) : this;
    toObject() : {[key:string]:any};
    toJSON() : string;

    save<T>() : Promise<T>;
    update<T>() : Promise<T>;
    delete<T>() : Promise<T>;
    refresh<T>() : Promise<T>;

    protectToUpdate(fields: Array<string>) : this;
    protectToExpose(fields: Array<string>) : this;

    __isPersistent() : boolean;
    __getStructure() : TypedStructure;
    __assignFromSourceObject(data: any) : this;
    __usePaginationForNextRequest() : PaginationInterface;
    __useExclusionForNextRequest() : boolean;
    __useSeekForNextRequest(seekValue?: QueryConstructorProduct): SeekForRelationInterface;
    __useOrderForNextRequest() : Array<OrderByModelInterface>;

}

/**
 *  Model static interface
 *  -----------------------------------------------------------------------
 *  Defines what methods Models should implement statically
 */
export interface ModelStaticInterface {

    new() : ModelInterface;
    prototype: ModelInterface;
    query<T>() : QueryInterface<T>;
    find<T>(id: number|string) : Promise<T>;
    list<T>(page: number, items: number, orderBy: Array<OrderByModelInterface>) : Promise<Array<T>>;
    findList<T>(id: Array<number|string>,
                page?: number,
                items?: number,
                orderBy?: Array<OrderByModelInterface>) : Promise<Array<T>>;
    findListForProperty<T>(property: string,
                           value: number | string | boolean,
                           page?: number,
                           items?: number,
                           orderBy?: Array<OrderByModelInterface>) : Promise<Array<T>>;
    relation<T>(relationName: string,
                page?: number,
                items?: number,
                orderBy?: Array<OrderByModelInterface>) : QueryConstructorExecutive<T>;
    truncate() : Promise<number>;
    count() : Promise<number>;
    __idList(page: number, items: number, orderBy: Array<OrderByModelInterface>) : Promise<Array<number|string>>

}

/**
 *  Model Counter Interface
 *  -----------------------------------------------------------------------
 *  If database doesn't provide atomic counter then it will be done though
 *  specific model with this Interface
 */
export interface ModelCounterInterface {

    prepare() : Promise<boolean>;
    prepareForModel(modelName: string) : Promise<boolean>;
    incrementAndGet(modelName: string) : Promise<number>;
    resetForModel(modelName: string) : Promise<boolean>;

}

/**
 * Interface representing common pagination object
 * -------------------------------------------------------------------------
 */
export interface PaginationInterface {
    pagination: boolean,
    items: number,
    offset: number
}

/**
 *  Joiner Model
 *  -----------------------------------------------------------------------
 *  Model which serves as a layer which joins two different other models
 *  through the database linkage
 */
export interface ModelJoinerInterface {

    __setCollection(c: any) : void;
    __setMasterName(n: string) : void;
    __setSlaveName(n: string) : void;

    getWhereMasterIs(value: number, pagination?: PaginationInterface) : Promise<Array<number>>;
    getWhereSlaveIs(value: number, pagination?: PaginationInterface) : Promise<Array<number>>;
    getWhereMasterIn(valueList: Array<number>, pagination?: PaginationInterface) : Promise<Array<number>>;
    getWhereSlaveIn(valueList: Array<number>, pagination?: PaginationInterface) : Promise<Array<number>>;
    getWhereMasterCrossingList(masterValue: number, valueList: Array<number>, pagination?: PaginationInterface) : Promise<Array<number>>;
    getWhereSlaveCrossingList(slaveValue: number, valueList: Array<number>, pagination?: PaginationInterface) : Promise<Array<number>>;
    addMasterSlavePair(master: number, slave: number) : Promise<UpdateWriteOpResult>;
    removeMasterSlavePair(master: number, slave: number) : Promise<DeleteWriteOpResultObject>

    countSlavePairs(slave: number) : Promise<number>;
    countMasterPairs(master: number) : Promise<number>;

    removeWhereSlaveIs(value: number) : Promise<DeleteWriteOpResultObject>;

    listMaster(pagination: PaginationInterface) : Promise<Array<number>>;
    listSlave(pagination: PaginationInterface) : Promise<Array<number>>;

}

/**
 *  Structure
 *  -----------------------------------------------------------------------
 *  Main definition of structure which reflects configuration of some
 *  table inside some database.
 */
export interface StructureInterface {
    className: string;
    classPrototype: any;
    connection: any;
    foreignDependency: boolean;
    foreignDependencyNames: Array<string>;
    foreignCascadeDependency: boolean;
    foreignCascadeDependencyNames: Array<string>;
    properties: {[key:string] : TypedProperty};
    propertyList: Array<TypedProperty>;
    propertyIndex: Set<string>;
    primaryIndex: PrimaryIndex;
    secondaryIndex: Map<string, SecondaryIndex>;
    getRelationForName(name: string) : CompiledRelation | null;
    setRelationForPropertyName(name: string, rel: CompiledRelation) : any;
    hasProperty(name: string) : boolean;
    getProperty(name: string) : TypedProperty | null;
}

/**
 *  Seek for Relation
 *  -----------------------------------------------------------------------
 *  Interface defining object which sufficient enough to carry information
 *  required for instantiated object to perform seek on one of it's
 *  relation properties
 */
export interface SeekForRelationInterface {
    executor: QueryExecutorInterface;
    query: QueryConstructorProduct;
}

/**
 *  Interface representing "orderBy" Model action values
 *  -----------------------------------------------------------------------
 */
export interface OrderByModelInterface {
    name: string,
    order: DataOutputOrderType | string
}

/**
 *  Enumerated options of which way to order data
 *  -----------------------------------------------------------------------
 */
export enum DataOutputOrderType {
    ASC = "asc",
    DESC = "desc"
}

/**
 *  Mongo Structure
 *  -----------------------------------------------------------------------
 *  Every structure may have their own collection driver
 *
 */
export interface StructureMongoInterface extends StructureInterface {
    connectionName: string;
    connection: Collection;
    counter: ModelCounterInterface;
}

/**
 *  MySQL Structure
 *  -----------------------------------------------------------------------
 *  Every structure may have their own collection driver
 *
 */
export interface StructureMYSQLInterface extends StructureInterface {
    connection: any;
}

/**
 *  Rejection Model (class)
 *  -----------------------------------------------------------------------
 *  Class mocking standard exception interface. Will be returned as a result
 *  of failed Promise actions of the framework operations.
 */
export class ModelRejection {

    constructor(public message: string, public code: number) {}

}

export class WrongExecutionOrderException extends Exception {
    constructor(message: string) {
        super(message, 466);
    }
}

export class DataCorruptedException extends Exception {
    constructor(message: string) {
        super(message, 400);
    }
}

export class DataNotFoundException extends Exception {
    constructor(message: string) {
        super(message, 404);
    }
}

export class DatabaseErrorException extends Exception {
    constructor(message: string) {
        super(message, 500);
    }
}

export class DatabaseConfigurationException extends Exception {
    constructor(message: string) {
        super(message, 400);
    }
}