/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Flow pipelines for Relationships satisfy all
 *              situations related to interaction among them
 *
 ***************************************************************/
import {CompiledRelation} from "./relations";
import {DatabaseErrorException, ModelInterface, ModelStaticInterface, StructureInterface} from "./model";
import {DataValidationException, Exception} from "ts-exceptions";

export class ModelFlows {

    /**
     * Check if current entity satisfies relationships it having defined by structure
     * -----------------------------------------------------------------------------------------------
     * @param {TypedStructure} structure
     * @param instance
     * @param {{}} query
     * @returns {boolean}
     */
    public static satisfyRelationshipsUponCreation(structure: StructureInterface,
                                             instance: ModelInterface & {[key: string] : any},
                                             query: {[key:string]:any}) {

        /* --------------------------------------
         *
         *  Foreign dependency case (Slave)
         *
         * --------------------------------------
         */
        if(structure.foreignDependency) {

            const list = structure.foreignDependencyNames;

            // Traverse all relations which should be affected by foreign check
            for(let i = 0; i < list.length; i++) {

                const r: CompiledRelation = structure.getRelationForName(list[i]);

                // Don't do checkups if we working with relation master
                if(r.master)
                    continue;

                // Check data consistency for Many relation
                if(r.outputMany) {

                    let data: Array<ModelInterface> = instance[r.propertyInclusionName];

                    // Validate that many relation is actual Array
                    if(!r.weak && !Array.isArray(data))

                        throw new DataValidationException(
                            "Relation error: Model <" + structure.className + "> "+
                            "has a foreign dependency for property [" + r.propertyName + "] "+
                            "with weak relation status: [" + String(r.weak) + "] " +
                            "which should be represented by a Array of relation entities. "+
                            "Value provided as dependency value is not an Array");

                    // Validate that relational data presented on entity prior to saving
                    //  (check only if model is not persistent prior to be saved)
                    if(!r.weak && (data.length === 0 || data === null) && !instance.__isPersistent())

                        throw new DataValidationException(
                            "Relation error: Model <" + structure.className + "> " +
                            "has a foreign dependency for property [" + r.propertyName + "] " +
                            "with weak relation status: [" + String(r.weak) + "] " +
                            "which should be represented by a Array of relation entities. " +
                            "Value provided as dependency value is an empty Array");

                    // Pass on data check if it's no data and it's a weak dependency
                    if(r.weak && (data === null || data.length === 0))
                        continue;

                    // Validate all entities to be persistent prior saving entity
                    for(let i = 0; i < data.length; i++) {

                        // Validate that data[i] is an object which is not null and is persistent
                        if(typeof data[i] !== "object")

                            throw new DataValidationException(
                                "Relation error: Model <" + structure.className + "> "+
                                "has a foreign dependency for property [" + r.propertyName + "] "+
                                "while one of the elements assigned as dependency is not having a type of Model. " +
                                "Assigned type is: " + (typeof data[i]));

                        if(data[i] === null)

                            throw new DataValidationException(
                                "Relation error: Model <" + structure.className + "> "+
                                "has a foreign dependency for property [" + r.propertyName + "] "+
                                "while one of the elements assigned as dependency is NULL.");

                        if(!data[i].__isPersistent()) {

                            throw new DataValidationException(
                                "Relation error: Model <" + structure.className + "> " +
                                "has a foreign dependency for property [" + r.propertyName + "] " +
                                "while one of the elements assigned as dependency is not marked as a " +
                                "persistent entity, and probably need to be saved first.");

                        }

                    }

                }
                // Check consistency and upgrade query for singular relationship
                else {

                    let data: ModelInterface = instance[r.propertyInclusionName];

                    // Check if data is null
                    if(data === null) {

                        // If data is not presented and relation is NOT WEAK then throw error
                        if(r.weak === false)

                            throw new DataValidationException(
                                "Relation error: Model <" + structure.className + "> " +
                                "has a foreign dependency for property [" + r.propertyName + "] " +
                                "which should be represented by a <ModelInterface> relation entity. " +
                                "Value provided as dependency value is NULL");

                        // If data is not presented and relation WEAK then continue to next relation
                        else

                            continue;

                    }

                    if(!data.__isPersistent())

                        throw new DataValidationException(
                            "Relation error: Model <" + structure.className + "> "+
                            "has a foreign dependency for property [" + r.propertyName + "] "+
                            "while element assigned as dependency is not marked as a " +
                            "persistent entity, and probably need to be saved first");

                    query[r.propertyName] = instance[r.propertyIdValueName];

                }

            }

        }
        /* --------------------------------------
         *
         *  Cascade dependency case (Master)
         *
         * --------------------------------------
         */
        if(structure.foreignCascadeDependency) {

            const list = structure.foreignCascadeDependencyNames;

            // Traverse all relations which should be affected by foreign check
            for(let i = 0; i < list.length; i++) {

                const r: CompiledRelation = structure.getRelationForName(list[i]);

                // Don't do checkups if we working with relation slave
                if (!r.master)
                    continue;

                // Check data consistency for Many relation
                if (r.outputMany) {

                    let data: Array<ModelInterface> = instance[r.propertyInclusionName];

                    // Don't check anything if data is null - means not defined
                    if(data === null)
                        continue;

                    // Validate that many relation is actual Array
                    if (!Array.isArray(data))

                        throw new DataValidationException(
                            "Relation error: Model <" + structure.className + "> " +
                            "has a foreign dependency for property [" + r.propertyName + "] " +
                            "which should be represented by a Array of relation entities. " +
                            "Value provided as dependency value is not an Array");

                    // Validate all entities to be persistent prior saving entity
                    for(let i = 0; i < data.length; i++) {

                        // Validate that data[i] is an object which is not null and is persistent
                        if(typeof data[i] !== "object")

                            throw new DataValidationException(
                                "Relation error: Model <" + structure.className + "> "+
                                "has a foreign dependency for property [" + r.propertyName + "] "+
                                "while one of the elements assigned as dependency is not having a type of Model. " +
                                "Assigned type is: " + (typeof data[i]));

                        if(data[i] === null)

                            throw new DataValidationException(
                                "Relation error: Model <" + structure.className + "> "+
                                "has a foreign dependency for property [" + r.propertyName + "] "+
                                "while one of the elements assigned as dependency is NULL.");

                        if(!data[i].__isPersistent()) {

                            throw new DataValidationException(
                                "Relation error: Model <" + structure.className + "> " +
                                "has a foreign dependency for property [" + r.propertyName + "] " +
                                "while one of the elements assigned as dependency is not marked as a " +
                                "persistent entity, and probably need to be saved first.");

                        }

                    }

                }

            }

        }

        return true;

    }

    /**
     * Check if current entity satisfies relationships it having defined by structure
     * -----------------------------------------------------------------------------------------------
     * @param {TypedStructure} structure
     * @param instance
     * @param {{}} query
     * @returns {boolean}
     */
    public static satisfyRelationshipsUponUpdate(structure: StructureInterface,
                                                 instance: ModelInterface & {[key: string] : any},
                                                 query: {[key:string]:any}) {

        /* --------------------------------------
         *
         *  Foreign dependency case (Slave)
         *
         * --------------------------------------
         */
        if(structure.foreignDependency) {

            const list = structure.foreignDependencyNames;

            // Traverse all relations which should be affected by foreign check
            for (let i = 0; i < list.length; i++) {

                const r: CompiledRelation = structure.getRelationForName(list[i]);

                // Ignore if master
                if(r.master)

                    continue;

                /* --------------------------------------
                 *
                 *  One To One/Many dependency
                 *
                 * --------------------------------------
                 */
                if (!r.outputMany) {

                    // Get inclusion and exclusion data from instance
                    const incData: ModelInterface | any = instance[r.propertyInclusionName];

                    // If we got something to include for main entity
                    if(incData !== null) {

                        // Change id to update entity for a new reference
                        query[r.propertyName] = incData[r.queryForProperty];

                        // Invalidate pulled reference flag on instance,
                        // for next request system pull proper reference
                        instance[r.propertyStatusName] = false;

                    }

                }
                /* --------------------------------------
                 *
                 *  Many To Something dependency
                 *
                 * --------------------------------------
                 */
                else {

                    let data: Array<ModelInterface> = instance[r.propertyInclusionName];

                    // Validate that many relation is actual Array
                    if(!r.weak && !Array.isArray(data))

                        throw new DataValidationException(
                            "Relation error: Model <" + structure.className + "> "+
                            "has a foreign dependency for property [" + r.propertyName + "] "+
                            "with weak relation status: [" + String(r.weak) + "] " +
                            "which should be represented by a Array of relation entities. "+
                            "Value provided as dependency value is not an Array");

                    // Validate that relational data presented on entity prior to update model
                    //  (check only if model is not persistent by some alteration)
                    if(!r.weak && (data.length === 0 || data === null) && !instance.__isPersistent())

                        throw new DataValidationException(
                            "Relation error: Model <" + structure.className + "> " +
                            "has a foreign dependency for property [" + r.propertyName + "] " +
                            "with weak relation status: [" + String(r.weak) + "] " +
                            "which should be represented by a Array of relation entities. " +
                            "Value provided as dependency value is an empty Array");

                    // Pass on data check if it's no data and it's a weak dependency
                    if(r.weak && (data === null || data.length === 0))
                        continue;

                    // Validate all entities to be persistent prior saving entity
                    for(let i = 0; i < data.length; i++) {

                        // Validate that data[i] is an object which is not null and is persistent
                        if(typeof data[i] !== "object" || data[i] === null || !data[i].__isPersistent()) {

                            throw new DataValidationException(
                                "Relation error: Model <" + structure.className + "> " +
                                "has a foreign dependency for property [" + r.propertyName + "] " +
                                "while one of the elements assigned as dependency is not marked as a " +
                                "persistent entity, and probably need to be saved first.");

                        }

                    }

                }

            }

        }

        /* --------------------------------------
         *
         *  Cascade dependency case (Master)
         *
         * --------------------------------------
         */
        if(structure.foreignCascadeDependency) {

            const list = structure.foreignCascadeDependencyNames;

            // Traverse all relations which should be affected by foreign check
            for (let i = 0; i < list.length; i++) {

                const r: CompiledRelation = structure.getRelationForName(list[i]);

                // Don't do checkups if we working with relation slave
                if (!r.master)
                    continue;

                // Check data consistency for Many relation
                if(r.outputMany) {

                    let data: Array<ModelInterface> = instance[r.propertyInclusionName];

                    // Don't check anything if data is null - means not defined
                    if (data === null)
                        continue;

                    // Validate that many relation is actual Array
                    if (!Array.isArray(data))

                        throw new DataValidationException(
                            "Relation error: Model <" + structure.className + "> " +
                            "has a foreign dependency for property [" + r.propertyName + "] " +
                            "which should be represented by a Array of relation entities. " +
                            "Value provided as dependency value is not an Array");

                    // Validate all entities to be persistent prior saving entity
                    for(let i = 0; i < data.length; i++) {

                        // Validate that data[i] is an object which is not null and is persistent
                        if(typeof data[i] !== "object" || data[i] === null || !data[i].__isPersistent())

                            throw new DataValidationException(
                                "Relation error: Model <" + structure.className + "> "+
                                "has a foreign dependency for property [" + r.propertyName + "] "+
                                "while one of the elements assigned as dependency is not marked as a " +
                                "persistent entity, and probably need to be saved first.");

                    }

                }

            }

        }

    }

    /**
     * Complete and propagate Many-to-Many relationships if any found to propagate
     * -----------------------------------------------------------------------------------------------
     * @param {TypedStructure} structure
     * @param instance
     * @returns {Promise<boolean>}
     */
    public static async completeRelationalPropagation(structure: StructureInterface,
                                                      instance: ModelInterface | any) {

        /* --------------------------------------
         *
         *  Foreign dependency case (Slave)
         *
         * --------------------------------------
         */
        if(structure.foreignDependency) {

            try {

                const list = structure.foreignDependencyNames;

                // Traverse all relations which should be affected by foreign check
                for (let i = 0; i < list.length; i++) {

                    let k = 0;

                    const r: CompiledRelation = structure.getRelationForName(list[i]);

                    // Ignore if master
                    if(r.master)
                        continue;

                    // Take many relations which we are not masters but dependant
                    if (r.outputMany) {

                        let incPairs: Array<{ m: any, s: any }> = [];
                        let excPairs: Array<{ m: any, s: any }> = [];

                        // Get inclusion and exclusion data from instance
                        const incData: Array<ModelInterface & any> = instance[r.propertyInclusionName];
                        const excData: Array<ModelInterface & any> = instance[r.propertyExclusionName];

                        // If inclusion data exists and is Array
                        if(incData !== null && Array.isArray(incData)) {

                            incPairs = new Array(incData.length);

                            // Create Id pairs which should be joined
                            for (k = 0; k < incData.length; k++)

                                incPairs[k] = {
                                    m: incData[k][r.queryForProperty],
                                    s: instance[r.selfPrimaryIndexName]
                                };

                        }

                        // If exclusion data exists and is Array
                        if(excData !== null && Array.isArray(excData)) {

                            excPairs = new Array(excData.length);

                            // Create Id pairs which should be excluded
                            for (k = 0; k < excData.length; k++)

                                excPairs[k] = {
                                    m: excData[k][r.queryForProperty],
                                    s: instance[r.selfPrimaryIndexName]
                                };

                        }

                        // Add pairs to joiner table if it need to be added
                        for(k = 0; k < incPairs.length; k++)
                            await r.joinModel.addMasterSlavePair(incPairs[k].m, incPairs[k].s);

                        // Add pairs to joiner table if it need to be added
                        for(k = 0; k < excPairs.length; k++)
                            await r.joinModel.removeMasterSlavePair(excPairs[k].m, excPairs[k].s);

                    }

                }

            } catch (e) {

                throw new DatabaseErrorException(e.message);

            }

        }
        /* --------------------------------------
         *
         *  Cascade dependency case (Master)
         *
         * --------------------------------------
         */
        if(structure.foreignCascadeDependency) {

            try {

                const list = structure.foreignCascadeDependencyNames;

                // Traverse all relations which should be affected by foreign check
                for (let i = 0; i < list.length; i++) {

                    let k = 0;

                    const r: CompiledRelation = structure.getRelationForName(list[i]);

                    // Continue if master
                    if(!r.master)
                        continue;

                    /*
                     *  (MTM Cases) Cascade dependency with relation to JOINER MODEL
                     *  ------------------------------------------------------------------
                     *
                     */
                    if (r.outputMany && r.joinModel) {

                        let incPairs: Array<{ m: any, s: any }> = [];
                        let excPairs: Array<{ m: any, s: any }> = [];

                        // Get inclusion and exclusion data from instance
                        const incData: Array<ModelInterface & any> = instance[r.propertyInclusionName];
                        const excData: Array<ModelInterface & any> = instance[r.propertyExclusionName];

                        // If inclusion data exists and is Array
                        if(incData !== null && Array.isArray(incData)) {

                            incPairs = new Array(incData.length);

                            // Create Id pairs which should be joined
                            for (k = 0; k < incData.length; k++)

                                incPairs[k] = {
                                    s: incData[k][r.queryForProperty],
                                    m: instance[r.selfPrimaryIndexName]
                                };

                        }

                        // If exclusion data exists and is Array
                        if(excData !== null && Array.isArray(excData)) {

                            excPairs = new Array(excData.length);

                            // Create Id pairs which should be excluded
                            for (k = 0; k < excData.length; k++)

                                excPairs[k] = {
                                    s: excData[k][r.queryForProperty],
                                    m: instance[r.selfPrimaryIndexName]
                                };

                        }

                        // Add pairs to joiner table if it need to be added
                        for (k = 0; k < incPairs.length; k++)
                            await r.joinModel.addMasterSlavePair(incPairs[k].m, incPairs[k].s);

                        // Add pairs to joiner table if it need to be added
                        for (k = 0; k < excPairs.length; k++)
                            await r.joinModel.removeMasterSlavePair(excPairs[k].m, excPairs[k].s);

                    }
                    /*
                     *  (OTM Cases) Cascade dependency case with no Joiner Model available
                     *  ------------------------------------------------------------------
                     *  Applicable for any One-to-Many cases saving.
                     *
                     *  1) Inclusion part (append):
                     *
                     *      This case can happen during a situation when saving of OTM relationship
                     *      linkage is happening from the master side, example:
                     *
                     *          dependency = new Dependency();
                     *          await dependency.save();
                     *
                     *          master = new Master();
                     *          master.dependencyProperty = [dependency];
                     *          await master.save();
                     *
                     *      So relationship is applied from the master side and dependency will be
                     *      auto-populated with values for that master.
                     *
                     *  2) Exclusion part (remove):
                     *
                     *      This case can happen during a situation of removing some entities of
                     *      OTM relationship, example:
                     *
                     *          dependencies = await master.paginate(1,10).dependencyProperty;
                     *
                     *          master.exclude().dependencyProperty = dependencies;
                     *
                     *          await master.save();
                     *
                     *      Please note: that in that case WEAK OTM dependencies will stay in DB with
                     *      linked parameter as null, while STRONG OTM dependencies will be erased
                     *      from the system completely
                     */
                    else if(r.outputMany) {

                        // Assume by default that OTM entity is weak
                        let foreignRelIsWeak = true;

                        // If JoinSlave added (may not be for non mutual relationship data)
                        if(r.joinSlave !== null)

                            // Then use this parameter from JoinSlave
                            foreignRelIsWeak = r.joinSlave.weak;

                        // Get inclusion and exclusion data from instance
                        const incData: Array<ModelInterface|any> = instance[r.propertyInclusionName];
                        const excData: Array<ModelInterface|any> = instance[r.propertyExclusionName];

                        // If inclusion data exists and is Array
                        if(incData !== null && Array.isArray(incData)) {

                            for(let inc = 0; inc < incData.length; inc++) {

                                const incEntity = incData[inc];

                                // Append current instance as a dependency
                                incEntity[r.queryForProperty] = instance;

                                // Process with standard model save
                                await incEntity.save();

                            }

                        }

                        // If exclusion data exists and is Array
                        if(excData !== null && Array.isArray(excData)) {

                            for(let exc = 0; exc < excData.length; exc++) {

                                const excEntity = excData[exc];

                                // If relation is weak then NULL the relational parameter and save
                                if(foreignRelIsWeak) {

                                    excEntity[r.queryForProperty] = null;

                                    // Process with standard model save
                                    await excEntity.save();

                                }
                                // If relation is strong - remove relational entity
                                else

                                    // Process with standard model delete
                                    await excEntity.delete();

                            }

                        }

                    }

                }

            } catch (e) {

                throw new DatabaseErrorException(e.message);

            }

        }

        return true;

    }

    /**
     * Complete deletion of Many references
     * -----------------------------------------------------------------------------------------------
     * @param {TypedStructure} structure
     * @param instance
     * @returns {Promise<void>}
     */
    public static async completeRelationDeletion(structure: StructureInterface,
                                                 instance: ModelInterface & any) {

        /*
         *  Check relations for slave dependency
         *  --------------------------------------
         *  Delete links related to slave dependency
         *  if any can be found in joiner table
         */
        if(structure.foreignDependency) {

            try {

                const list = structure.foreignDependencyNames;

                // Traverse all relations which should be affected by foreign check
                for (let i = 0; i < list.length; i++) {

                    const r: CompiledRelation = structure.getRelationForName(list[i]);

                    if(r.outputMany)

                        await r.joinModel.removeWhereSlaveIs(instance[r.selfPrimaryIndexName]);

                }

            } catch (e) {

                throw new DatabaseErrorException(e.message);

            }

        }
        /*
         *  Check relations for master dependency
         *  if anything was defined as cascade
         *  --------------------------------------
         *  Delete references linked to current master
         *  if reference has only master as the linked
         *  reference it will be deleted, otherwise only
         *  it's reference to current master will be
         *  removed
         */
        else if(structure.foreignCascadeDependency) {

            try {

                // Get list of cascaded dependencies
                const list = structure.foreignCascadeDependencyNames;

                // Traverse all relations which should be affected by foreign check
                for (let i = 0; i < list.length; i++) {

                    const r: CompiledRelation = structure.getRelationForName(list[i]);

                    // Take pulling model from relation
                    // (Pulling model is a model representing foreign entity static class)
                    const pullingModel: ModelStaticInterface = r.throughEntity;

                    // Take primary index value of Master
                    const masterId: number = instance[r.selfPrimaryIndexName];

                    // Check if relation happens to be Has Many through BelongsToMany back linkage
                    if (r.outputMany && r.joinModel !== null) {

                        // Check if this MTM dependency is not Weak dependency
                        // (which should not be affected by cascaded operations)
                        if(r.joinSlave.weak === true)
                            continue;

                        const n: number = await r.joinModel.countMasterPairs(masterId);

                        let offset: number = 0;
                        let items: number = 1024;

                        // Stage 1: get all of the links for slave relationship
                        for (let k = n; k > 0; k -= items) {

                            const pairedSlaves = await r.joinModel.getWhereMasterIs(masterId, {
                                pagination: true,
                                offset: offset,
                                items: items
                            });

                            // Stage 2: look at slave pairs we have for relationship
                            for (let p = 0; p < pairedSlaves.length; p++) {

                                const slaveId = pairedSlaves[p];

                                const n: number = await r.joinModel.countSlavePairs(slaveId);

                                // Stage 3: If we found that entity linked to cascade
                                // is actually alone entity then delete it
                                if (n === 1) {

                                    // Find those lone entities and delete them
                                    try {

                                        //-> TODO Substitute this heavy method with some more light on the structure level
                                        const entity = await pullingModel.find<ModelInterface>(slaveId);

                                        await entity.delete();

                                    }
                                    // Catch errors which happened during entity deletion operation
                                    catch (e) {

                                        // Pass on Not found entities, deletion process of main entity should
                                        // not be stopped if some relation was already been deleted
                                        if (e.code !== 404)

                                            throw new Exception("Error happened during resolving relation schema " +
                                                "for item deletion. Original error: " + e.message, e.code);

                                    }

                                }

                                // Remove linkage
                                await r.joinModel.removeMasterSlavePair(masterId, slaveId);

                            }

                        }

                    }
                    // If entities linked not through joiner model but by simple reference - pull them and delete them
                    else {

                        // Weak Relations should stay in place on cascade delete operations
                        if(r.joinSlave.weak)
                            continue;

                        //-> TODO Write better process of deletion every entity which is not using Joiner table

                        // Take all entities which belongs to master index (through constructor)
                        const foreignEntities = await r.throughEntity
                            .query<ModelInterface>()
                            .construct()
                            .where(r.queryForProperty)
                            .equals(masterId)
                            .getMany();

                        // Delete entity per entity
                        for(let k = 0; k < foreignEntities.length; k++) {

                            // Find those lone entities and delete them
                            try {

                                // Launch deletion process
                                await foreignEntities[k].delete();

                            } catch (e) {

                                // Avoid process to be stopped on any errors at deletion of
                                // entities which a relations

                            }

                        }

                    }

                } // End Main for loop

            }
            // Main sub-process catch
            catch (e) {

                // If initial error was Exception type then rethrow
                if(e !== null) {

                    if(e.hasOwnProperty("code"))
                        throw e;
                }

                // If unrecognized type then convert it to DB Exception
                throw new DatabaseErrorException(e.message);

            }

        }

        return true;

    }

    /**
     *
     * -----------------------------------------------------------------------------------------------
     * @param {CompiledRelation} relation
     * @param {number} masterId
     * @returns {Promise<number>}
     */
    public static async getCountForManyRelationWithMasterId(relation: CompiledRelation,
                                                            masterId: number) : Promise<number> {

        if(relation.outputMany && relation.joinModel !== null) {

            try {

                return await relation.joinModel.countMasterPairs(masterId);

            } catch (e) {

                throw new Exception(e, 500);

            }

        }

        throw new Exception("Can't get count for Many relation with no Join model defined", 500);

    }

}