/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview ORM Mongo model extendable class
 *
 ***************************************************************/
import "reflect-metadata";
import {
    BelongsRelationPropertyType,
    DatabaseConfigurationException,
    DatabaseErrorException,
    DataCorruptedException,
    Model as BaseModel,
    ModelInterface,
    ModelRejection,
    ModelSource,
    ModelStaticInterface, ModelType,
    OrderByModelInterface,
    PaginationInterface,
    PropertyType,
    Relations,
    SeekForRelationInterface,
    StructureMongoInterface,
    WrongExecutionOrderException
} from "./common/model";
import {ConfigurationException} from "../exceptions/exceptions";
import {Collection} from "mongodb";
import {MongoQueryExecutor} from "./mongo/query/mongo.executor";
import {MongoQueryInterface, Query} from "./common/query/query";
import {MongoInitializer} from "./mongo/mongo.initializer";
import {TypedPropertyFactory} from "./common/typed.factory";
import {PrimaryIndex, TypedProperty, TypedStructure} from "./common/typed.structure";
import {ModelFlows} from "./common/model.flows";
import {DataValidationException, Exception} from "ts-exceptions";
import {App, Type} from "../app";
import {CompiledRelation} from "./common/relations";
import {
    QueryConstructor, QueryConstructorExecutive, QueryConstructorNonExecutive,
    QueryConstructorProduct, RelationalQueryConstructor
} from "./common/query/query.constructor";
import {QueryExecutor} from "./common/query/query.executor";

export type StaticModelOf<T> = { new() : T };

/**
 * Mongo Model
 * ---------------------------------------------------------------------------------
 *
 * Classes which dedicated to be represented by Mongo Database should extend
 * that model to inherit all properties and methods available for modeling
 * through Mongo persistent storage driver.
 */
export class MongoModel extends BaseModel implements ModelInterface {

    /**
     * Static storage of entity system schema
     *
     * @type {Map<string, TypedStructure>}
     * @private
     */
    private static __schemaStorage: Map<string, TypedStructure> = new Map();
    /**
     * Pagination parameter for next pull request consider as a limiter. Mostly
     * suitable for management output of relation data
     *
     * @type {number}
     * @private
     */
    protected __paginateNextItems: number = null;
    /**
     * Pagination parameter for next pull request consider as an offset. Mostly
     * suitable for management output of relation data
     *
     * @type {number}
     * @private
     * @system
     */
    protected __paginateNextPage: number = null;
    /**
     * If next operation should have exclusion flow then this flag should be set
     * as true. Mostly applied for Many-To-Many relationship dependency management
     *
     * @type {boolean}
     * @private
     * @system
     */
    protected __exclusionMode: boolean = false;
    /**
     * If next operation should order relation output by some of the model properties
     * then this property should have be pre-filled with orderBy properties
     *
     * @type {OrderByModelInterface[]}
     * @private
     */
    protected __orderNextRequest: Array<OrderByModelInterface> = null;
    /**
     * If next operation should be seek mode operation for relational search
     * this property will be filled with Constructor Product and should be
     * used with relational search executors.
     *
     * @type {QueryConstructorProduct}
     * @private
     * @system
     */
    protected __seekMode: QueryConstructorProduct = null;
    /**
     * Reference to assembled structure
     *
     * @type {StructureMongoInterface}
     * @protected
     */
    protected __structureRef: StructureMongoInterface = null;
    /**
     * Flag with carrying information about current entity persistence in DB
     *
     * @type {boolean}
     * @protected
     */
    protected __persists: boolean = false;
    /**
     *
     * @type {{}}
     * @private
     */
    protected __restrictToUpdateFields: {[key:string] : boolean} = {};
    /**
     *
     * @type {number}
     * @private
     */
    protected __restrictToUpdateAmount: number = 0;
    /**
     *
     * @type {{}}
     * @private
     */
    protected __restrictToExposeFields: {[key:string] : boolean} = {};
    /**
     *
     * @type {number}
     * @private
     */
    protected __restrictToExposeAmount: number = 0;

    /**
     *  Initialize Mongo Model
     *  -----------------------------------------------------------------------------
     *  Will construct and initialize new Model with
     *  existing structure defining that model properties
     *  with linkage to database connector
     */
    constructor(data: {} = null) {

        super();

        if(this.__structureRef === null)
            this.__structureRef = this.__getStructure();

        if(data !== null)
            this.fromObject(data);

    }

    /**
     * Outputs boolean value with meaning of confirmation of data persistence in DB
     * -----------------------------------------------------------------------------
     * Service method
     *
     * @returns {boolean}
     *
     */
    public __isPersistent() : boolean {
        return this.__persists;
    }

    /**
     * Outputs relation for some property name
     * -----------------------------------------------------------------------------
     * Service method
     * May output null reference as non existent relation property
     *
     * @service
     * @param {string} name
     * @returns {CompiledRelation | null}
     *
     */
    public _getRelationForProperty(name: string) : CompiledRelation | null {

        return this.__structureRef.getRelationForName(name);

    }

    /**
     * Outputs value for some simple assigned relationship (OTM, OTO)
     * -----------------------------------------------------------------------------
     * Service method
     * May output null reference as non existent relation property or if
     * relationship is actual MTM
     *
     * @service
     * @param {string} name
     * @returns {string | number | null}
     */
    public _getRelationValueForProperty(name: string) : string | number | null {

        const rel = this.__structureRef.getRelationForName(name);

        if(rel !== null) {

            const that: any = this;

            if(rel.joinModel !== null)
                return null;

            return that[rel.propertyIdValueName];

        }

        return null;

    }

    /**
     * Pull entity for primary id index
     * -----------------------------------------------------------------------------
     * @param {number} id
     * @returns {Promise<T>}
     * @throws {DatabaseConfigurationException}
     */
    public static find<T extends MongoModel>(this: StaticModelOf<T>, id: number | string) : Promise<T> {

        const prototypeOfInstance: any = this;

        const schema: StructureMongoInterface = MongoModel.__getStructureStatic(prototypeOfInstance);

        // Check that primary index is defined for current Entity at it's schema
        if(schema.primaryIndex !== null) {

            const query: any = {};

            // Create query for primary index
            query[schema.primaryIndex.name] = id;

            // Do find in async promise
            return new Promise<T>((resolve, reject:(v: ModelRejection) => void) => {

                schema.connection.findOne(query).then(

                    // On successful query resolution
                    data => {

                        // On empty results do Rejection with Exception Interface
                        if(data === null)
                            reject(new ModelRejection("Not found", 404));

                        // Apply recieved data to entity
                        const instance = new prototypeOfInstance().__assignFromSourceObject(data);
                        instance.__persists = true;

                        // On successful result
                        resolve(instance);

                    },
                    // On server error
                    error => reject(new ModelRejection(error, 500)));

            });

        }

        throw new DatabaseConfigurationException(
            "Your instance: <" + schema.className + "> of MongoModel is lack of primary index, "+
            "that prevents using of find method to retrieve entities. "+
            "Please define @Property({primary:true, type:Type.[Int,String]}) for be able to do find operations");

    }

    /**
     * Count amount of records presented for desired entity
     * -----------------------------------------------------------------------------
     * @returns {Promise<number>}
     * @throws {DatabaseConfigurationException}
     */
    public static count() : Promise<number> {

        const prototypeOfInstance: any = this;

        const schema: StructureMongoInterface = MongoModel.__getStructureStatic(prototypeOfInstance);

        // Do find in async promise
        return new Promise<number>((resolve, reject:(v: ModelRejection) => void) => {

            schema.connection.count({}).then(

                // On successful query resolution
                data => {

                    // On empty results do Rejection with Exception Interface
                    if(data === null)

                        reject(new ModelRejection("Not found", 404));

                    // On successful result
                    resolve(Number(data));

                },
                // On server error
                error => reject(new ModelRejection(error, 500)));

        });

    }

    /**
     * Will truncate model data inside the table
     * -----------------------------------------------------------------------------
     * @returns {Promise<number>}
     * @throws DatabaseErrorException
     */
    public static async truncate() : Promise<number> {

        const prototypeOfInstance: any = this;

        const schema: StructureMongoInterface = MongoModel.__getStructureStatic(prototypeOfInstance);

        let total = 0;
        let offset = 0;
        const items = 100;
        const target = 9999999999;

        for(let i = 0; i < target; i++) {

            try {

                const amount = await MongoModel.__truncateOffset(schema.connection, prototypeOfInstance, offset, items);

                if(amount === 0)
                    break;

                total += amount;

            } catch (e) {

                App.error("Error happened during truncation of Mongo Model. Error: " + e.message,
                    "orm:model:truncate");

                App.error(e.getStackTrace());

                throw e;

            }

        }

        await schema.counter.resetForModel(schema.className);

        return total;

    }

    /**
     * Pull list of entities paginated
     * -----------------------------------------------------------------------------
     * @param {number} page
     * @param {number} items
     * @param {Array<OrderByModelInterface>} orderBy
     * @returns {Promise<Array<T>>}
     */
    public static list<T>(page: number = 1, items: number = 100, orderBy: Array<OrderByModelInterface> = null) : Promise<Array<T>> {

        const prototypeOfInstance: any = this;

        const schema: StructureMongoInterface = MongoModel.__getStructureStatic(prototypeOfInstance);

        const query: any = {};

        // Do find in async promise
        return new Promise<T[]>((resolve, reject:(v: ModelRejection) => void) => {

            let sorts: {[key: string] : number|string} = {};

            if(orderBy !== null) {

                for(let i = 0; i < orderBy.length; i++) {

                    sorts[orderBy[i].name] = orderBy[i].order === "desc" ? -1 : 1;

                }

            }

            schema.connection
                .find(query)
                .skip((page - 1) * items)
                .sort(sorts)
                .limit(items)
                .toArray().then(

                // On successful query resolution
                data => {

                    // On empty results
                    if(data === null)

                        reject(new ModelRejection("Not found", 404));

                    const result: Array<T> = new Array(data.length);

                    for(let i = 0; i < data.length; i++) {

                        let instance = new prototypeOfInstance().__assignFromSourceObject(data[i]);
                        instance.__persists = true;
                        result[i] = instance;

                    }

                    // On successful result
                    resolve(result);

                },
                // On server error
                error => reject(new ModelRejection(error, 500)));

        });

    }

    /**
     * Pull list of entities for set of primary id indexes
     * -----------------------------------------------------------------------------
     * @param {Array<number | string>} list
     * @param {number} page
     * @param {number} items
     * @param {OrderByModelInterface[]} orderBy
     * @returns {Promise<Array<T>>}
     */
    public static findList<T>(list: Array<number | string>,
                              page: number = 1,
                              items: number = 100,
                              orderBy: Array<OrderByModelInterface> = null) : Promise<Array<T>> {

        const prototypeOfInstance: any = this;

        const schema: StructureMongoInterface = MongoModel.__getStructureStatic(prototypeOfInstance);

        // Check that primary index is defined for current Entity at it's schema
        if(schema.primaryIndex !== null) {

            const query: any = {};

            // Create query for primary index
            query[schema.primaryIndex.name] = {$in: list};

            // Do find in async promise
            return new Promise<T[]>((resolve, reject:(v: ModelRejection) => void) => {

                let sorts: {[key: string] : number|string} = {};

                if(orderBy !== null) {

                    for(let i = 0; i < orderBy.length; i++) {

                        sorts[orderBy[i].name] = orderBy[i].order === "desc" ? -1 : 1;

                    }

                }

                schema.connection
                    .find(query)
                    .skip((page - 1) * items)
                    .sort(sorts)
                    .limit(items)
                    .toArray().then(

                    // On successful query resolution
                    data => {

                        // On empty results
                        if(data === null)

                            reject(new ModelRejection("Not found", 404));

                        const result: Array<T> = new Array(data.length);

                        for(let i = 0; i < data.length; i++) {

                            let instance = new prototypeOfInstance().__assignFromSourceObject(data[i]);
                            instance.__persists = true;
                            result[i] = instance;

                        }

                        // On successful result
                        resolve(result);

                    },
                    // On server error
                    error => reject(new ModelRejection(error, 500)));

            });

        }

        throw new DatabaseConfigurationException(
            "Your instance: <" + schema.className + "> of MongoModel is lack of primary index, "+
            "that prevents using of find method to retrieve entities. "+
            "Please define @Property({primary:true, type:Type.[Int,String]}) for be able to do find operations");

    }

    /**
     * Find a list of entities where some property equals to some value
     * -----------------------------------------------------------------------------
     * @param {string} property
     * @param {number | string | boolean} value
     * @param {number} page
     * @param {number} items
     * @param {OrderByModelInterface[]} orderBy
     * @returns {Promise<Array<T>>}
     */
    public static findListForProperty<T>(property: string,
                                         value: number | string | boolean,
                                         page: number = 1,
                                         items: number = 100,
                                         orderBy: Array<OrderByModelInterface> = null) : Promise<Array<T>> {

        const prototypeOfInstance: any = this;

        const schema: StructureMongoInterface = MongoModel.__getStructureStatic(prototypeOfInstance);

        const query: any = {};

        // Create query for primary index
        query[property] = value;

        // Do find in async promise
        return new Promise<T[]>((resolve, reject:(v: ModelRejection) => void) => {

            let sorts: {[key: string] : number|string} = {};

            if(orderBy !== null) {

                for(let i = 0; i < orderBy.length; i++) {

                    sorts[orderBy[i].name] = orderBy[i].order === "desc" ? -1 : 1;

                }

            }

            schema.connection
                .find(query)
                .skip((page - 1) * items)
                .sort(sorts)
                .limit(items)
                .toArray().then(

                // On successful query resolution
                data => {

                    // On empty results
                    if(data === null)

                        reject(new ModelRejection("Not found", 404));

                    const result: Array<T> = new Array(data.length);

                    for(let i = 0; i < data.length; i++) {

                        let instance = new prototypeOfInstance().__assignFromSourceObject(data[i]);
                        instance.__persists = true;
                        result[i] = instance;

                    }

                    // On successful result
                    resolve(result);

                },
                // On server error
                error => reject(new ModelRejection(error, 500)));

        });

    }

    /**
     * Provide method to construct or retrieve right away some relation for static
     * model
     * -----------------------------------------------------------------------------
     * @param {string} relationParamName
     * @param {number} page
     * @param {number} items
     * @param {Array<OrderByModelInterface>} orderBy
     * @returns {QueryConstructorExecutive<T>}
     */
    public static relation<T>(relationParamName: string,
                              page = 1,
                              items = 100,
                              orderBy: Array<OrderByModelInterface> = null) : QueryConstructorExecutive<T> {

        const prototypeOfInstance: any = this;

        /*
         *  Find and extract main structure
         */
        const structure: TypedStructure = MongoModel.__getStructureStatic(prototypeOfInstance);

        /*
         *  Check if required relation is presented within this structure
         */
        const rel = structure.getRelationForName(relationParamName);

        if(rel !== null) {

            /*
             *  Extract counter structure
             */
            const foreignStructure = MongoModel.__getStructureStatic(rel.throughEntity as any);

            /*
             *  Define executor which will be assigned to fetch results
             */
            const specificExecutor = new MongoQueryExecutor(foreignStructure);

            /*
             *  Define abstract executor which constructor can accept
             */
            const preparedExecutor = new QueryExecutor({}, specificExecutor, rel.throughEntity);

            /*
             *  Initiate and return Relational Query constructor with parameters above
             */
            return new RelationalQueryConstructor<T>(
                relationParamName,
                structure,
                foreignStructure,
                preparedExecutor,
                {
                    pagination: true,
                    offset: (page - 1) * items,
                    items
                }, orderBy);

        }

        throw new Exception("relation wasn't able" +
            " to fetch relation with name: " + relationParamName, 500);

    }

    /**
     * Create query for pulling current entity with specific parameters
     * -----------------------------------------------------------------------------
     * @returns {MongoQueryInterface}
     */
    public static query<T>() : MongoQueryInterface<T> {

        const executor = new MongoQueryExecutor(MongoModel.__getStructureStatic(this as any));

        return new Query<T>(executor, this as any);

    }

    /**
     * Paginate next data pull operation.
     * -----------------------------------------------------------------------------
     * Primarily only relation pull operations will be able to use that method
     * prepends to actual relation pull action
     *
     * @param {number} page
     * @param {number} itemsOnPage
     */
    public paginate(itemsOnPage: number, page?: number) : this {

        this.__paginateNextItems = Number(itemsOnPage);

        if(page)
            this.__paginateNextPage = Number(page);

        return this;

    }

    /**
     * Turn on exclusion mode for next update operation
     * -----------------------------------------------------------------------------
     * Primarily this method should prepend only relations going to be passed to
     * setter
     *
     * @returns this
     */
    public exclude() : this {

        this.__exclusionMode = true;

        return this;

    }

    /**
     * Provide constructor for dependency search
     * -----------------------------------------------------------------------------
     * @returns {QueryConstructorNonExecutive<this>}
     */
    public seek() : QueryConstructorNonExecutive<this> {

        return new QueryConstructor<this>(
            null, null, this
        ) as QueryConstructorNonExecutive<this>;

    }

    /**
     * Provide constructor for dependency search
     * -----------------------------------------------------------------------------
     * @returns {QueryConstructorNonExecutive<this>}
     */
    public orderBy(properties: Array<OrderByModelInterface>) : this {

        this.__orderNextRequest = properties;

        return this;

    }

    /**
     * Count of availability of some relation for provided name
     * -----------------------------------------------------------------------------
     * Will return a number for available amount of items
     * of one of the existing relations.
     *
     * @param {string} relationName
     * @returns {number}
     */
    protected countRelation(relationName: string) : Promise<number> {

        const rel = this.__structureRef.getRelationForName(relationName);
        const that: any = this;

        if(rel === null)

            throw new Exception("Count relation method error: there is no relation for name: " + relationName +
                " was defined on a structure: " + this.__structureRef.className, 500);

        // TODO Add single dependencies
        if(rel.outputMany && rel.joinModel !== null) {

            return new Promise((resolve, reject) => {

                ModelFlows
                    .getCountForManyRelationWithMasterId(rel, that[this.__structureRef.primaryIndex.name])
                    .then(
                    res => {
                        resolve(res);
                    },
                    rej => reject(rej)
                );

            });

        }

    }

    /**
     * Will save current entity to permanent storage
     * -----------------------------------------------------------------------------
     * @returns {Promise<T>}
     * @throws {DatabaseConfigurationException}
     */
    public save<T>() : Promise<T> {

        /*
         *  If current entity persists then we should use update instead of save
         */
        if(this.__persists)
            return this.update();

        const query: {[key:string] : any} = {};
        const that:{[key:string]:any} = this;
        const primary = this.__structureRef.primaryIndex;

        /*
         * Entity can be saved only if primary index is defined for that one,
         * that required for entity may be fetched via find after saving
         */
        if(primary !== null) {

            /*
             *  Create new Database query from Structure properties
             */
            const properties = this.__structureRef.propertyList;

            for(let i = 0; i < properties.length; i++) {

                const property = properties[i];

                // Check if property is required to have a value prior to be saved
                // interrupt process if data is not valid
                if(property.notNull && that[property.name] === null)

                    throw new DataValidationException("Property ["+property.name+"] "+
                        "of Model: <" +this.__structureRef.className+ "> can't be null or undefined");

                if(!property.isRelation())

                    query[property.name] = that[property.name];

            }

            /*
             *  Return Async Promise for saving process
             */
            return new Promise<T>((resolve, reject:(v: ModelRejection) => void) => {

                /*
                 *  If we saving with auto-increment of primary index
                 */
                if(primary.auto)

                    this.__saveAndIncrement<T>(that as T, primary, query, resolve, reject);

                /*
                 *  If we saving with manually assigned primary index value
                 */
                else

                    this.__saveNoIncrement<T>(that as T, primary, query, resolve, reject);

            });

        }

        throw new DatabaseConfigurationException(
            "Primary index not assigned for entity you trying to save: <" + this.__structureRef.className +
            "> You should define primary index for one of property for that entity like as following: "+
            "@Property({primary:true, [auto:true..., ");

    }

    /**
     * Will process update of current entity at permanent storage
     * -----------------------------------------------------------------------------
     * @returns {Promise<T>}
     * @throws {DatabaseConfigurationException}
     */
    public update<T>() : Promise<T> {

        const query: {[key:string] : any} = {};
        const that:{[key:string]:any} = this;
        const primary = this.__structureRef.primaryIndex;

        /*
         * Entity can be saved only if primary index is defined for that one,
         * that required for entity may be fetched via find after saving
         */
        if(primary !== null) {

            /*
             *  Create new Database query from Structure properties
             */
            const properties = this.__structureRef.propertyList;

            for(let i = 0; i < properties.length; i++) {

                const property = properties[i];

                // Check if property is required to have a value prior to be saved
                // interrupt process if data is not valid
                if(property.notNull && that[property.name] === null)

                    throw new DataValidationException("Property ["+property.name+"] "+
                        "of Model: <" +this.__structureRef.className+ "> can't be null or undefined");

                // If property is relational then it shouldn't go to common update flow
                if(!property.isRelation())

                    query[property.name] = that[property.name];

            }

            const search:any = {};

            search[primary.name] = that[primary.name];

            /*
             *  Make update
             */
            return new Promise<T>((resolve, reject) => {

                try {
                    
	                // Resolve singular relationships if any presented
	                ModelFlows.satisfyRelationshipsUponUpdate(this.__structureRef, this, query);
	                
                } catch (e) {
	
	                new ModelRejection(e.message, 500);
	
                }

                this.__structureRef.connection.updateOne(search, {$set: query}).then(

                    // Updated object returned
                    updated => {

                        // Continue with promise resolution by checking if there any unresolved MTM rel
                        ModelFlows.completeRelationalPropagation(this.__structureRef, this).then(
                            success => resolve(that as T),
                            error => reject(new ModelRejection(error, 500))
                        );

                    },
                    // Operation was failed
                    error => reject(new ModelRejection(error, 500)));

            });

        }

        throw new DatabaseConfigurationException(
            "Primary index not assigned for entity you trying to update: <" + this.__structureRef.className +
            "> You should define primary index for one of property for that entity like as following: "+
            "@Property({primary:true, ... ");

    }

    /**
     * Will process deletion of current entity from permanent storage
     * -----------------------------------------------------------------------------
     * @returns {Promise<T>}
     * @throws {DatabaseConfigurationException}
     */
    public delete<T>() : Promise<T> {

        const that: {[key:string]:any} = this;
        const primary = this.__structureRef.primaryIndex;

        /*
         * Entity can be saved only if primary index is defined for that one,
         * that required for entity may be fetched via find after saving
         */
        if(primary !== null) {

            const search: any = {};

            search[primary.name] = that[primary.name];

            return new Promise<T>((resolve, reject) => {

                ModelFlows.completeRelationDeletion(this.__structureRef, that).then(() => {

                        this.__structureRef.connection.deleteOne(search).then(

                        // Deleted object returned
                        deleted => {

                            // Remove presence of Id and persistence flag
                            if(deleted.deletedCount > 0) {
                                this.__persists = false;
                                that[primary.name] = null;
                            }

                            resolve(that as T);

                        },
                        // Operation was failed
                        error => reject(new ModelRejection(error, 500)));

                },
                // Reject with passing error from Model Flows, bc Model Flows operate Exceptions
                (err) => reject(err));

            });

        }

        throw new DatabaseConfigurationException(
            "Primary index not assigned for entity you trying to update: <" + this.__structureRef.className +
            "> You should define primary index for one of property for that entity like as following:"+
            " @Property({primary:true, ... ");

    }

    /**
     * Will refresh entity data
     * -----------------------------------------------------------------------------
     * @returns {Promise<T>}
     */
    public refresh<T>(refreshRelations: boolean = false) : Promise<T> {

        // Prior to refresh check that entity is already having persistent status
        if(this.__persists) {

            const primary = this.__structureRef.primaryIndex;

            const that: {[key:string]:any} = this;

            /*
             * Entity can be retrieved only if primary index is defined for that one
             */
            if(primary !== null) {

                const id = that[primary.name];

                return new Promise<T>((resolve, reject) => {

                    const query: any = {};

                    // Create query for primary index
                    query[primary.name] = id;

                    this.__structureRef.connection.findOne(query).then(data => {

                        // On empty results
                        if(data === null)

                            reject(new ModelRejection("Not found", 404));

                        // Apply received data to entity
                        that.__assignFromSourceObject(data);
                        that.__persists = true;

                        // Refresh relations if flag is selected
                        if(refreshRelations) {

                            // Refresh all of the relations presented on entity
                            this.__refreshRelations().then(res => {

                               resolve(that as T);

                            });

                        } else {

                            // Clear all relationship data (will require re-pull operations on entity)
                            this.__clearRelationsStatusFlags();

                            // On successful result with no relations refresh
                            resolve(that as T);

                        }


                    }, error => reject(new ModelRejection(error, 500)));

                });

            }

            throw new DatabaseConfigurationException(
                "Primary index not assigned for entity you trying to refresh: <" + this.__structureRef.className +
                "> You should define primary index for one of property for that entity like as following:"+
                " @Property({primary:true, ...");

        }

        throw new WrongExecutionOrderException("\nRefresh method trying to be executed on: <"
            + this.__structureRef.className + "> entity which not persists in database or wasn't yet retrieved. "
            + "Wrong execution order detected on model, "
            + "please use find or any other pulling methods prior to calling refresh()");

    }

    /**
     * Refresh all relations which were activated previously by some pull action
     * -----------------------------------------------------------------------------
     * @returns {Promise<void>}
     * @private
     */
    private async __refreshRelations() : Promise<void> {

        const props = this.__structureRef.propertyList;

        const that: {[key:string]:any} = this;

        // Traverse properties
        for(let i = 0; i < props.length; i++) {

            // Check if property is relation
            if(props[i].isRelation()) {

                const rel = this._getRelationForProperty(props[i].name);

                // Check if relation was loaded within some operation before current refresh
                if(that[rel.propertyStatusName] === true)

                    // Pull it again if it was pulled before
                    await that[props[i].name];

            }

        }

    }

    /**
     * Clear all of the status markers on the entity relations
     * -----------------------------------------------------------------------------
     * @private
     */
    private __clearRelationsStatusFlags() {

        const props = this.__structureRef.propertyList;

        const that: {[key:string]:any} = this;

        // Traverse properties
        for(let i = 0; i < props.length; i++) {

            // Check if property is relation
            if(props[i].isRelation()) {

                const rel = this._getRelationForProperty(props[i].name);

                // Mark relation as not pulled (will require to re-pull it after this operation)
                that[rel.propertyStatusName] = false

            }

        }

    }

    /**
     * Assign entity data from Object
     * -----------------------------------------------------------------------------
     *
     * Note: fields which are protected by protect and restriction guards will
     * not be assigned to entity
     *
     * @param data
     * @returns this
     */
    public fromObject(data: any) : this {

        const that: any = this;

        for(let key in data) {

            if(data.hasOwnProperty(key)) {

                const prop = this.__structureRef.getProperty(key);

                // Check if such property exists on Model for that Key
                if (prop !== null) {

                    // Prevent protected fields from being updated from JSON model
                    if(prop.protect)

                        continue;

                    // Prevent fields which are restricted to be updated on per entity level
                    if(this.__restrictToUpdateAmount !== 0) {

                        if(this.__restrictToUpdateFields.hasOwnProperty(prop.name))

                            continue;

                    }

                    if(!prop.isRelation())
                        that[key] = data[key];

                }

            }

        }

        // Reset protect restrictions after to Object operation done
        if(this.__restrictToUpdateAmount !== 0) {

            // Reset counter
            this.__restrictToUpdateAmount = 0;

            // Reset protecting storage
            this.__restrictToUpdateFields = {};

        }

        return this;

    }

    /**
     * Assign entity data from JSON String object
     * -----------------------------------------------------------------------------
     * @param {string} string
     * @returns this
     * @throws {DataCorruptedException}
     */
    public fromJSON(string: string) : this {

        try {

            return this.fromObject(JSON.parse(string));

        } catch (e) {

            throw new DataCorruptedException("<" + this.__structureRef.className +
                "> can't be created from JSON string: "+ string +". Original Error: " + e);

        }

    }

    /**
     * Outputs entity data as an Object
     * -----------------------------------------------------------------------------
     *
     * Note: fields which are protected with expose or restriction guards
     * will not be included into final output
     *
     * @returns {{}}
     */
    public toObject() : {[key:string]:any} {

        const output:any = {};
        const that: any = this;

        const properties = this.__structureRef.propertyList;

        for(let i=0; i < properties.length; i++) {

            const property = properties[i];

            // Do not expose what shouldn't be expose
            if(!property.expose)

                continue;

            // Check if exposition restriction was defined
            if(this.__restrictToExposeAmount !== 0) {

                if(this.__restrictToExposeFields.hasOwnProperty(property.name))

                    continue;

            }

            // If property is value property then just take it
            if(!property.isRelation())

                output[property.name] = that[property.name];

            // If property is a relation
            else {

                // Pull relation configuration
                const rel = this.__structureRef.getRelationForName(property.name);

                // If relation marked as a multiple entity relation (Many)
                if(rel.outputMany) {

                    // Check if storage has some entities defined, otherwise just skip this field as unnecessary one
                    if(that[rel.propertyStorageName] !== null && that[rel.propertyStorageName].length > 0) {

                        output[property.name] = new Array(that[rel.propertyStorageName].length);
                        let i = 0;

                        // Output relational data via ModelInterface toObject method
                        that[rel.propertyStorageName].map((relObj: ModelInterface) =>
                            output[property.name][i++] = relObj.toObject()
                        );

                    }

                }
                // If relation is marked as handling single entity relation (can be OTM)
                else if(that[rel.propertyStorageName] !== null)

                    output[property.name] = that[rel.propertyStorageName].toObject();

            }

        }

        // Reset expose restrictions after to Object operation done
        if(this.__restrictToExposeAmount !== 0) {

            // Reset counter
            this.__restrictToExposeAmount = 0;

            // Reset exposing storage
            this.__restrictToExposeFields = {};

        }

        return output;

    }

    /**
     * Outputs entity data as a JSON String
     * -----------------------------------------------------------------------------
     * @returns {string}
     * @throws {DataCorruptedException}
     */
    public toJSON() {

        try {

            return JSON.stringify(this.toObject());

        } catch (e) {

            throw new DataCorruptedException("<" + this.__structureRef.className
                + "> can't be transformed to JSON string because of error: " + e);

        }

    }

    /**
     * Define set of fields which should not be assigned with fromObject method
     * -----------------------------------------------------------------------------
     * @param {Array<string>} fieldNames
     * @returns this
     */
    public protectToUpdate(fieldNames: Array<string>) : this {

        for(let i = 0; i < fieldNames.length; i++) {

            this.__restrictToUpdateFields[fieldNames[i]] = true;

            this.__restrictToUpdateAmount++;

        }

        return this;

    }

    /**
     * Define set of fields which should be prevented to be exposed with toObject
     * -----------------------------------------------------------------------------
     * @param {Array<string>} fieldNames
     * @returns this
     */
    public protectToExpose(fieldNames: Array<string>) : this {

        for(let i = 0; i < fieldNames.length; i++) {

            this.__restrictToExposeFields[fieldNames[i]] = true;

            this.__restrictToExposeAmount++;

        }

        return this;

    }

    /**
     * Will assign data from some Object of <key, value>
     * -----------------------------------------------------------------------------
     * @param data
     * @returns {MongoModel}
     * @protected
     */
    public __assignFromSourceObject(data: any) {

        // Interrupt on no data available
        if(data === null)
            return this;

        const that: any = this;

        // Assign default mongo _id property if found on assigning object
        if(data.hasOwnProperty("_id"))
            that["_id"] = data["_id"];

        const properties = this.__structureRef.propertyList;

        for(let i=0; i < properties.length; i++) {

            const property = properties[i];

            // If property is not represented as relation then take it as is
            if(!property.isRelation()) {

                if (data.hasOwnProperty(property.column)) {

                    const value = data[property.column];

                    // If value null then assign it back to property
                    if(value === null)

                        that[property.name] = value;

                    // If it's type of Date then convert it back to Date
                    else if(property.type === Type.Date)

                        that[property.name] = new Date(value);

                    else

                        that[property.name] = value;

                }

            }
            // If property is relational then do specific cases
            else {

                const rel = this.__structureRef.getRelationForName(property.name);

                // If property linked as Belongs to One
                // assign value to hidden relational property
                if(!rel.master && !rel.outputMany)

                    that[rel.propertyIdValueName] = data[property.column];

            }

        }

        // Make persists based on primary index
        if(this.__structureRef.primaryIndex !== null) {

            if(that[this.__structureRef.primaryIndex.name] !== null)
                that.__persists = true;

        }

        return this;

    }

    /**
     * Provides pagination object for requests which can happen at instance
     * level of entity (non static), for example relation requests
     * -----------------------------------------------------------------------------
     *
     * Usage:
     *
     *  // Make selection of entity instance from persistent storage
     *  let entity = await Entity.find<Entity>([id:number|string]);
     *
     *  // Pull relation data which is defined as relational data
     *  let relationData = await entity.paginate(100,1).relationItems;
     *
     * @returns {PaginationInterface}
     * @private
     */
    public __usePaginationForNextRequest() : PaginationInterface {

        const pi: PaginationInterface = {
            pagination: false,
            items: 100,
            offset: 0
        };

        if(this.__paginateNextItems !== null && this.__paginateNextItems > 0) {

            pi.pagination = true;
            pi.items = this.__paginateNextItems;

            if(this.__paginateNextPage !== null)
                pi.offset = this.__paginateNextPage > 0 ? ((this.__paginateNextPage - 1) * this.__paginateNextItems) : 0;

            this.__paginateNextItems = null;
            this.__paginateNextPage = null;

        }

        return pi;

    }

    /**
     * Provides list of objects for next request will be using some orderBy some
     * property which persists with object
     * -----------------------------------------------------------------------------
     *
     * Usage:
     *
     *  // Make selection of entity instance from persistent storage
     *  let entity = await Entity.find<Entity>([id:number|string]);
     *
     *  // Pull relation data which is defined as relational data
     *  let relationData = await entity.orderBy(...).relationItems;
     *
     * @returns {PaginationInterface}
     * @private
     */
    public __useOrderForNextRequest() : Array<OrderByModelInterface> {

        const o = this.__orderNextRequest;

        this.__orderNextRequest = null;

        return o;

    }


    /**
     * Provides boolean flag of type of action which current instance need to do next
     * while saving relational data. If it is exclusion mode then all dependency which
     * passed as exclusion should be unlinked to current property.
     *
     * However if there is no linkage at all - management of dependency existence
     * should be on conscience of application developer, who need to proceed with:
     *
     *      entity.delete() action
     *
     * -----------------------------------------------------------------------------
     * @returns {boolean}
     * @private
     */
    public __useExclusionForNextRequest() : boolean {

        if(this.__exclusionMode) {

            this.__exclusionMode = false;
            return true;

        }

        return false;

    }

    /**
     * Get object if next request should be with Seek relation capability
     * -----------------------------------------------------------------------------
     * @returns {QueryConstructorProduct}
     * @private
     */
    public __useSeekForNextRequest(seekValue?: QueryConstructorProduct) : SeekForRelationInterface {

        if(typeof seekValue !== "undefined") {
            this.__seekMode = seekValue;
            return null;
        }

        if(this.__seekMode !== null) {

            // Take seeking parameters
            const seekFor = this.__seekMode;

            // Reset seek mode
            this.__seekMode = null;

            // Return Seek Relational object
            return {
                executor: new MongoQueryExecutor(this.__structureRef),
                query: seekFor
            };

        }

        return null;

    }

    /**
     *
     * -----------------------------------------------------------------------------
     * @param definition
     * @param prototype
     * @private
     */
    public static __initializeModelDefinition(definition: ModelType, prototype: Function) : TypedStructure {

        let structure = null;

        const classNameRegEx = /(?:\S+\s+){1}([a-zA-Z_$][0-9a-zA-Z_$]*)/;
        const className =  classNameRegEx.exec(prototype.toString())[1];
        /*
         *  Check if schema is already created for class which property belongs to
         */
        if(!MongoModel.__schemaStorage.has(className)) {

            structure = MongoModel.__createNewStructureForName(className);

            MongoModel.__schemaStorage.set(className, structure);

        } else

            structure = MongoModel.__schemaStorage.get(className);

        if(structure.classPrototype === null)

            structure.classPrototype = prototype;

        if(definition.hasOwnProperty("sourceDb"))

            structure.connectionName = definition.sourceDb;

        return structure;

    }

    /**
     * Will make property initialization for class structure
     * -----------------------------------------------------------------------------
     * @param {TypedProperty} property
     * @param {Function} prototype
     * @public
     */
    public static __initializeProperty(property: TypedProperty, prototype: Function) {

        let structure = null;

        /*
         *  Check if schema is already created for class which property belongs to
         */
        if(!MongoModel.__schemaStorage.has(property.getClassName())) {

            structure = MongoModel.__createNewStructure(property);

            MongoModel.__schemaStorage.set(property.getClassName(), structure);

        }
        /*
         *  Get schema if it exists
         */
        else

            structure = MongoModel.__schemaStorage.get(property.getClassName());

        structure.assignProperty(property);

        if(structure.classPrototype === null)

            structure.classPrototype = prototype;

    }

    /**
     * Will create Mongo schema from existing Virtual structure
     * -----------------------------------------------------------------------------
     * @public
     */
    public static __initializeStructure(s: TypedStructure) {
        MongoInitializer.init(s);
    }

    /**
     * Will return structure assigned for current model
     * -----------------------------------------------------------------------------
     * @returns {TypedStructure}
     * @public
     */
    public __getStructure() : TypedStructure {

        const structure = MongoModel.__schemaStorage.get(this.constructor.name);

        /*
         *  Check if return from Map was initialized structure
         */
        if(typeof structure !== "undefined") {

            if (this.hasOwnProperty("__structureRef"))
                this.__structureRef = structure;

        }
        /*
         *  Throw error if configuration is not available for model
         */
        else

            throw new ConfigurationException(
                "Mongo Model for <" + this.constructor.name
                + "> was not configured or instantiated with any parameters.", 400);

        /*
         *  If structure was not initialized yet - process initialization
         */
        if(structure.connection === null)
            MongoModel.__initializeStructure(structure);

        return structure;

    }

    /**
     * Will return structure for Static instance
     * -----------------------------------------------------------------------------
     * @param {Function} instance
     * @returns {TypedStructure}
     * @public
     */
    public static __getStructureStatic(instance: Function) : TypedStructure {

        const classNameRegEx = /(?:\S+\s+){1}([a-zA-Z_$][0-9a-zA-Z_$]*)/;
        const className =  classNameRegEx.exec(instance.toString())[1];

        const structure = MongoModel.__schemaStorage.get(className);

        /*
         *  If structure was not initialized yet - process initialization
         */
        if(structure.connection === null)
            MongoModel.__initializeStructure(structure);

        return structure;

    }

    /**
     * Will return structure for Static instance
     * -----------------------------------------------------------------------------
     * @param {string} name
     * @returns {TypedStructure}
     * @public
     */
    public static __getStructureByName(name: string) : TypedStructure | null {

        if(MongoModel.__schemaStorage.has(name))

            return MongoModel.__schemaStorage.get(name);

        return null;

    }

    /**
     * Will return structure for Static instance
     * -----------------------------------------------------------------------------
     * @returns {Array<TypedStructure>}
     * @public
     */
    public static __getStructureList() : Array<TypedStructure> {

        return Array.from(MongoModel.__schemaStorage.values());

    }

    /**
     * Pull list of entities paginated
     * -----------------------------------------------------------------------------
     * @param {number} page
     * @param {number} items
     * @param {Array<OrderByModelInterface>} orderBy
     * @returns {Promise<Array<number|string>>}
     */
    public static __idList(page: number = 1,
                           items: number = 100,
                           orderBy: Array<OrderByModelInterface> = null) : Promise<Array<number|string>> {

        const prototypeOfInstance: any = this;

        const schema: StructureMongoInterface = MongoModel.__getStructureStatic(prototypeOfInstance);

        // Select index name
        const primary = schema.primaryIndex;
        const primaryName = primary.name;

        // If index is not defined operation will not be completed
        if(primary === null)

            throw new Exception("[Model Error] For system __idList method primary index" +
                " should be defined for that model", 500);

        return new Promise<Array<number|string>>((resolve, reject:(v: ModelRejection) => void) => {

            // Define projection (eliminate possible _id) case
            let project: {[key: string] : number|string} = {_id:-1};

            // Set main index as projection
            project[primaryName] = 1;

            // Define sorts if any presented
            let sorts: {[key: string] : number|string} = {};

            // Fill sorts
            if(orderBy !== null) {

                for(let i = 0; i < orderBy.length; i++)

                    sorts[orderBy[i].name] = orderBy[i].order === "desc" ? -1 : 1;

            }

            // Make request
            schema.connection
                .find()
                .project(project)
                .skip((page - 1) * items)
                .sort(sorts)
                .limit(items)
                .toArray().then(

                // On successful query resolution
                data => {

                    // On empty results
                    if(data === null)

                        reject(new ModelRejection("Not found", 404));

                    const result: Array<number|string> = new Array(data.length);

                    // Transform to list if ID
                    for(let i = 0; i < data.length; i++)

                        result[i] = data[i][primaryName];

                    // Return as array of indexes
                    resolve(result);

                },
                // On server error
                error => reject(new ModelRejection(error, 500)));

        });

    }

    /**
     * Set the value of auto counter to some number value, or create that counter
     * -----------------------------------------------------------------------------
     * @returns {Promise<boolean>}
     * @private
     */
    public static __adjustAutoCounter() : Promise<boolean> {

        return new Promise<boolean>((resolve, reject) => {

            resolve(true);

        });

    }

    /**
     * Save new entity while primary index not a concern of Model but provided value
     * -----------------------------------------------------------------------------
     * @param {MongoModel} instance
     * @param {PrimaryIndex} primaryIndex
     * @param {{}}query
     * @param {Function} resolve
     * @param {(reason: ModelRejection) => void} reject
     * @private
     */
    private __saveNoIncrement<T>(
        instance: MongoModel & any,
        primaryIndex: PrimaryIndex,
        query: {[key:string] : any},
        resolve: Function,
        reject: (reason: ModelRejection) => void) {

        // Check if Primary index value is defined
        if(typeof query[primaryIndex.name] === "undefined" || query[primaryIndex.name] === null)

            reject(new ModelRejection(
                "As your primary index for model: <" + this.__structureRef.className +
                "> is not defined as auto increment index - you must manually provide value "+
                "for that primary index field: [" + primaryIndex.name + "] prior to save entity as new", 400));

        try {
         
	        // Check on relationships prior to saving
	        ModelFlows.satisfyRelationshipsUponCreation(this.__structureRef, instance, query);
	
        } catch (e) {
	
	        new ModelRejection(e.message, 500);
	
        }

        // Second - start insert operation for entity
        this.__structureRef.connection.insertOne(query).then(
            data => {

                // Reassign primary key value from data came with response
                instance[primaryIndex.name] = data.ops[0][primaryIndex.name];

                // Mark entity as persistent, meaning that system confirms it exists on perm basis
                instance.__persists = true;

                // Continue with promise resolution by checking if there any unresolved MTM rel
                ModelFlows.completeRelationalPropagation(this.__structureRef, instance).then(
                    success => resolve(instance),
                    error=>reject(error)
                );

            },
            // Saving rejection
            error => reject(new ModelRejection(error, 500))
        );

    }

    /**
     * Save new entity and increment it's primary index
     * -----------------------------------------------------------------------------
     * @param {MongoModel} instance
     * @param {PrimaryIndex} primaryIndex
     * @param {{}} query
     * @param {Function} resolve
     * @param {(reason: ModelRejection) => void} reject
     * @private
     */
    private __saveAndIncrement<T>(
        instance: MongoModel & any,
        primaryIndex: PrimaryIndex,
        query: {[key:string] : any},
        resolve: Function,
        reject: (reason: ModelRejection) => void) {

        try {
         
	        // Check on relationships prior to saving
	        ModelFlows.satisfyRelationshipsUponCreation(this.__structureRef, instance, query);
	
        } catch (e) {
	
	        new ModelRejection(e.message, 500);
         
        }
        
        //First get next ID sequence for that class entity
        this.__structureRef.counter.incrementAndGet(this.__structureRef.className).then(
            counter => {

                // Assign new id to primary index
                query[primaryIndex.name] = counter;

                // Second - start insert operation for entity
                this.__structureRef.connection.insertOne(query).then(data => {

                        // Reassign primary key value from data came with response
                        instance[primaryIndex.name] = data.ops[0][primaryIndex.name];

                        // Mark entity as persistent, meaning that system confirms it exists on perm basis
                        instance.__persists = true;

                        // Continue with promise resolution by checking if there any unresolved MTM rel
                        ModelFlows.completeRelationalPropagation(this.__structureRef, instance).then(
                            success => resolve(instance),
                            error=>reject(error)
                        );

                    },
                    // Saving rejection
                    error => reject(new ModelRejection(error, 500))
                );

            },
            // Incremental Rejection
            error => reject(new ModelRejection(error, 500))
        );

    }

    private async __rollback() {

        if(this.__persists) {



        }

    }

    /**
     * Truncate data for some collection at some offset (bulk delete at offset)
     * -----------------------------------------------------------------------------
     * @param {"mongoose".Collection} conn
     * @param {ModelStaticInterface} proto
     * @param {number} offset
     * @param {number} items
     * @returns {Promise<number>}
     * @private
     */
    private static __truncateOffset(conn: Collection, proto: ModelStaticInterface, offset: number, items: number) {

        // Do find in async promise
        return new Promise<number>((resolve, reject:(v: ModelRejection) => void) => {

            conn.find().skip(offset * items).limit(items).toArray().then(

                data => {

                    // On empty results
                    if(data === null)
                        reject(new ModelRejection("Not found", 404));

                    // On empty results
                    if(typeof data === "undefined")
                        reject(new ModelRejection("Not found", 404));

                    // If data is not list
                    if(!Array.isArray(data))
                        reject(new ModelRejection("Not found", 404));

                    const result: Array<MongoModel> = new Array(data.length);

                    // Preassemble chunk of data for prebuild relationship information
                    for(let i = 0; i < data.length; i++) {

                        let instance = new proto().__assignFromSourceObject(data[i]) as MongoModel;
                        instance.__persists = true;
                        result[i] = instance;

                    }

                    // Proceed with truncation of chunk
                    MongoModel.__truncateList(result).then(
                        count => resolve(count),
                        error => reject(error));

                }, error => {

                    reject(new ModelRejection(error, 500));

                }

            )

        });

    }

    /**
     * Make Async operation of items full delete procedure, where result is guaranteed
     * number of deleted items or Error exception
     * -----------------------------------------------------------------------------
     * @param {Array<ModelInterface>} list
     * @returns {Promise<number>}
     * @private
     */
    private static async __truncateList(list: Array<ModelInterface>) : Promise<number> {

        let k = 0;

        try {

            for (let i = 0; i < list.length; i++) {

                await list[i].delete();
                k++;

            }

        } catch (e) {

            throw e;

        }

        return k;

    }

}

/**
 * Parameter definition for assign behavioral attributes on some Model
 * -----------------------------------------------------------------------------
 * @param definition
 * @constructor
 */
export const Model = (definition: ModelType) : any => {

    return (target: any) => {

        const struct = MongoModel.__initializeModelDefinition(definition, target);

        struct.sourceStorageType = ModelSource.Mongo;

        return;

    }

};

/**
 * Parameter definition for mark model properties as storage presented items
 * -----------------------------------------------------------------------------
 * @param {PropertyType} definition
 * @returns {Function}
 * @constructor
 */
export const Property = (definition?: PropertyType) : Function => {

    return (target: MongoModel = null,
            key: string = null,
            type: TypedPropertyDescriptor<any> = null) : any => {

        const r = Reflect as any;

        const systemType = r.getMetadata("design:type", target, key);

        const property = TypedPropertyFactory.createFromPropertyDefinition(target, key, systemType, definition);

        property.parentObjectModelStorage = ModelSource.Mongo;

        MongoModel.__initializeProperty(property, target as any);

        return;

    };

};

/**
 * Relation definition for mark model relations
 * -----------------------------------------------------------------------------
 * @param {() => Function} of
 * @param {string} through
 * @returns {Function}
 * @constructor
 */
export const HasMany = (of: () => Function, through?: string) : Function => {

    return (target: MongoModel = null,
            key: string = null,
            type: TypedPropertyDescriptor<any> = null) : any => {

        const r = Reflect as any;

        const systemType = r.getMetadata("design:type", target, key);

        const property = TypedPropertyFactory
            .createFromRelationDefinition(target, key, systemType, Relations.hasMany, of, through);

        property.parentObjectModelStorage = ModelSource.Mongo;

        MongoModel.__initializeProperty(property, target as any);

        return;

    }

};

/**
 * Relation definition for mark model relations
 * -----------------------------------------------------------------------------
 * @param {() => Function} of
 * @param {string} through
 * @returns {Function}
 * @constructor
 */
export const HasOne = (of: () => Function, through?: string) : Function => {

    return (target: MongoModel = null,
            key: string = null,
            type: TypedPropertyDescriptor<any> = null) : any => {

        const r = Reflect as any;

        const systemType = r.getMetadata("design:type", target, key);

        const property = TypedPropertyFactory
            .createFromRelationDefinition(target, key, systemType, Relations.hasOne, of, through);

        property.parentObjectModelStorage = ModelSource.Mongo;

        MongoModel.__initializeProperty(property, target as any);

        return;

    }

};

/**
 * Relation definition for mark model relations
 * -----------------------------------------------------------------------------
 * @param {() => Function} of
 * @param {BelongsRelationPropertyType} parameters
 * @returns {Function}
 * @constructor
 */
export const BelongsToMany = (of: () => Function, parameters?: BelongsRelationPropertyType) : Function => {

    return (target: MongoModel = null,
            key: string = null,
            type: TypedPropertyDescriptor<any> = null) : any => {

        const r = Reflect as any;

        const systemType = r.getMetadata("design:type", target, key);

        let through = null;
        let weak = false;
        let unique = false;
        let uniqueWith = [];

        // Parse parameters object to have wide extension of Relation behavior
        if(typeof parameters === "object") {

            if(parameters.hasOwnProperty("through"))
                through = parameters.through;

            if(parameters.hasOwnProperty("weak"))
                weak = Boolean(parameters.weak);

            if(parameters.hasOwnProperty("unique"))
                unique = Boolean(parameters.unique);

            if(parameters.hasOwnProperty("uniqueWith") && Array.isArray(parameters.uniqueWith))
                uniqueWith = parameters.uniqueWith;

        }

        // Take relation properties and create Structure property for relation
        const property = TypedPropertyFactory
            .createFromRelationDefinition(target, key, systemType, Relations.belongsToMany, of, through, weak);

        // Set storage type definition on schema for Schema can understand which
        // type of initializer or flow to use
        property.parentObjectModelStorage = ModelSource.Mongo;

        MongoModel.__initializeProperty(property, target as any);

        return;

    }

};

/**
 * Relation definition for mark model relations
 * -----------------------------------------------------------------------------
 * @param {() => Function} of
 * @param {BelongsRelationPropertyType} parameters
 * @returns {Function}
 * @constructor
 */
export const BelongsToOne = (of: () => Function, parameters?: BelongsRelationPropertyType) : Function => {

    return (target: MongoModel = null,
            key: string = null,
            type: TypedPropertyDescriptor<any> = null) : any => {

        const r = Reflect as any;

        const systemType = r.getMetadata("design:type", target, key);

        let through = null;
        let weak = false;
        let unique = false;
        let uniqueWith = [];

        // Parse parameters object to have wide extension of Relation behavior
        if(typeof parameters === "object") {

            if(parameters.hasOwnProperty("through"))
                through = parameters.through;

            if(parameters.hasOwnProperty("weak"))
                weak = Boolean(parameters.weak);

            if(parameters.hasOwnProperty("unique"))
                unique = Boolean(parameters.unique);

            if(parameters.hasOwnProperty("uniqueWith") && Array.isArray(parameters.uniqueWith))
                uniqueWith = parameters.uniqueWith;

        }

        // Take relation properties and create Structure property for relation
        const property = TypedPropertyFactory
            .createFromRelationDefinition(target, key, systemType, Relations.belongsToOne, of, through, weak);

        // Set storage type definition on schema for Schema can understand which
        // type of initializer or flow to use
        property.parentObjectModelStorage = ModelSource.Mongo;

        MongoModel.__initializeProperty(property, target as any);

        return;

    }

};