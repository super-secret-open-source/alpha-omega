/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Counter schema to support auto mode for objects
 *
 ***************************************************************/
import {Collection} from "mongoose";
import {ConfigurationException} from "../../exceptions/exceptions";
import {ModelCounterInterface} from "../common/model";

/**
 *  Mongo Counter
 *  --------------------------------------------------------------------
 *  Provides capability to do auto-increments
 *  on specific models presented
 *  in the system.
 */
export class MongoCounter implements ModelCounterInterface {

    private totalObjects: number = 0;

    private connector: Collection = null;

    /**
     * Initialize Counter model with appropriate connection layer
     * --------------------------------------------------------------------
     * @param {Collection} connector
     */
    constructor(connector: Collection) {
        this.connector = connector;
    }

    /**
     * Make counter pre-initialization
     * --------------------------------------------------------------------
     * @returns {Promise<boolean>}
     */
    public prepare() : Promise<boolean> {

        return new Promise<boolean>((resolve, reject) => {

            this.connector.count({}).then((amount) => {

                this.totalObjects = Number(amount);

                resolve(true);

            }, err => reject(err));

        });

    }

    /**
     * Will prepare counter model for serve index counting for Mongo Models
     * --------------------------------------------------------------------
     * @param {string} modelName
     * @returns {Promise<boolean>}
     */
    public prepareForModel(modelName: string) : Promise<boolean> {

        return new Promise<boolean>((resolve, reject) => {

            this.connector.findOne({name: modelName}).then(data => {

                // If data was not found — create counter for model
                if(data === null) {

                    this.connector.findOneAndUpdate(
                        {name: modelName},
                        {id: (++this.totalObjects), name: modelName, counter: 0},
                        {upsert: true})
                        // Data was created and counter initialized
                        .then(data => {

                            // Check data Ok result to be a positive number or zero
                            if (data.ok > -1)

                                resolve(true);

                            // Otherwise it is not pretty much a lot of information to be
                            // sure that counter was properly created, so proceed with rejection
                            else

                                reject(new ConfigurationException(
                                    "Can't create counter for model <" + modelName + '>', 500));

                        },
                            // Counter wasn't able to be initialized
                            err => reject(err)

                    );

                }
                // If data found then counter is created and system can proceed with that model
                else
                    resolve(true);

            },
            err => reject(err));

        });

    }

    /**
     * Will make increment and get operation for Mongo Models
     * --------------------------------------------------------------------
     * @param {string} modelName
     * @returns {Promise<Number>}
     */
    public incrementAndGet(modelName: string) : Promise<number> {

        return new Promise<number>((resolve, reject) => {

            this.connector.findOneAndUpdate(
                {name: modelName},
                {$inc: {counter: 1}},
                {upsert:true}).then(

                data => {

                    if(data.value === null)
                        resolve(1);
                    else
                        resolve(++data.value.counter);

                },error => {

                    reject(error);

                }

            );

        });

    }

    /**
     * Reset counter for some of the model name
     * --------------------------------------------------------------------
     * That operation should go strictly after truncate operation, which
     * will wipe all existing data from table and index for next counter
     * operation produce expected and proper result.
     *
     * @param {string} modelName
     * @returns {Promise<boolean>}
     */
    public resetForModel(modelName: string) : Promise<boolean> {

        return new Promise<boolean>((resolve, reject) => {

            this.connector.findOneAndUpdate(
                {name: modelName},
                {$set : {counter: 0}},
                {upsert:true}).then(

                data => {

                    if(data.value === null)
                        resolve(true);
                    else
                        resolve(true);

                },error => {

                    reject(error);

                }

            );

        });

    }

    /**
     * Set new counter value for some model Name
     * --------------------------------------------------------------------
     * @param {string} modelName
     * @param {number} value
     * @returns {Promise<boolean>}
     */
    public setForModel(modelName: string, value: number) : Promise<boolean> {

        return new Promise<boolean>((resolve, reject) => {

            this.connector.findOneAndUpdate(
                {name: modelName},
                {$set : {counter: value}},
                {upsert:true}).then(

                data => {

                    if(data.value === null)
                        resolve(true);
                    else
                        resolve(true);

                },error => {

                    reject(error);

                }

            );

        });

    }

}