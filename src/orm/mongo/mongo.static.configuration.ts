/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Mongo static configuration container
 *
 ***************************************************************/
import {ModelCounterInterface} from "../common/model";

export class MongoStaticConfiguration {

    private static counters: {[key: string]: ModelCounterInterface} = {};

    public static addCounter(dbName: string, counter: ModelCounterInterface) {

        MongoStaticConfiguration.counters[dbName] = counter;

    }

    public static getCounter(dbName: string) {

        if(MongoStaticConfiguration.counters.hasOwnProperty(dbName)) {

            return MongoStaticConfiguration.counters[dbName];

        }

        return null;

    }

}