/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Mongo complex Joins manager
 *
 ***************************************************************/
import {ModelJoinerInterface, ModelRejection, PaginationInterface} from "../common/model";
import {Cursor, DeleteWriteOpResultObject, UpdateWriteOpResult, Collection} from "mongodb";

export class MongoJoinerModel implements ModelJoinerInterface {

    private collection: Collection<any> = null;

    private masterProperty: string = null;

    private slaveProperty: string = null;

    __setCollection(c: any): void {
        this.collection = c as any;
    }

    __setMasterName(n: string): void {
        this.masterProperty = n;
    }

    __setSlaveName(n: string): void {
        this.slaveProperty = n;
    }

    /**
     * Get all values of Slave records by polling for Master ID
     * ---------------------------------------------------------------------
     * @param {number} value
     * @param {PaginationInterface} pagination
     * @returns {Promise<Array<number>>}
     */
    public getWhereMasterIs(value: number, pagination?: PaginationInterface): Promise<Array<number>> {

        let query: any = {};

        query[this.masterProperty] = value;

        let proj: any = {};

        // Append projection to not include _id field (if exists) into output
        proj[this.slaveProperty] = 1;
        proj["_id"] = 0;

        let cursor: Cursor<any> = this.collection.find(query).project(proj);

        if(pagination.pagination) {
            cursor.limit(pagination.items);
            cursor.skip(pagination.offset);
        }

        return new Promise((resolve, reject) => {

             cursor.toArray().then(data => {

                 const o = new Array(data.length);

                 for(let i = 0; i < data.length; i++)
                     o[i] = data[i][this.slaveProperty];

                 resolve(o);

             }, error => {

                 reject(new ModelRejection(error, 500));

             });

        });

    }

    /**
     * Get all values of Slave records by polling for list of Master ID
     * ---------------------------------------------------------------------
     * @param {number[]} valueList
     * @param {PaginationInterface} pagination
     * @returns {Promise<Array<number>>}
     */
    public getWhereMasterIn(valueList: Array<number>, pagination?: PaginationInterface) : Promise<Array<number>> {

        let query: any = {};

        query[this.masterProperty] = {$in: valueList};

        let proj: any = {};

        // Append projection to not include _id field (if exists) into output
        proj[this.slaveProperty] = 1;
        proj["_id"] = 0;

        let cursor: Cursor<any> = this.collection.find(query).project(proj);

        if(pagination.pagination) {
            cursor.limit(pagination.items);
            cursor.skip(pagination.offset);
        }

        return new Promise((resolve, reject) => {

            cursor.toArray().then(data => {

                const o = new Array(data.length);

                for(let i = 0; i < data.length; i++)
                    o[i] = data[i][this.slaveProperty];

                resolve(o);

            }, error => {

                reject(new ModelRejection(error, 500));

            });

        });

    }

    /**
     * Get all values of Master records by polling for Slave Id
     * ---------------------------------------------------------------------
     * @param {number} value
     * @param {PaginationInterface} pagination
     * @returns {Promise<Array<number>>}
     */
    public getWhereSlaveIs(value: number, pagination?: PaginationInterface): Promise<Array<number>> {

        let query: any = {};

        query[this.slaveProperty] = value;

        let proj: any = {};

        // Append projection to not include _id field (if exists) into output
        proj[this.masterProperty] = 1;
        proj["_id"] = 0;

        let cursor: Cursor<any> = this.collection.find(query).project(proj);

        if(pagination.pagination) {
            cursor.limit(pagination.items);
            cursor.skip(pagination.offset);
        }

        return new Promise((resolve, reject) => {

            cursor.toArray().then(data => {

                const o = new Array(data.length);

                for(let i = 0; i < data.length; i++)
                    o[i] = data[i][this.masterProperty];

                resolve(o);

            }, error => {

                reject(new ModelRejection(error, 500));

            });

        });

    }

    /**
     * Get all values of Master records by polling for list of Slave ID
     * ---------------------------------------------------------------------
     * @param {number[]} valueList
     * @param {PaginationInterface} pagination
     * @returns {Promise<Array<number>>}
     */
    public getWhereSlaveIn(valueList: Array<number>, pagination?: PaginationInterface) : Promise<Array<number>> {

        let query: any = {};

        query[this.slaveProperty] = {$in: valueList};

        let proj: any = {};

        // Append projection to not include _id field (if exists) into output
        proj[this.masterProperty] = 1;
        proj["_id"] = 0;

        let cursor: Cursor<any> = this.collection.find(query).project(proj);

        if(pagination.pagination) {
            cursor.limit(pagination.items);
            cursor.skip(pagination.offset);
        }

        return new Promise((resolve, reject) => {

            cursor.toArray().then(data => {

                const o = new Array(data.length);

                for(let i = 0; i < data.length; i++)
                    o[i] = data[i][this.masterProperty];

                resolve(o);

            }, error => {

                reject(new ModelRejection(error, 500));

            });

        });

    }

    /**
     * Get all values of intersection for Master Id and List of Slave Id
     * ---------------------------------------------------------------------
     * @param {number} masterValue
     * @param {number[]} valueList
     * @param {PaginationInterface} pagination
     * @returns {Promise<Array<number>>}
     */
    public getWhereMasterCrossingList(masterValue: number,
                                      valueList: Array<number>,
                                      pagination?: PaginationInterface) : Promise<Array<number>> {

        const masterQuery: any = {};
        masterQuery[this.masterProperty] = masterValue;

        const slaveQuery: any = {};
        slaveQuery[this.slaveProperty] = {$in: valueList};

        let query: any = {$and: [masterQuery, slaveQuery]};

        let proj: any = {};

        // Append projection to not include _id field (if exists) into output
        proj[this.slaveProperty] = 1;
        proj["_id"] = 0;

        let cursor: Cursor<any> = this.collection.find(query).project(proj);

        if(pagination.pagination) {
            cursor.limit(pagination.items);
            cursor.skip(pagination.offset);
        }

        return new Promise((resolve, reject) => {

            cursor.toArray().then(data => {

                const o = new Array(data.length);

                for(let i = 0; i < data.length; i++)
                    o[i] = data[i][this.slaveProperty];

                resolve(o);

            }, error => {

                reject(new ModelRejection(error, 500));

            });

        });

    }

    /**
     * Get all values of intersection for Slave Id and List of Master Id
     * ---------------------------------------------------------------------
     * @param {number} slaveValue
     * @param {Array<number>} valueList
     * @param {PaginationInterface} pagination
     * @returns {Promise<Array<number>>}
     */
    public getWhereSlaveCrossingList(slaveValue: number,
                                     valueList: Array<number>,
                                     pagination?: PaginationInterface) : Promise<Array<number>> {

        const masterQuery: any = {};
        masterQuery[this.masterProperty] = {$in: valueList};

        const slaveQuery: any = {};
        slaveQuery[this.slaveProperty] = slaveValue;

        let query: any = {$and: [slaveQuery, masterQuery]};

        let proj: any = {};

        // Append projection to not include _id field (if exists) into output
        proj[this.masterProperty] = 1;
        proj["_id"] = 0;

        let cursor: Cursor<any> = this.collection.find(query).project(proj);

        if(pagination.pagination) {
            cursor.limit(pagination.items);
            cursor.skip(pagination.offset);
        }

        return new Promise((resolve, reject) => {

            cursor.toArray().then(data => {

                const o = new Array(data.length);

                for(let i = 0; i < data.length; i++)
                    o[i] = data[i][this.masterProperty];

                resolve(o);

            }, error => {

                reject(new ModelRejection(error, 500));

            });

        });

    }

    /**
     * Add Master Slave pair
     * ---------------------------------------------------------------------
     * Add pair of values where Master Id is a primary relation Id index
     * and Slave Id is a belonging relationship index
     *
     * @param {number} master
     * @param {number} slave
     * @returns {Promise<UpdateWriteOpResult>}
     */
    public addMasterSlavePair(master: number, slave: number): Promise<UpdateWriteOpResult> {

        let query: any = {};

        query[this.masterProperty] = master;
        query[this.slaveProperty] = slave;

        return this.collection.updateOne(query, query, {upsert:true});

    }

    /**
     * Delete pair of master <-> slave relationship
     * ---------------------------------------------------------------------
     * @param {number} master
     * @param {number} slave
     * @returns {Promise<DeleteWriteOpResultObject>}
     */
    public removeMasterSlavePair(master: number, slave: number): Promise<DeleteWriteOpResultObject> {

        let query: any = {};

        query[this.masterProperty] = master;
        query[this.slaveProperty] = slave;

        return this.collection.deleteOne(query);

    }

    /**
     * Remove bulk data relationship by erasing all pairs for some Slave Id
     * ---------------------------------------------------------------------
     * @param {number} value
     * @returns {Promise<DeleteWriteOpResultObject>}
     */
    public removeWhereSlaveIs(value: number) : Promise<DeleteWriteOpResultObject> {

        let query: any = {};

        query[this.slaveProperty] = value;

        return this.collection.deleteMany(query);

    }

    /**
     * Count amount of relations for slave entity index
     * ---------------------------------------------------------------------
     * @param {number} slave
     * @returns {Promise<number>}
     */
    public countSlavePairs(slave: number) : Promise<number> {

        let query: any = {};

        query[this.slaveProperty] = slave;

        return this.collection.count(query) as any;

    }

    /**
     * Count amount of relations for master entity index
     * ---------------------------------------------------------------------
     * @param {number} master
     * @returns {Promise<number>}
     */
    public countMasterPairs(master: number) : Promise<number> {

        let query: any = {};

        query[this.masterProperty] = master;

        return this.collection.count(query) as any;

    }

    /**
     * List Master records
     * ---------------------------------------------------------------------
     * @param {PaginationInterface} pagination
     * @returns {Promise<Array<number>>}
     */
    public listMaster(pagination: PaginationInterface) : Promise<Array<number>> {

        let query: any = {};

        let proj: any = {};

        // Append projection to not include _id field (if exists) into output
        proj[this.masterProperty] = 1;
        proj["_id"] = 0;

        let cursor: Cursor<any> = this.collection.find(query).project(proj);

        if(pagination.pagination) {
            cursor.limit(pagination.items);
            cursor.skip(pagination.offset);
        }

        return new Promise((resolve, reject) => {

            const unique: any = {};

            cursor.toArray().then(data => {

                const o = [];

                for(let i = 0, k = 0; i < data.length; i++) {

                    const val = data[i][this.masterProperty];

                    if(!unique.hasOwnProperty(val)) {
                        o[k++] = val;
                        unique[val] = true;
                    }

                }

                resolve(o);

            }, error => {

                reject(new ModelRejection(error, 500));

            });

        });

    }

    /**
     * List Slave Records
     * ---------------------------------------------------------------------
     * @param {PaginationInterface} pagination
     * @returns {Promise<Array<number>>}
     */
    public listSlave(pagination: PaginationInterface) : Promise<Array<number>> {

        let query: any = {};

        let proj: any = {};

        // Append projection to not include _id field (if exists) into output
        proj[this.slaveProperty] = 1;
        proj["_id"] = 0;

        let cursor: Cursor<any> = this.collection.find(query).project(proj);

        if(pagination.pagination) {
            cursor.limit(pagination.items);
            cursor.skip(pagination.offset);
        }

        return new Promise((resolve, reject) => {

            const unique: any = {};

            cursor.toArray().then(data => {

                const o = [];

                for(let i = 0, k = 0; i < data.length; i++) {

                    const val = data[i][this.slaveProperty];

                    if(!unique.hasOwnProperty(val)) {
                        o[k++] = val;
                        unique[val] = true;
                    }

                }

                resolve(o);

            }, error => {

                reject(new ModelRejection(error, 500));

            });

        });

    }

}