/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Mongo Query constructor execution pipeline
 *
 ***************************************************************/
import {QueryExecutorInterface, QueryExecutorSpecificInterface} from "../../common/query/query";
import {ModelRejection, StructureInterface} from "../../common/model";
import {Collection} from "mongoose";
import {Cursor} from "mongodb";
import {
    QueryConstructorOperators,
    QueryConstructorProduct, QueryConstructorProductPipelineProduct,
    QueryConstructorRelation
} from "../../common/query/query.constructor";

export class MongoQueryExecutor implements QueryExecutorSpecificInterface {

    public constructQuery: boolean = false;

    public queryToConstruct: QueryConstructorProduct = null;

    constructor(private structure: StructureInterface) {}

    getConstructorProduct(): QueryConstructorProduct {
        return this.queryToConstruct;
    }

    setConstructorProduct(p: QueryConstructorProduct): void {
        this.queryToConstruct = p;
    }

    /**
     * Get one result for Mongo Query
     * --------------------------------------------------------------------
     * @param {MongoQuery} query
     * @param exemplar
     * @returns {Promise<T>}
     */
    public getOne<T>(query: MongoQuery, exemplar: any) : Promise<T> {

        return new Promise<T>((resolve, reject) => {

            let conn: Collection = this.structure.connection;

            conn.findOne(query.query).then(data => {

                if(data === null) {

                    resolve(null);

                } else {

                    let user = new exemplar().__assignFromSourceObject(data);
                    resolve(user);

                }

            }, error => reject(new ModelRejection(error, 500)));

        });

    }

    /**
     * Get array of results for MongoQuery
     * --------------------------------------------------------------------
     * @param {MongoQuery} query
     * @param exemplar
     * @returns {Promise<T>}
     */
    public getMany<T>(query: MongoQuery, exemplar: any) : Promise<T> {

        return new Promise<T>((resolve, reject) => {

            let conn: Collection = this.structure.connection;

            let cursor: Cursor<T> = null;

            // Build query object
            if(query !== null && query.hasOwnProperty("query")) {

                cursor = conn.find<T>(query.query);

                if(query.hasOwnProperty("limit"))
                    cursor.limit(Number(query.limit));

                if(query.hasOwnProperty("offset"))
                    cursor.skip(Number(query.offset));

                if(query.hasOwnProperty("sort")) {
                    if(typeof query.sort === "object")
                        cursor.sort(query.sort);
                }

            } else

                // If query object is not defined - throw rejection with explanation
                reject(new ModelRejection("Query object is not defined for operation getMany at Query object "+
                    "called for Model: "+this.structure.className+". "+
                    "Try to refer to MongoQuery interface when defining new object Query", 400));

            // Get multiple results
            cursor.toArray().then(results => {

                let res = new Array(results.length);

                // TODO Replace transform with for loop which will be faster process to assemble data array
                // Transform
                results.map((r,i) => res[i] = (new exemplar()).__assignFromSourceObject(r));

                // Return Promise
                resolve(res as any);

            }, error => reject(new ModelRejection(error, 500)));

        });

    }

    /**
     * Construct query object from Query Construct Product
     * --------------------------------------------------------------------
     * @param {QueryConstructorProduct} qc
     * @returns {{}}
     * @private
     */
    public __constructQuery(qc: QueryConstructorProduct) : { [p: string]: any } {

        // rewind Query Constructor Product to the end node
        qc.end();

        let query: MongoQuery = {
            query: {},
        };

        // Add starting offset
        if(qc.offset !== null)
            query.offset = qc.offset;

        // Add results limiter
        if(qc.limit !== null)
            query.limit = qc.limit;

        // Add sorting and ordering of result output
        if(qc.order !== null && qc.order.length > 0) {

            let sorts: {[key: string] : number} = {};

            for(let i = 0; i < qc.order.length; i++)

                sorts[qc.order[i].name] = qc.order[i].order === "desc" ? -1 : 1;

            query.sort = sorts;

        }

        let singleClue: Array<any> = [];
        let andClue: Array<any> = [];
        let orClue: Array<any> = [];
        let lastRelation: QueryConstructorRelation = null;

        /*
         *  Create query elements according to constructor elements
         */

        let element: QueryConstructorProductPipelineProduct;

        while (element = qc.prev()) {

            let piece: any = {};

            // Check operator on pipeline element
            if(element.operator === QueryConstructorOperators.Equal) {
                piece[element.name] = element.value;
            }
            else if(element.operator === QueryConstructorOperators.In) {
                piece[element.name] = {$in: element.value};
            }
            else if(element.operator === QueryConstructorOperators.NotEqual) {
                piece[element.name] = {$not: {$eq: element.value}};
            }
            else if(element.operator === QueryConstructorOperators.Greater) {
                piece[element.name] = {$gt: element.value};
            }
            else if(element.operator === QueryConstructorOperators.Lesser) {
                piece[element.name] = {$lt: element.value};
            }

            // Check relations with meaning of AND
            if(element.relation === QueryConstructorRelation.And) {

                if(qc.length() > 0) {

                    if(orClue.length > 0) {

                        andClue.push({$or: orClue});
                        orClue = [];

                    } else if(singleClue.length > 0) {

                        for(let andIdx = 0; andIdx < singleClue.length; andIdx++) {

                            andClue.push(singleClue[andIdx]);

                        }

                        singleClue = [];

                    }

                    lastRelation = QueryConstructorRelation.And;

                    continue;

                }

            }
            // Check relation with meaning of OR
            else if(element.relation === QueryConstructorRelation.Or) {

                if(qc.length() > 0) {

                    if(andClue.length > 0) {

                        orClue.push({$and:andClue});

                        andClue = [];

                    } else if(singleClue.length > 0) {

                        for(let orIdx = 0; orIdx < singleClue.length; orIdx++) {

                            orClue.push(singleClue[orIdx]);

                        }

                        singleClue = [];

                    }

                    lastRelation = QueryConstructorRelation.Or;

                    continue;

                }

            }
            // In all cases which cannot be catch by AND or OR relations add as single
            else {

                // Check if any of relations was checked before single goes to pop
                //  basically - checkup for pair relation for not prepended with AND/OR where statement
                if(lastRelation !== null) {

                    // If last relation was OR then fulfill OR array
                    if(lastRelation === QueryConstructorRelation.Or)

                        orClue.push(piece);

                    // If last relation was AND then fulfull AND array
                    else if(lastRelation === QueryConstructorRelation.And)

                        andClue.push(piece);

                    // Nullify last relation for stop propagating properties until relation defined
                    lastRelation = null;

                } else

                    // If single again - then add it to single clue
                    singleClue.push(piece);

            }

            // If search came to beginning of constructor pipeline
            if(qc.isAtBeginning()) {

                // Apply everything to AND line
                if(andClue.length > 0) {

                    for(let i=0; i < singleClue.length; i++) {

                        andClue.push(singleClue[i]);

                    }

                    // Reset single Queue pipeline size
                    singleClue.length = 0;

                }
                // Apply everything to OR line
                else if(orClue.length > 0) {

                    for(let i=0; i < singleClue.length; i++)

                        orClue.push(singleClue[i]);

                    // Reset single Queue pipeline size
                    singleClue.length = 0;

                }

            }

        }

        /*
         *  Assemble results to query
         */

        // Check single Clue pipeline size and process those if any detected
        //  meaning that constructor came up only with single type of request
        if(singleClue.length > 0) {

            for(let i = 0; i < singleClue.length; i++) {

                let keys:any = Object.keys(singleClue[i]);
                query.query[keys[0]] = singleClue[i][keys[0]];

            }

        }
        // Check AND Clue pipeline and process it if any data detected
        else if(andClue.length > 0) {

            query.query = {$and: andClue};

        }
        // Check OR Clue pipeline and process it if any data detected
        else if(orClue.length > 0) {

            query.query = {$or: orClue};

        }

        return query;

    }

}

export interface MongoQuery {
    query: {[key:string]:any};
    offset?: number;
    limit?: number;
    sort?: {[key:string]:number};
}