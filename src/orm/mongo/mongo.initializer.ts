/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview Render models extending Mongo model to ORM objects
 *
 ***************************************************************/
import * as Mongoose from "mongoose";
import {TypedStructure} from "../common/typed.structure";
import {CompiledRelation} from "../common/relations";
import {ModelJoinerManager} from "../common/model.joiner.manager";
import {MongoJoinerModel} from "./mongo.joiner.model";
import {MongoCounter} from "./mongo.counter.model";
import {ConfigurationException} from "../../exceptions/exceptions";
import {Type} from "../../app";
import {MongoStaticConfiguration} from "./mongo.static.configuration";
import {ModelManager} from "../common/model.manager";
import {ModelCounterInterface} from "../common/model";
/**
 * Mongo Initializer
 * -------------------------------------------------------------------------
 *
 * Will process initialization of Mongo schema and Mongo collection
 * for Entity represented by a TypedStructure.
 *
 * As a result initialized schema will be populated with ready to
 * work connector which suitable to make all required requests
 * for model CRUD operations
 */
export class MongoInitializer {

    public static counterStructureInitialized: boolean = false;

    public static collectionNames: Array<string> = [];

    /**
     * Initialize some structure with Mongo connector
     * ---------------------------------------------------------------------
     *
     * Will make initialization of Structures to have specific fields at
     * persistent storage, as well as indexes will be properly aligned
     * according to structure parameters
     *
     * @param {TypedStructure} structure
     */
    public static async init(structure: TypedStructure) {

        // Prepare schema index information to be pre-aligned
        structure.initForSchema();

        let conn = null;
        let name = null;

        /*
         *  Use default connection for structures without the
         *  specific database pointer defined
         */
        if(structure.connectionName === null) {

            conn = ModelManager.getDefaultMongoInstance();
            name = ModelManager.getDefaultMongoInstanceName();

        }
        /*
         *  Use named connection for structures with the specific
         *  database pointer attached
         */
        else if(ModelManager.getNamedDB(structure.connectionName)) {

            conn = ModelManager.getNamedDB(structure.connectionName);
            name = structure.connectionName;

        }

        /*
         *  If counter cannot be retrieved
         */
        if(MongoStaticConfiguration.getCounter(name) === null)

            structure.counter = this.initCounter(conn, name);

        else

            structure.counter = MongoStaticConfiguration.getCounter(name);

        // Pull Mongoose Schema definer
        let Schema = Mongoose.Schema;

        // Pull Int/Long for integer value proper storage
        let SchemaLongWrapper = Schema as {Types:any};

        // Define schema object
        let schemaObject: {[index:string]:any} = {};

        // Prevent of reindexing some of the properties
        const relationIndexes: Array<{idx: {[key:string]:any}, opt:{unique?:boolean}}> = [];

        // Process all properties to define Mongo/Mongoose structure
        const properties = structure.properties;

        for(let name in properties) {

            const property = properties[name];
            /* ------------------------------------------------------------------------
             *
             *                      Define relation properties,
             *             which representing some linkage between models
             *
             * ------------------------------------------------------------------------
             */

            // If property is relation then initialize it as a relation
            if(property.isRelation()) {

                const compiled = structure.getRelationForName(property.name);

                // If compiled relation by somehow didn't arrive after retrieval,
                // throw misconfiguration
                if(compiled === null)

                    throw new ConfigurationException(
                        "Problem at relation instantiation found. Structure for <" + structure.className + "> "+
                        "has some relation specified for property [" + property.name + "] and seems that this "+
                        "relation or broken, or unconfirmed from the other side. Please check that model which "+
                        "this property refers to is having reference for that relationship to be mutual", 500);

                // Pass property to relation initialization only if it
                // trying to use complex relation modeling
                if(compiled.joinName !== null)

                    this.initComplexRelation(compiled, conn);


                // If relation is Slave relation on mutual linkage we need
                // to define index field as is
                else if(!compiled.master && property.relation.mutual) {

                    const foreignStruct = property.relation.foreignStructure;

                    if(foreignStruct.primaryIndex === null)

                        throw new ConfigurationException(
                            "Problem at relation instantiation found. Structure for <" + structure.className + "> "+
                            "has some relation specified for property [" + property.name + "] and seems that "+
                            "relation has no primary index defined on the side of foreign structure." +
                            "Please check that model which "+
                            "this property refers to has primary index defined", 500);

                    const type = foreignStruct.primaryIndex.type;

                    // Define index type for field on slave of mutual relation
                    if(type === Type.Int)

                        schemaObject[property.name] = SchemaLongWrapper.Types.Long;

                    else if(type === Type.Float)

                        schemaObject[property.name] = Number;

                    else if(type === Type.String)

                        schemaObject[property.name] = String;

                    // Define UniqueWith index set which describes multiple
                    // index locking behavior where error will pop up if
                    // duplication of data will be found on save or update
                    if(property.secondaryMergeKeys.length > 0) {

                        let indexEntity: any = {};

                        let unique = {unique: true};

                        indexEntity[property.name] = 1;

                        property.secondaryMergeKeys.map(key => {

                            if(structure.properties.hasOwnProperty(key)) {

                                indexEntity[key] = 1;

                            }

                        });

                        // Push as unique, because definition for relation
                        // says UniqueWith. That allow build objects with
                        // indexes locking duplication ability on several fields
                        relationIndexes.push({idx:indexEntity, opt:unique});

                    }

                    // Define index just for that property as unique
                    if(property.unique) {

                        let indexEntity: any = {};

                        indexEntity[property.name] = 1;

                        relationIndexes.push({idx:indexEntity, opt:{unique: true}});

                    }
                    // Define index just for that property without options
                    else {

                        let indexEntity: any = {};

                        indexEntity[property.name] = 1;

                        relationIndexes.push({idx:indexEntity, opt:{}});

                    }

                }

                // Do not proceed forward on property definitions if it was relational
                // definition parsing
                continue;

            }

            /* ------------------------------------------------------------------------
             *
             *                  Define properties which is not relations
             *
             * ------------------------------------------------------------------------
             */

            // Assign Type String
            if(property.type === Type.String) {
                schemaObject[property.name] = String;
            }
            // Assign Type Integer
            else if(property.type === Type.Int) {

                /*
                 * Check if while Type Integer is assigned, property primary index status
                 * If property is a primary index then assign it to unique index
                 */
                if(property.primary) {

                    let narrowProperty:any = {
                        type: SchemaLongWrapper.Types.Long,
                        unique:true
                    };

                    schemaObject[property.name] = narrowProperty;

                } else

                    schemaObject[property.name] = SchemaLongWrapper.Types.Long;

            }
            // Assign Type Boolean
            else if(property.type === Type.Bool) {
                schemaObject[property.name] = Boolean;
            }
            // Assign Type Date
            else if(property.type === Type.Date) {
                schemaObject[property.name] = Date;
            }
            // Assign Type Float
            else if(property.type === Type.Float) {
                schemaObject[property.name] = Number;
            }
            // Assign Type Array
            else if(property.type === Type.Array) {
                schemaObject[property.name] = Array;
            }
            // Assign Type Map
            else if(property.type === Type.Map) {
                schemaObject[property.name] = Object;
            }

        }

        const collectionName: string = (structure.className.toLowerCase()) + 's';

        /*
         * Define schema
         * ------------------------------------------------------------------------
         */
        let schema = new Schema(schemaObject, {collection: collectionName});

        /*
         * Assign secondary indexes
         * ------------------------------------------------------------------------
         */
        structure.secondaryIndex.forEach(s => {

            let indexEntity: any = {};

            // Define unique index for secondary
            let unique: any = s.unique ? {unique: true} : {};

            // Assign index fields, can be compound
            s.fieldSet.map(indexProp => indexEntity[indexProp] = 1);

            // Apply schema secondary index
            schema.index(indexEntity, unique);

        });

        /*
         * Assign relational indexes
         * ------------------------------------------------------------------------
         */
        relationIndexes.map(index => {

            schema.index(index.idx, index.opt);

        });

        /*
         * Create Mongoose Model which required for extract connection
         * ------------------------------------------------------------------------
         */
        let model = conn.model(structure.className, schema);

        /*
         * Assign collection as connection for typed structure
         * ------------------------------------------------------------------------
         */
        structure.connection = model.collection;

        /*
         * If structure relays on Primary index with auto-increment then it will be
         * prepared during that step
         * ------------------------------------------------------------------------
         */
        if(structure.primaryIndex !== null && structure.primaryIndex.auto) {

            try {

                // Initialize counter total schema objects counter
                await structure.counter.prepare();

                // Initialize all counter values for models prior to first serve
                await structure.counter.prepareForModel(structure.className);

            } catch (e) {

                throw new ConfigurationException(
                    "Cannot prepare Mongo Counter for "+ structure.className + ", error: " + e.message, 500);

            }

        }

        /*
         * Record name as assigned
         * ------------------------------------------------------------------------
         */
        this.collectionNames.push(collectionName);

        return true;

    }

    /**
     * Initialize relation
     * ---------------------------------------------------------------------
     * Method will make initialization of complex relation like MTM modeling
     *
     * @param {CompiledRelation} relation
     * @param conn
     */
    public static initComplexRelation(relation: CompiledRelation, conn: any) {

        // For preserve some effeciency on table documents
        // Join table pair columns will be simplified to just
        // letters a and b
        const masterName = "a";
        const slaveName = "b";

        // If not master nor mutual - reject operation
        if(!relation.master || relation.joinName === null) {
            return;
        }

        // Pull Mongoose Schema definer
        let Schema = Mongoose.Schema;

        // Pull Int/Long for integer value proper storage
        let SchemaLongWrapper = Schema as {Types:any};

        // Define schema object
        let schemaObject: {[index:string]:any} = {};

        schemaObject[masterName] = SchemaLongWrapper.Types.Long;
        schemaObject[slaveName] = SchemaLongWrapper.Types.Long;

        /*
         * Define schema
         * ------------------------------------------------------------------------
         */
        let schema = new Schema(schemaObject, {collection: relation.joinName});

        let ownIndex: any = {};
        ownIndex[masterName] = 1;

        let foreignIndex: any = {};
        foreignIndex[slaveName] = 1;

        schema.index(ownIndex);
        schema.index(foreignIndex);

        /*
         * Create Mongoose Model which required for extract connection
         * ------------------------------------------------------------------------
         */
        let model = conn.model(relation.joinName, schema);

        /*
         * Create Joiner Model as retrieval interface for Join data
         * ------------------------------------------------------------------------
         */
        let joiner = new MongoJoinerModel();
        joiner.__setCollection(model.collection);
        joiner.__setMasterName(masterName);
        joiner.__setSlaveName(slaveName);

        ModelJoinerManager.set(relation.joinName, joiner);

        /*
         * Assign joiner model to relations
         * ------------------------------------------------------------------------
         */
        relation.joinModel = joiner;
        relation.joinSlave.joinModel = joiner;

        this.collectionNames.push(relation.joinName);

    }

    /**
     * Initialize Mongo Counter structure
     * -------------------------------------------------------------------------
     * As Mongo not providing self incrementing identifiers which is required
     * to maintain model interoperability with RDB solutions within same model
     * structure, special database structure should serve those id increments
     *
     * @see MongoCounter model for exact imcrementAndGet implementation
     * @param conn
     * @param name
     */
    public static initCounter(conn: any, name: string) : ModelCounterInterface {

        let Schema = Mongoose.Schema;
        let SchemaLongWrapper = Schema as {Types:any};

        let schema = new Schema({

            id: SchemaLongWrapper.Types.Long,
            name: String,
            counter: SchemaLongWrapper.Types.Long

        }, {collection: 'counter'});

        schema.index({id: 1},{unique:true});
        schema.index({name: 1},{unique:true});

        let model = conn.model("_counter_", schema);

        const counter = new MongoCounter(model.collection);

        MongoStaticConfiguration.addCounter(name, counter);

        return counter;

    }

}