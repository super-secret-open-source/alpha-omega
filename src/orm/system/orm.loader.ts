/***************************************************************
 *
 * @author Anton Repin
 * @version beta
 * @overview ORM Loading and schema preparation mechanics
 *
 ***************************************************************/
import {ConfigurationException} from "../../exceptions/exceptions";
import {TypedStructure} from "../common/typed.structure";
import {MongoInitializer} from "../mongo/mongo.initializer";
import {MongoModel} from "../mongo.model";
import {ModelManager} from "../common/model.manager";
import {DBConfiguration} from "../../config/configurator";
import {App} from "../../app";

export class ORMLoader {

    /**
     * Load ORM Structures using DBConfiguration data
     * ----------------------------------------------------------------------------------
     * @param {DBConfiguration[]} configuration
     * @returns {Promise<boolean>}
     */
    public async load(configuration: DBConfiguration[]) {

        /*
         *      Take configuration list from config definition
         */
        const list = configuration;

        /*
         *      Apply and instantiate Database configurations
         */
        list.map(config => {

            ModelManager.setConfiguration(config.name, config);

        });

        /*
         *      Set default configuration for Model Manager
         */
        if(list.length > 0)

            ModelManager.setDefaultConfigurationName(list[0].name);

        try {

            /*
             *      Wait until Model Manager connects to all data sources
             */
            await ModelManager.waitForReadyState();

            /*
             *      Prepare all schema for models on initialization for Mongo Models
             */
            const mongoStructures: Array<TypedStructure> = MongoModel.__getStructureList();

            /*
             *      First pass to initialize relations
             */
            for(let i = 0; i < mongoStructures.length; i++) {

                if(mongoStructures[i].relationStructure !== null)
                    mongoStructures[i].relationStructure.init();

            }

            /*
             *      Second pass to finalize relations and create linkage
             */
            for(let i = 0; i < mongoStructures.length; i++) {

                if(mongoStructures[i].relationStructure !== null)
                    mongoStructures[i].relationStructure.prepare();

            }

            /*
             *      Wait until Mongo Initializer will make all structures
             *      live and prepared for further execution
             */
            for(let i = 0; i < mongoStructures.length; i++) {

                try {

                    // await for every structure to be pre-initialized
                    await MongoInitializer.init(mongoStructures[i]);

                } catch (e) {

                    // Show error happened at certain type of initialization
                    App.error('[' + e.code + "] ORM Loader was unable to properly initialize " +
                        "Mongo structure " +mongoStructures[i].className +"" + e.message, "orm:init");

                    // Rethrow original error
                    throw e;

                }

            }

            /*
             *      Prepare all schema for models on initialization for MySQL Models
             */

            // TODO MYSQL Model interaction

            return true;

        } catch (e) {

            App.error("Cannot finish program initialization, will exit. Error: " + e.message, "app:init");

            throw new ConfigurationException("Cannot finish initialization. Error: " + e.message,
                500);

        }

    }

}